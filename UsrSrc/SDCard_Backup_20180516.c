#include "SDCard.h"
#include <stdlib.h>
#include <string.h>
extern RTC_HandleTypeDef hrtc;


  
extern DMA_HandleTypeDef hdma_sdio_rx;
extern DMA_HandleTypeDef hdma_sdio_tx;
extern SD_HandleTypeDef hsd;

FIL SettingFile;
FIL MoldFile;
FIL LogFile;
FIL LanguageFile;


FATFS SDFatFs;  /* File system object for SD card logical drive */
char SDPath[4]; /* SD card logical drive path */
static uint8_t buffer[_MAX_SS]; /* a work buffer for the f_mkfs() */

//FRESULT res;                                          /* FatFs function common result code */
uint32_t byteswritten, bytesread;                     /* File write/read counts */

MOLD_MSG MoldMsg;
IO_MSG IOMsg;
ERROR_MSG ErrMsg;
SETTING_MSG SettingMsg;

SDRES CountLogFromFile(FIL* fp, uint16_t* pTotalCnt);
SDRES LoadRecentLog(ERROR_MSG* pErrHistory, uint8_t size, uint16_t* pCountErrLine);
SDRES DeleteLogFile(void);


SDRES GetExistFile(const TCHAR* path);
SDRES AddLogToEndofFile(ERR_MSG* pErrMsg);

//void WriteSD(FILES thisFile, uint8_t* writeBuffer, uint8_t size)
//{
//	/*##-5- Write data to the text file ################################*/
//	if(thisFile == FILE_SETTINGS)	
//		res = f_write(&SettingFile, writeBuffer, size, (void *)&byteswritten);
//	else if(thisFile == FILE_MOLD)	
//		res = f_write(&MoldFile, writeBuffer, size, (void *)&byteswritten);
//	else if(thisFile == FILE_LANGUAGE)	
//		res = f_write(&LanguageFile, writeBuffer, size, (void *)&byteswritten);
//	else if(thisFile == FILE_LOG)	
//		res = f_write(&LogFile, writeBuffer, size, (void *)&byteswritten);
//	else
//	{
//	}
//	if((byteswritten == 0) || (res != FR_OK))
//	{
//		/* 'STM32.TXT' file Write or EOF Error */
////		Error_Handler();
//	}
//}
//void ReadSD(FILES thisFile, uint8_t* readBuffer, uint8_t size)
//{
//	/*##-8- Read data from the text file ###########################*/
//	if(thisFile == FILE_SETTINGS)	
//		res = f_read(&SettingFile, readBuffer, size, (UINT*)&bytesread);
//	else if(thisFile == FILE_MOLD)
//		res = f_read(&MoldFile, readBuffer, size, (UINT*)&bytesread);
//	else if(thisFile == FILE_LANGUAGE)
//		res = f_read(&LanguageFile, readBuffer, size, (UINT*)&bytesread);
//	else if(thisFile == FILE_LOG)
//		res = f_read(&LogFile, readBuffer, size, (UINT*)&bytesread);
//
//	if((bytesread == 0) || (res != FR_OK))
//	{
//		/* 'STM32.TXT' file Read or EOF Error */
////		Error_Handler();
//	}
//}
//void CloseSD(void)
//{
//	/*##-6- Close the open text file #################################*/
//	f_close(&SettingFile);
//	f_close(&MoldFile);
//	f_close(&LogFile);
//	f_close(&LanguageFile);
//	printf("Closed Files/r");
//
//}

void OpenSD(FIL *File, char const *strFilename)
{
	if(f_open(File, strFilename, FA_OPEN_ALWAYS | FA_WRITE) != FR_OK)
	{
		/* 'STM32.TXT' file Open for write Error */
		//          Error_Handler();
		//			  printf("*************FAIL OPEN**************\r");
	}
}
void FormatSD(void)
{
	/*##-3- Create a FAT file system (format) on the logical drive #########*/
      /* WARNING: Formatting the uSD card will delete all content on the device */
      if(f_mkfs((TCHAR const*)SDPath, FM_ANY, 0, buffer, sizeof(buffer)) != FR_OK)
      {
        /* FatFs Format Error */
//			printf("*************FAIL FORMAT**************\r");
//        Error_Handler();
      }
}

SDRES LoadSDMold(MOLD_MSG* pMoldMsg)
{
//	if(f_open(&MoldFile, FILE_NAME_SETTING, FA_OPEN_EXISTING | FA_READ) != FR_OK)
//	{
//		//WRITE DEFAULT FILE
//		if(f_open(&MoldFile, FILE_NAME_SETTING, FA_CREATE_NEW | FA_WRITE) != FR_OK)
//		{
//			return SD_FAIL;
//		}
//		else
//		{
//			pMoldMsg->ControlMode = CONTROLMODE_FILE_SAVED;
//			pMoldMsg->Version = (VERSION_MAIN*10000) + (VERSION_SUB*100) + VERSION_PATCH;
//			pMoldMsg->Language = LANGUAGE_ENG;
//			pMoldMsg->FuncSaveCnt = FUNC_DISABLE;
//			res = f_write(&MoldFile, pMoldMsg, sizeof(pMoldMsg), (void *)&byteswritten);
//			f_close(&SettingFile);
//			if((byteswritten == 0) || (res != FR_OK)) return SD_FAIL;
//		}
//	}
//	else
//	{
//		res = f_read(&MoldFile, pMoldMsg, sizeof(pMoldMsg), (UINT*)&bytesread);
//		f_close(&SettingFile);
//		if((bytesread == 0) || (res != FR_OK)) return SD_FAIL;
//		if(pMoldMsg->ControlMode != CONTROLMODE_FILE_SAVED) return SD_FAIL;
//		
//	}
	return SD_OK;
}

SDRES LoadSDSetting(SETTING_MSG* pSettingMsg)
{
//	static uint64_t TempTick, CurrentTick;
//	static uint32_t currentTime;
//	printf("Func Start!!\n\r");
	FRESULT res;
//	TempTick = HAL_GetTick();
//	printf("Time Reset\n\r");
//	CurrentTick = HAL_GetTick();
	
	if(f_open(&SettingFile, FILE_NAME_SETTING, FA_OPEN_EXISTING | FA_READ) != FR_OK)
	{
//		CurrentTick = HAL_GetTick();
//		currentTime = (uint32_t)(CurrentTick - TempTick);
//		printf("f_open fail(Not Exist) : %d\n\r",currentTime);
		
//		TempTick = HAL_GetTick();
//		printf("Time Reset\n\r");
		
//		FormatSD();
		
//		CurrentTick = HAL_GetTick();
//		currentTime = (uint32_t)(CurrentTick - TempTick);
//		printf("Format Time : %d\n\r",currentTime);
		
		
//		TempTick = HAL_GetTick();
//		printf("Time Reset\n\r");
	
		//WRITE DEFAULT FILE
		if(f_open(&SettingFile, FILE_NAME_SETTING, FA_CREATE_NEW | FA_WRITE) != FR_OK)
		{
			return SD_FAIL;
		}
		else
		{
//			CurrentTick = HAL_GetTick();
//			currentTime = (uint32_t)(CurrentTick - TempTick);
//			printf("f_open fail(Make Done) : %d\n\r",currentTime);
			
			pSettingMsg->ControlMode = CONTROLMODE_FILE_SAVED;
			pSettingMsg->Version = (VERSION_MAIN*10000) + (VERSION_SUB*100) + VERSION_PATCH;
			pSettingMsg->Language = LANGUAGE_ENG;
			pSettingMsg->FuncSaveCnt = FUNC_DISABLE;
			
//			TempTick = HAL_GetTick();
//			printf("Time Reset\n\r");
			
			res = f_write(&SettingFile, pSettingMsg, sizeof(SETTING_MSG), (void *)&byteswritten);
//			res = f_write(&SettingFile, "ABCDEFGHIJK", 11, (void *)&byteswritten);
			
//			CurrentTick = HAL_GetTick();
//			currentTime = (uint32_t)(CurrentTick - TempTick);
//			printf("Writing Time : %d\n\r",currentTime);
			f_close(&SettingFile);
			
//			CurrentTick = HAL_GetTick();
//			currentTime = (uint32_t)(CurrentTick - TempTick);
//			printf("Writing Time & Close: %d\n\r",currentTime);
			
			if((byteswritten == 0) || (res != FR_OK)) return SD_FAIL;
		}
	}
	else
	{
//		CurrentTick = HAL_GetTick();
//		currentTime = (uint32_t)(CurrentTick - TempTick);
//		printf("f_open Success : %d\n\r",currentTime);
			
//		TempTick = HAL_GetTick();
//		printf("Time Reset\n\r");
		
		res = f_read(&SettingFile, pSettingMsg, sizeof(SETTING_MSG), (UINT*)&bytesread);
		
//		CurrentTick = HAL_GetTick();
//			currentTime = (uint32_t)(CurrentTick - TempTick);
//		printf("Read Time : %d\n\r",currentTime);
		
		f_close(&SettingFile);
		
//		CurrentTick = HAL_GetTick();
//			currentTime = (uint32_t)(CurrentTick - TempTick);
//		printf("Read Time & Close: %d\n\r",currentTime);
		
		if((bytesread == 0) || (res != FR_OK)) return SD_FAIL;
		if(pSettingMsg->ControlMode != CONTROLMODE_FILE_SAVED) return SD_FAIL;
		
	}
	return SD_OK;
}

SDRES AddSDLog(ERROR_MSG* pErrMsg)
{
	FIL* fp;
	volatile uint16_t TotalCnt = 0;;
	
	fp = &LogFile;
	
	if(f_open(fp, FILE_NAME_LOG, FA_OPEN_ALWAYS | FA_WRITE) != FR_OK)
	{
		return SD_FILE_OPEN_FAIL;
	}
	
	CountLogFromFile(fp, &TotalCnt);
	
	if(TotalCnt < MAX_LOG_COUNT) //add log
	{
		
		f_lseek (fp, f_size (fp));
	f_printf(fp, "%2d-%2d-%2d\t%2d:%2d:%2d%5d%5d",
				pErrMsg->Time.Date.Year,pErrMsg->Time.Date.Month,pErrMsg->Time.Date.Date,
				pErrMsg->Time.Time.Hours,pErrMsg->Time.Time.Minutes,pErrMsg->Time.Time.Seconds,
				pErrMsg->ErrorCode, pErrMsg->MoldNo);
	res = f_write(fp, pErrMsg, sizeof(ERROR_MSG), (void *)&byteswritten);
	f_printf(fp, "\n");
	}
	else //move and addlog
	{
	}
	
	FRESULT res;
	
	
	FRESULT fr;
	
	FILINFO fno;
	FIL* fp;
	char FileName[13];
	STRUCT_TIME NowTime;
//	DIR dir;
	
	
	fr = f_stat("/log", &fno);
	switch (fr) {
		case FR_OK:
//		printf("Size: %lu\n", fno.fsize);
//		printf("Timestamp: %u/%02u/%02u, %02u:%02u\n",
//				 (fno.fdate >> 9) + 1980, fno.fdate >> 5 & 15, fno.fdate & 31,
//				 fno.ftime >> 11, fno.ftime >> 5 & 63);
//		printf("Attributes: %c%c%c%c%c\n",
//				 (fno.fattrib & AM_DIR) ? 'D' : '-',
//				 (fno.fattrib & AM_RDO) ? 'R' : '-',
//				 (fno.fattrib & AM_HID) ? 'H' : '-',
//				 (fno.fattrib & AM_SYS) ? 'S' : '-',
//				 (fno.fattrib & AM_ARC) ? 'A' : '-');
		break;
		
		case FR_NO_FILE:
//		printf("It is not exist.\n");
		res = f_mkdir("/log");
		if(res) printf("Fail to Mkdir...\n");
		break;
		
		default:
		printf("An error occured. (%d)\n", fr);
	}
	
	f_chdir("/log");
//	f_opendir(&dir, "/log");
	
	fp = &LogFile;
	NowTime = GetTime();
	
	sprintf(&FileName[0], "20000000.log\n");
	
	FileName[2] = (NowTime.Date.Year)/10 + 0x30;
	FileName[3] = (NowTime.Date.Year)%10 + 0x30;
	FileName[4] = (NowTime.Date.Month)/10 + 0x30;
	FileName[5] = (NowTime.Date.Month)%10 + 0x30;
	FileName[6] = (NowTime.Date.Date)/10 + 0x30;
	FileName[7] = (NowTime.Date.Date)%10 + 0x30;
	
	if(f_open(fp, FileName, FA_OPEN_ALWAYS | FA_WRITE) != FR_OK)
	{
		return SD_FAIL;
	}
	f_lseek (fp, f_size (fp));
	f_printf(fp, "%2d-%2d-%2d\t%2d:%2d:%2d%5d%5d",
				pErrMsg->Time.Date.Year,pErrMsg->Time.Date.Month,pErrMsg->Time.Date.Date,
				pErrMsg->Time.Time.Hours,pErrMsg->Time.Time.Minutes,pErrMsg->Time.Time.Seconds,
				pErrMsg->ErrorCode, pErrMsg->MoldNo);
	res = f_write(fp, pErrMsg, sizeof(ERROR_MSG), (void *)&byteswritten);
	f_printf(fp, "\n");
	f_close(fp);
	if((byteswritten == 0) || (res != FR_OK)) return SD_FAIL;
		
		/*
//	FIL* fp;
//	uint32_t CntErr;
//	ERROR_MSG TempErrMsg;
//	
//	fp = &LogFile;
//	if(f_open(fp, FILE_NAME_LOG, FA_OPEN_ALWAYS | FA_WRITE) != FR_OK)
//	{
//		return SD_FAIL;
//	}
//
//		CntErr = (uint32_t)(f_size(fp) / LOG_BYTE_PER_LINE);
//		
//	
//		f_lseek (fp, f_size (fp));
//		f_printf(fp, "%2d-%2d-%2d\t%2d:%2d:%2d%5d%5d",
//					pErrMsg->Time.Date.Year,pErrMsg->Time.Date.Month,pErrMsg->Time.Date.Date,
//					pErrMsg->Time.Time.Hours,pErrMsg->Time.Time.Minutes,pErrMsg->Time.Time.Seconds,
//					pErrMsg->ErrorCode, pErrMsg->MoldNo);
//		res = f_write(fp, pErrMsg, sizeof(ERROR_MSG), (void *)&byteswritten);
//		f_printf(fp, "\n");
//
//		f_close(fp);
//		if((byteswritten == 0) || (res != FR_OK)) return SD_FAIL;*/

	return SD_OK;
}

SDRES DeleteLogFile(void)
{
	DIR dir;
	FILINFO fno;
	FRESULT res;
	f_closedir(&dir);
	
	res = f_opendir(&dir, PATH_LOG);                       /* Open the directory */
	if(res != FR_OK) printf("Fail to OpenDir in Deletelogfile()..\n");
	f_chdir(PATH_LOG);
	res = f_readdir(&dir, &fno);
	//		if (res != FR_OK || fno.fname[0] == 0) printf("fail to read dir\n");
	//		f_unlink(fno.fname);
	printf("Deleted a file...\n");
	
	f_closedir(&dir);
	return SD_OK;
}
SDRES CountLogFromFile(FIL* fp, uint16_t* pTotalCnt)
{
	FILINFO fno;
	FRESULT res;
	
	*pTotalCnt = (uint16_t)(f_size(fp) / LOG_BYTE_PER_LINE)
	return SD_OK;
}
SDRES AddLogToEndofFile(ERR_MSG* pErrMsg)
{
	f_lseek (fp, f_size (fp));
	f_printf(fp, "%2d-%2d-%2d\t%2d:%2d:%2d%5d%5d",
				pErrMsg->Time.Date.Year,pErrMsg->Time.Date.Month,pErrMsg->Time.Date.Date,
				pErrMsg->Time.Time.Hours,pErrMsg->Time.Time.Minutes,pErrMsg->Time.Time.Seconds,
				pErrMsg->ErrorCode, pErrMsg->MoldNo);
	res = f_write(fp, pErrMsg, sizeof(ERROR_MSG), (void *)&byteswritten);
	f_printf(fp, "\n");
}




SDRES LoadSDLog(ERROR_MSG* pErrHistory, uint8_t size)
{
//	FILINFO fno;
//	FRESULT fr;
//	DIR dir;
	uint16_t CountErrLine;
//	uint16_t Bigger;
//	FIL* fp;
//	uint16_t CntErr;
//	uint16_t i,j;

	printf("Start Loding log files\n");
	

//	HAL_Delay(1000);
	CountLogFromFiles(&CountErrLine);
	
//	CountLogFromFiles(&CountErrLine);
//	HAL_Delay(1000);
	if(CountErrLine > MAX_LOG_COUNT) DeleteLogFile();
//		HAL_Delay(2000);

	LoadRecentLog(pErrHistory, size, &CountErrLine);
//	HAL_Delay(1000);
	
	
	
//	for(int i=0;i<size;i++)
	for(int i=0;i<5;i++)
	{
		printf("%2d-%2d-%2d %2d:%2d:%2d\t %5d\t%5d\n",
				 pErrHistory[i].Time.Date.Year,pErrHistory[i].Time.Date.Month,pErrHistory[i].Time.Date.Date,
				 pErrHistory[i].Time.Time.Hours,pErrHistory[i].Time.Time.Minutes,pErrHistory[i].Time.Time.Seconds,
				 pErrHistory[i].ErrorCode, pErrHistory[i].MoldNo);
	}
	
	
//	f_closedir(&dir);
    
	 
//	printf("", fno.

//	FIL* fp;
//	uint32_t CntErr;
//	
//	fp = &LogFile;
//	
//	if(f_open(fp, FILE_NAME_LOG, FA_OPEN_ALWAYS | FA_READ) != FR_OK)
//	{
//		return SD_FAIL;
//	}
//
//		CntErr = (uint32_t)(f_size(fp) / LOG_BYTE_PER_LINE);
//		if(CntErr > size) CntErr = size;
//		
////		printf("Total Byte : %d\n\r",f_size(fp));
////		printf("Count of line : %d\n\r", CntErr);
//		
//		f_lseek (fp, f_size(fp) - LOG_BYTE_PER_LINE + LOG_STRING_PER_LINE);
////		printf("Start position : %d\n\r",f_tell(fp));
//		for(int i=0; i< CntErr ;i++)
//		{
//			res = f_read(fp, &(pErrHistory[i]), sizeof(ERROR_MSG), (UINT*)&bytesread);
//			f_lseek (fp, f_tell(fp) - LOG_BYTE_PER_LINE - SIZE_OF_ERROR_MSG);
////			printf("%d position : %d\t",i, f_tell(fp));
////		  printf("%2d-%2d-%2d %2d:%2d:%2d\t %5d\t%5d\n\r",
////					pErrHistory[i].Time.Date.Year,pErrHistory[i].Time.Date.Month,pErrHistory[i].Time.Date.Date,
////					pErrHistory[i].Time.Time.Hours,pErrHistory[i].Time.Time.Minutes,pErrHistory[i].Time.Time.Seconds,
////					pErrHistory[i].ErrorCode, pErrHistory[i].MoldNo);
//		}
//
//		f_close(fp);
//		if((bytesread == 0) || (res != FR_OK)) return SD_FAIL;

	return SD_OK;
}
SDRES LoadRecentLog(ERROR_MSG* pErrHistory, uint8_t size, uint16_t* pCountErrLine)
{
	uint16_t cnt, CntErr;
	
//	res = f_opendir(&dir, PATH_LOG);                       /* Open the directory */
//	if(res != FR_OK) printf("Fail to open DIR!!\n");
//	if (res != FR_OK) return SD_FAIL;
	
	DIR dir;
	FILINFO fno;
	FIL* fp;
	char path[20];
	uint16_t	i;
	
	f_closedir(&dir);
	
	sprintf(&path[0], "%s", PATH_LOG);
	
//	f_closedir(&dir);
	
//	res = f_opendir(&dir, PATH_LOG);                       /* Open the directory */
//	if(res != FR_OK) printf("Fail to open DIR!!\n");
//	
//	//	f_chdir(PATH_LOG);
//	
//	if (res != FR_OK) return SD_FAIL;
//	
//
//	cnt = 0;
//	for (;;) {
//		res = f_readdir(&dir, &fno);                   /* Read a directory item */
//		if (res != FR_OK || fno.fname[0] == 0) break;  /* Break on error or end of dir */
//		//		printf("%s\n", fno.fname);
//		
//		//		f_chdir(PATH_LOG);
//		//		HAL_Delay(100);
//		sprintf(&path[4], "/%s", fno.fname);
//		//		f_close(fp);
//		
//		
//		res = f_chdir(PATH_LOG);
//		//		printf("f_chdir = %d \n", res);
//		//		res = f_open(fp, path, FA_OPEN_ALWAYS | FA_READ);
//		res = f_open(fp, fno.fname, FA_READ);
//		//		res = f_open(fp, path, FA_OPEN_EXISTING | FA_READ);
//		if(res != FR_OK)
//		{
//			
//			//			printf("Fail to open file!!:%s :%d \n", fno.fname, res);
//			printf("Fail to open file!!:%s :%d \n", path, res);
//			return SD_FAIL;
//		}
//		else
//		{
////			f_close(fp);
//		}
//		CntErr = (uint32_t)(f_size(fp) / LOG_BYTE_PER_LINE);
//		f_lseek (fp, LOG_STRING_PER_LINE);
//		
//		for(i=0; i< CntErr ;i++)
//		{
//			if(*pCountErrLine > size && *pCountErrLine < size + cnt)
//				f_read(fp, &(pErrHistory[size - cnt]), sizeof(ERROR_MSG), (UINT*)&bytesread);
//			else if(*pCountErrLine <= size && *pCountErrLine + cnt < size)
//				f_read(fp, &(pErrHistory[*pCountErrLine - cnt]), sizeof(ERROR_MSG), (UINT*)&bytesread);
//			
//			f_lseek (fp, f_tell(fp) + LOG_STRING_PER_LINE + 2);
//			cnt++;
//		}
//		
//		f_close(fp);
//	}
	
	

	
//	f_closedir(&dir);

	return SD_OK;

}
SDRES InitSD(void)
{
	FILINFO fno;
	FRESULT fr;
	FRESULT res;
	
	if(HAL_GPIO_ReadPin(SD_DETECT_GPIO_Port, SD_DETECT_Pin) == GPIO_PIN_RESET)
	{
		printf("ERROR : Slot Empty!!\n\r");
		return SD_SLOT_EMPTY;
	}
	if(HAL_GPIO_ReadPin(SD_WRITE_PROTECTION_GPIO_Port, SD_WRITE_PROTECTION_Pin) == GPIO_PIN_RESET)
	{
		printf("ERROR : SD Write Protection!!\n\r");
		return SD_SLOT_WR_PROTECT;
	}

	/*##-1- Link the micro SD disk I/O driver ##################################*/
	if(FATFS_LinkDriver(&SD_Driver, SDPath) != 0)
	{
		printf("ERROR : Cannot Link Driver!!\n\r");
		return SD_FAIL;
	}
	/*##-2- Register the file system object to the FatFs module ##############*/
	if(f_mount(&SDFatFs, (TCHAR const*)SDPath, 0) != FR_OK)
    {
		 printf("ERROR : Cannot mount!!\n\r");
		 return SD_FAIL;
      /* FatFs Initialization Error */
//      Error_Handler();
    }
	
	fr = f_stat(PATH_LOG, &fno);
	switch (fr) {
		case FR_OK:
//		printf("Size: %lu\n", fno.fsize);
//		printf("Timestamp: %u/%02u/%02u, %02u:%02u\n",
//				 (fno.fdate >> 9) + 1980, fno.fdate >> 5 & 15, fno.fdate & 31,
//				 fno.ftime >> 11, fno.ftime >> 5 & 63);
//		printf("Attributes: %c%c%c%c%c\n",
//				 (fno.fattrib & AM_DIR) ? 'D' : '-',
//				 (fno.fattrib & AM_RDO) ? 'R' : '-',
//				 (fno.fattrib & AM_HID) ? 'H' : '-',
//				 (fno.fattrib & AM_SYS) ? 'S' : '-',
//				 (fno.fattrib & AM_ARC) ? 'A' : '-');
		break;
		
		case FR_NO_FILE:
		printf("Log folder is not exist.\n");
		
		res = f_mkdir(PATH_LOG);
		if(res)
		{
			printf("Fail to Mkdir...\n");		
			return SD_DO_NOT_EXIST_FILE;
		}
		break;
		
		default:
		printf("An error occured. (%d)\n", fr);
	}

	return SD_OK;
}
SDRES GetExistFile(const TCHAR* path)
{
	FRESULT fr;
	FILINFO fno;
	
	fr = f_stat(path, &fno);
	
	switch (fr) {
		
		case FR_OK:
		return SD_FILE_EXIST;
		break;
		
		case FR_NO_FILE:
		return SD_DO_NOT_EXIST_FILE;
		
		break;
		
		default:
		printf("An error occured. (%d)\n", fr);
	}
	return SD_OK;
}
SDRES MakeFile(const TCHAR* path)
{
}


STRUCT_TIME GetTime(void)
{
	STRUCT_TIME sTime;
	HAL_RTC_GetTime(&hrtc, &(sTime.Time), FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &(sTime.Date), FORMAT_BIN);
	
	return sTime;
}
void SetTime(STRUCT_TIME* pTime)
{
	HAL_RTC_SetTime(&hrtc, &(pTime->Time), FORMAT_BIN);
	HAL_RTC_SetDate(&hrtc, &(pTime->Date), FORMAT_BIN);
}
uint8_t ByteToAscii(uint8_t data)
{
	if(data < 10) return (data + 0x30);
	else return(data - 10 + 0x41);
}




