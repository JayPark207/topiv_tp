/* Includes -------------------------------------------------------------------*/
#include <string.h>
#include "stdlib.h"
#include "AppMain.h"
#include "SDCard.h"
#include "AppKey.h"
#include "ActiveRun.h"

/* typedef --------------------------------------------------------------------*/

/* define ---------------------------------------------------------------------*/
uint8_t ReadType(void)
{
  return (uint8_t)(((GPIOB->IDR >> 14) & 0x03) ^ 0x03);
}

//key가 숫자인지 아닌지 판별 
uint8_t KeyIsNum(uint32_t* key)
{
  uint8_t ret;
  switch(*key)
  {
  case KEY_NO_0:
  case KEY_NO_1:
  case KEY_NO_2:
  case KEY_NO_3:
  case KEY_NO_4:
  case KEY_NO_5:
  case KEY_NO_6:
  case KEY_NO_7:
  case KEY_NO_8:
  case KEY_NO_9:
    ret = KEY_IS_NUM;
    break;
  default:
    ret = KEY_IS_NOT_NUM;
    break;
  }
  return ret;
}

uint8_t KeyNum(uint32_t* key)
{
  uint8_t ret;
  switch(*key)
  {
  case KEY_NO_0:
    ret = 0;
    break;
  case KEY_NO_1:
    ret = 1;
    break;
  case KEY_NO_2:
    ret = 2;
    break;
  case KEY_NO_3:
    ret = 3;
    break;
  case KEY_NO_4:
    ret = 4;
    break;
  case KEY_NO_5:
    ret = 5;
    break;
  case KEY_NO_6:
    ret = 6;
    break;
  case KEY_NO_7:
    ret = 7;
    break;
  case KEY_NO_8:
    ret = 8;
    break;
  case KEY_NO_9:
    ret = 9;
    break;
  default:
    ret = 0xFF;
    break;
  }
  return ret;
}

void ChangeSCMode(void)
{
  ControlMsg.CommStateForDL = 0;
  ControlMsg.CommMode = MODE_INIT;
}

void ChangeLanguage(void)
{
  if(RobotCfg.Language == LANGUAGE_ENG) RobotCfg.Language = LANGUAGE_KOR;
  else if(RobotCfg.Language == LANGUAGE_KOR) RobotCfg.Language = LANGUAGE_CHN;
//중국어
//  else if(RobotCfg.Language == LANGUAGE_CHN) RobotCfg.Language = LANGUAGE_ENG;    //210302
  WriteRobotCfg(&RobotCfg);
}

void ImportDelayTime(void)
{
  if((ControlMsg.TimerDisp[DOWN_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == DOWN_TIME))
    MotionDelayTemp.DownDelay = ((MotionDelayTemp.DownDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[KICK_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == KICK_TIME))
    MotionDelayTemp.KickDelay = ((MotionDelayTemp.KickDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[EJECTOR_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == EJECTOR_TIME))
    MotionDelayTemp.EjectorDelay = ((MotionDelayTemp.EjectorDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[CHUCK_TIME] == USE)  && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == CHUCK_TIME))
    MotionDelayTemp.ChuckDelay = ((MotionDelayTemp.ChuckDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[KICK_RETURN_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == KICK_RETURN_TIME))
    MotionDelayTemp.KickReturnDelay = ((MotionDelayTemp.KickReturnDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[UP_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == UP_TIME))
    MotionDelayTemp.UpDelay = ((MotionDelayTemp.UpDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[SWING_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == SWING_TIME))
    MotionDelayTemp.SwingDelay = ((MotionDelayTemp.SwingDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[SECOND_DOWN_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == SECOND_DOWN_TIME))
    MotionDelayTemp.Down2ndDelay = ((MotionDelayTemp.Down2ndDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[RELEASE_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == RELEASE_TIME))
    MotionDelayTemp.OpenDelay = ((MotionDelayTemp.OpenDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[SECOND_UP_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == SECOND_UP_TIME))
    MotionDelayTemp.Up2ndDelay = ((MotionDelayTemp.Up2ndDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[CHUCK_ROTATION_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == CHUCK_ROTATION_TIME))
    MotionDelayTemp.ChuckRotateReturnDelay = ((MotionDelayTemp.ChuckRotateReturnDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[SWING_RETURN_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == SWING_RETURN_TIME))
    MotionDelayTemp.SwingReturnDelay = ((MotionDelayTemp.SwingReturnDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[NIPPER_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == NIPPER_TIME))
    MotionDelayTemp.NipperOnDelay = ((MotionDelayTemp.NipperOnDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[CONVEYOR_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == CONVEYOR_TIME))
    MotionDelayTemp.ConveyorDelay = ((MotionDelayTemp.ConveyorDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
}

void ActiveRun(void)
{
  
  static uint8_t BootingRetry = 0;
  static uint8_t PageChange = 0;
  
    //Mold List Ascending order
  uint16_t ExistMoldNoTempTemp = 0;
  uint16_t ExistMoldNoTemp = 0;
  uint16_t IndexTemp = 0;
  
  //Tick
  static uint64_t RTCTempTick = 0;
  static uint64_t LoadingTempTick = 0;
  static uint64_t MoldOpenTempTick = 0;
  
  
  //Mold mange (?)
  static uint16_t ExistMoldNoIndex = 1;
  static uint16_t CheckMoldNo = 1;
  static char CheckMoldName[MAX_MOLDNAME_LENGTH];
  
  char ExistMoldNameTemp[MAX_MOLDNAME_LENGTH];      //MAX_MOLDNAME_LENGTH 10
  char ExistMoldNameTempTemp[MAX_MOLDNAME_LENGTH];
  


  SDRES SdRes;   //enum형
  SDRES SdResTemp; 
  
    switch(ControlMsg.TpMode)
	{
    case INIT_READ_TYPE:
      //After 1s
      if(!(ChkExpireSysTick(&LoadingTempTick))) break;
      SdRes = ChkSD();
      if(SdRes == SD_OK)
      {
        BootingRetry = 0;
        SetSysTick(&LoadingTempTick, TIME_LOAD_DELAY);
        ControlMsg.TpMode = INIT_DISP_LOGO;
      }
      //Retry #3, Delay 300ms
      else
      {
        if(!(ChkExpireSysTick(&LoadingTempTick))) break;
        BootingRetry++;
        SetSysTick(&LoadingTempTick, TIME_LOAD_DELAY);
        if(BOOTING_ERROR_COUNT < BootingRetry)     // 3<BootingRetry  4번  try 
        {
          if(SdRes == SD_SLOT_EMPTY)
          {
            BootingRetry = 0;
            RobotCfg.ErrorState = ERROR_STATE_OCCUR;
            RobotCfg.ErrCode = ERRCODE_SD_EMPTY;
          }
          else if(SdRes == SD_SLOT_WR_PROTECT)
          {
            BootingRetry = 0;
            RobotCfg.ErrorState = ERROR_STATE_OCCUR;
            RobotCfg.ErrCode = ERRCODE_SD_LOCKING;
          }
        }
      }
      break;
      //INIT_DISP_LOGO//INIT_DISP_LOGO//INIT_DISP_LOGO//INIT_DISP_LOGO//INIT_DISP_LOGO//INIT_DISP_LOGO
    case INIT_DISP_LOGO:
      //Mount SD
      SdRes = MountSD();
      SetSysTick(&SDTempTick, TIME_SC_DELAY);
      ControlMsg.TpMode = INIT_LOAD_SD;
      break;
      //INIT_LOAD_SD//INIT_LOAD_SD//INIT_LOAD_SD//INIT_LOAD_SD//INIT_LOAD_SD//INIT_LOAD_SD//INIT_LOAD_SD
    case INIT_LOAD_SD:
      //Mount SD value confirm
      if(SdRes == SD_OK)
      {
        SdRes = ReadRobotCfg(&RobotCfg);
        if(RobotCfg.MoldNo == 0)
        {
          SdResTemp = ReadRobotCfgBackUp(&RobotCfg);
          if(SdResTemp == SD_OK)  //sd카드 내 파일 불러오기 성공
          {
            BootingRetry = 0;
            RobotCfg.RobotType = ReadType();
            WriteRobotCfg(&RobotCfg);
            MoldMsg.MoldNo = RobotCfg.MoldNo;
            ReadMold(&MoldMsg,RobotCfg.MoldNo);
            TimerDispCheck();
            ModeDispCheck(MoldMsg.MoldNo);
            StepCheck();
            MakeSequence();
            ControlMsg.CommMode = MODE_INIT;
            
            RobotCfg.Setting.IMMType = STANDARD;
            RobotCfg.Setting.RotationState = ROTATION;
            
            ControlMsg.TpMode = INIT_SC;
            SetSysTick(&LoadingTempTick, TIME_SC_BOOTING);				
            SetSysTick(&SDTempTick, TIME_SC_DELAY);
          }
			 //SdResTemp != SD_OK 불러오기 실패
          else
          {
            BootingRetry = 0;
            RobotCfg.ErrorState = ERROR_STATE_OCCUR;
            RobotCfg.ErrCode = ERROR_STATE_EMO;
          }
        }
		  //RobotCfg.MoldNo != 0
        else       
        {
          if(SdRes == SD_OK)            //중복 조건 인듯하다 
          {
            BootingRetry = 0;
            RobotCfg.RobotType = ReadType();
            //				if (!(ChkExpireSysTick(&SDTempTick))) break;
            //			 OpenMold(RobotCfg.MoldNo);
            MoldMsg.MoldNo = RobotCfg.MoldNo;
            ReadMold(&MoldMsg,RobotCfg.MoldNo);
            TimerDispCheck();
            ModeDispCheck(MoldMsg.MoldNo);
            StepCheck();
            MakeSequence();
            ControlMsg.CommMode = MODE_INIT;
            //IMMType
            if(!((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)))
            {
              //				RobotCfg.Language = LANGUAGE_ENG;
              RobotCfg.Setting.IMMType = STANDARD;
              RobotCfg.Setting.RotationState = ROTATION;
            }
            ControlMsg.TpMode = INIT_SC;
            SetSysTick(&LoadingTempTick, TIME_SC_BOOTING);				
            SetSysTick(&SDTempTick, TIME_SC_DELAY);
          }
          else    //(SdRes != SD_OK)
          {
            BootingRetry = 0;
            RobotCfg.ErrorState = ERROR_STATE_OCCUR;
            RobotCfg.ErrCode = ERRCODE_SD_NOT_FILE;
          }
        }
      }
      else	        //(SdRes != SD_OK)
      {
        //Retry #3, Delay 300ms
        if(!(ChkExpireSysTick(&LoadingTempTick))) break;
        BootingRetry++;
        SetSysTick(&LoadingTempTick, TIME_LOAD_DELAY);
        if(BootingRetry < BOOTING_ERROR_COUNT) SdRes = MountSD();
        else         //(BootingRetry >= BOOTING_ERROR_COUNT)
        {
          BootingRetry = 0;
          RobotCfg.ErrorState = ERROR_STATE_OCCUR;
          RobotCfg.ErrCode = ERRCODE_SD_DISABLE;
        }
      }
      break;
      //INIT_SC//INIT_SC//INIT_SC//INIT_SC//INIT_SC//INIT_SC//INIT_SC//INIT_SC
    case INIT_SC:
      if(!(ChkExpireSysTick(&LoadingTempTick))) break;
      MoldMsg.ExistMold[0] = NEW_MOLD;
      ControlMsg.CommMode = MODE_SEQ;
      ControlMsg.TpMode = INIT_MOLD_LIST_UP;
      break;
      //INIT_MOLD_LIST_UP//INIT_MOLD_LIST_UP//INIT_MOLD_LIST_UP//INIT_MOLD_LIST_UP//INIT_MOLD_LIST_UP
    case INIT_MOLD_LIST_UP:
      //Mold File ListUp
      //Checking SDcard Mold No
      if(CheckMoldNo < MAX_MOLDNO_LENGTH+1)      //CheckMoldNo < 100
      {
        SdRes = CheckExistMoldFile
			 (CheckMoldNo,CheckMoldName);	
		  
		  
        if(SdRes == SD_FILE_EXIST)
        {
          MoldMsg.ExistMold[ExistMoldNoIndex] = CheckMoldNo;
          for(uint8_t MoldNameChar=0 ; MoldNameChar < MAX_MOLDNAME_LENGTH+1 ; MoldNameChar++)        //0~10?? 0~9여야하지않나?
          {
            MoldMsg.ExistMoldName[ExistMoldNoIndex][MoldNameChar] = CheckMoldName[MoldNameChar];
          }
          ExistMoldNoIndex++;
        }
        CheckMoldNo++;
        if(CheckMoldNo == MAX_MOLDNO_LENGTH+1) MoldMsg.MoldCount = ExistMoldNoIndex;
      }
      if(MoldMsg.MoldCount == ExistMoldNoIndex)
      {
        CheckMoldNo = 1;
        ExistMoldNoIndex = 1;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
		  
		  
        //To go SETTING_MANUFACTURER Page 
        if(ControlMsg.KeyValue == KEY_MANUFACTURER)
        {
          UnderbarPos = 0;
          memcpy(&SettingTemp, &(RobotCfg.Setting), sizeof(MANUFACTURER_SETTING));
          ControlMsg.TpMode = SETTING_MANUFACTURER;
        }
		  
		  //ControlMsg.KeyValue != KEY_MANUFACTURER
        else
        {
          if((RobotCfg.Setting.DoorSignalChange == USE) && (RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_OFF))
          {
            RobotCfg.ErrCode = MANUAL_ERR_SAFTYDOOR_CHANGE;
            ControlMsg.TpMode = MANUAL_ERROR;
          }
          else
          {
            if(ControlMsg.fHomingDone == HOMING_DONE) 
            {
              ControlMsg.CommStateForDL = 2;
              ControlMsg.TpMode = MANUAL_OPERATING;
            }
            else 
            {
              ControlMsg.TpMode = MANUAL_HOMING;
              PageChange = PAGE_MANUAL;
            }
          }
        }
		  
		  
        ClearPage();
      }
      break;
      
      //Complete	Booting	//Complete	Booting	//Complete	Booting	//Complete	Booting	//Complete	Booting	//Complete	Booting	//Complete	Booting	//Complete	Booting	
      //MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING
    case MANUAL_OPERATING:
      //SC CommMode
      if(ControlMsg.CommStateForDL == 1) ControlMsg.CommMode = MODE_SEQ;
      else if(ControlMsg.CommStateForDL == 2)
      {
        ControlMsg.CommMode = MODE_MANUAL;
        ControlMsg.CommStateForDL = 0xFF;
      }
      //HomingState
      if(ControlMsg.fHomingDone == HOMING_DONE) ControlMsg.fHomingDone = HOMING_START;
      
      //Interlock
      ManualInterlock();
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      
      //IO
      ManualIO();
      
      //Page Change
      if(ControlMsg.KeyValue == KEY_MENU_AUTO)//KEY_MENU_AUTO//KEY_MENU_AUTO//KEY_MENU_AUTO//KEY_MENU_AUTO
      {
        if((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_OFF))
        {
          RobotCfg.ErrCode = MANUAL_ERR_SAFTYDOOR_AUTO;
          ControlMsg.TpMode = MANUAL_ERROR;
        }
        else if((RobotCfg.Setting.ItlSaftyDoor == USE) && (RobotCfg.Setting.ItlAutoInjection == USE))
        {
          if((IOMsg.X18_MoldOpenComplete == SIGNAL_ON) && (AutoInjectionTemp == SIGNAL_ON))
          {
            if(IOMsg.Y2A_MoldOpenClose == SIGNAL_OFF)
            {
              RobotCfg.ErrCode = MANUAL_ERR_MOLD;
              ControlMsg.TpMode = MANUAL_ERROR;
            }
          }
          else ControlMsg.TpMode = AUTO_MESSAGE_INIT;
        }
        else
        {
          ControlMsg.fHomingDone = HOMING_START;
          ControlMsg.TpMode = AUTO_MESSAGE_INIT;
        }
      }
      else if(ControlMsg.KeyValue == KEY_MENU_MODE)
      {
        ClearPage();
        memcpy(&MotionModeTemp, &(RobotCfg.MotionMode), sizeof(MOTION_MODE));
        memset(ControlMsg.ModeDisp,0,sizeof(ControlMsg.ModeDisp));
        memset(ControlMsg.ModeDispTemp,0,sizeof(ControlMsg.ModeDispTemp));
        ModeDispCheck(RobotCfg.MoldNo);
        ControlMsg.TpMode = MANUAL_MODE;
      }
      else if(ControlMsg.KeyValue == KEY_MENU_TIMER)
      {
        ClearPage();
        memcpy(&MotionDelayTemp, &(RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
        memset(ControlMsg.TimerDisp,0,sizeof(ControlMsg.TimerDisp));
        memset(ControlMsg.TimerDispTemp,0,sizeof(ControlMsg.TimerDispTemp));
        TimerDispCheck();
        ControlMsg.TpMode = MANUAL_TIMER;
      }
      else if(ControlMsg.KeyValue == KEY_MENU_COUNTER)
      {
        ClearPage();
        memcpy(&CountMsg, &(RobotCfg.Count), sizeof(COUNT_MSG));
        ControlMsg.TpMode = MANUAL_COUNTER;
      }
      else if(ControlMsg.KeyValue == KEY_MENU_IO)
      {
        ClearPage();
        ControlMsg.TpMode = MANUAL_IO;
      }
      else if(ControlMsg.KeyValue == KEY_MENU_STEP)
      {
        if((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_OFF))
        {
          RobotCfg.ErrCode = MANUAL_ERR_SAFTYDOOR_STEP;
          ControlMsg.TpMode = MANUAL_ERROR;
        }
        else
        {
          StepCheck();
          StepCursor = DISPLAY_OFF;
          ControlMsg.CommMode = MODE_STEP;
          ControlMsg.TpMode = MANUAL_HOMING;
          PageChange = PAGE_STEP;
        }
      }
      else if(ControlMsg.KeyValue == KEY_MENU_CYCLE)
      {
        if((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_OFF))
        {
          RobotCfg.ErrCode = MANUAL_ERR_SAFTYDOOR_CYCLE;
          ControlMsg.TpMode = MANUAL_ERROR;
        }
        else
        {
          memcpy(&CycleDelay, &(RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
          StepCheck();
          ControlMsg.CommMode = MODE_CYCLE;
          ControlMsg.TpMode = MANUAL_HOMING;
          PageChange = PAGE_CYCLE;
        }
      }
      else if(ControlMsg.KeyValue == 0)
      {
        ClearPage();
        MoldNoTemp = 0;
        ControlMsg.TpMode = MANUAL_MOLD_SEARCH;
      }
      else if(ControlMsg.KeyValue == KEY_ERROR_LOG)
      {
        ClearPage();
        ReadError(ErrMsgTemp);
        ErrMsg.ErrorCount = CountErr();
        ControlMsg.TpMode = MANUAL_ERROR_LIST;
      }
      else if(ControlMsg.KeyValue == KEY_DISP_VERSION)
      {
        ClearPage();
        ControlMsg.TpMode = MANUAL_VERSION;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      
      //Function
      else if(ControlMsg.KeyValue == KEY_ALARM)
      {
        RobotCfg.Function.Buzzer ^= FUNC_ON;
        WriteFunction();
        ChangeSCMode();
        fPreviousPage = MANUAL_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(ControlMsg.KeyValue == KEY_DETECT)
      {
        RobotCfg.Function.Detection ^= FUNC_ON;
        WriteFunction();
        ChangeSCMode();
        fPreviousPage = MANUAL_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(ControlMsg.KeyValue == KEY_EJECTOR)
      {
        RobotCfg.Function.Ejector ^= FUNC_ON;
        WriteFunction();
        ChangeSCMode();
        fPreviousPage = MANUAL_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(ControlMsg.KeyValue == KEY_REJECT)
      {
        RobotCfg.Function.Reject ^= FUNC_ON;
        WriteFunction();
        ChangeSCMode();
        fPreviousPage = MANUAL_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        RobotCfg.ErrorState = ERROR_STATE_NONE;
        ChangeSCMode();
        ClearPage();
      }
      break;
      //MANUAL_HOMING//MANUAL_HOMING//MANUAL_HOMING//MANUAL_HOMING//MANUAL_HOMING
    case MANUAL_HOMING :
      if(ControlMsg.CommStateForDL == 1)
      {
        ControlMsg.fDoHoming = 0;
        ControlMsg.CommMode = MODE_SEQ;
      }
      if((PageChange == PAGE_MANUAL) && (ControlMsg.fHomingDone == HOMING_DONE))
      {
        ControlMsg.fDoHoming = 0;
        ControlMsg.CommStateForDL = 2;
        ControlMsg.TpMode = MANUAL_OPERATING;
//        PageChange = 0;
		  PageChange = PAGE_NONE;   //2103032 0->PAGE_NONE
      }
      else if((PageChange == PAGE_STEP) && (ControlMsg.fHomingDone == HOMING_DONE))
      {
        ControlMsg.fDoHoming = 0;
        //		  ControlMsg.StepSeq = SeqMsg.SeqDispArr[SeqMsg.SeqDispPos];
        PageChange = PAGE_NONE;
        ControlMsg.TpMode = MANUAL_STEP_OPERATING;
      }
      else if((PageChange == PAGE_CYCLE) && (ControlMsg.fHomingDone == HOMING_DONE))
      {
        ControlMsg.fDoHoming = 0;
        PageChange = PAGE_NONE;
        ControlMsg.TpMode = MANUAL_CYCLE_OPERATING;
      }
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue  == KEY_MENU_STOP)
      {
        ControlMsg.fDoHoming = 0;
        ChangeSCMode();
        PageChange = PAGE_NONE;
        ControlMsg.TpMode = MANUAL_OPERATING;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      //AUTO_MESSAGE_INIT//AUTO_MESSAGE_INIT//AUTO_MESSAGE_INIT//AUTO_MESSAGE_INIT//AUTO_MESSAGE_INIT
    case AUTO_MESSAGE_INIT:
      memcpy(&CycleDelay, &(RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
      TimerDispCheck();
      ModeDispCheck(RobotCfg.MoldNo);
      StepCheck();
      ControlMsg.TpMode = AUTO_MESSAGE;
      break;
      //AUTO_MESSAGE//AUTO_MESSAGE//AUTO_MESSAGE//AUTO_MESSAGE//AUTO_MESSAGE//AUTO_MESSAGE//AUTO_MESSAGE
    case AUTO_MESSAGE:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_AUTO)
      {
        if((RobotCfg.Setting.ItlAutoInjection == USE) && (IOMsg.X19_AutoInjection == SIGNAL_ON))
          IOMsg.Y29_CycleStart = SIGNAL_OFF;
        //		  else IOMsg.Y29_CycleStart = SIGNAL_ON;
        //		  if (RobotCfg.Function.Ejector == FUNC_ON) IOMsg.Y2B_Ejector = SIGNAL_OFF;
        ControlMsg.TpMode = AUTO_OPERATING;
        ControlMsg.CommMode = MODE_AUTO;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ControlMsg.TpMode = MANUAL_OPERATING;
        ControlMsg.CommStateForDL = 2;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      //AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING
    case AUTO_OPERATING:
      if(ControlMsg.fHomingDone == HOMING_DONE)
        AutoState(MODE_AUTO_OP);				//To continue to auto-operate even when the page changes
      else if((ControlMsg.fHomingDone == HOMING_START) && (ControlMsg.KeyValue == KEY_MENU_STOP)) 
      {
        ClearAuto();
        ControlMsg.TpMode = MANUAL_OPERATING;		  //201123
        ControlMsg.CommStateForDL = 2;
        //PageChange = 0;
		  PageChange = PAGE_NONE;                     //2103032 0->PAGE_NONE
      }
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_MODE)
      {
        ClearPage();
        memcpy(&MotionModeTemp, &(RobotCfg.MotionMode), sizeof(MOTION_MODE));
        memset(ControlMsg.ModeDisp,0,sizeof(ControlMsg.ModeDisp));
        memset(ControlMsg.ModeDispTemp,0,sizeof(ControlMsg.ModeDispTemp));
        ModeDispCheck(RobotCfg.MoldNo);
        ControlMsg.TpMode = AUTO_MODE;
      }
      else if(ControlMsg.KeyValue == KEY_MENU_IO)
      {
        ControlMsg.TpMode = AUTO_IO;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_MENU_TIMER)
      {
        memcpy(&MotionDelayTemp, &(RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
        memset(ControlMsg.TimerDisp,0,sizeof(ControlMsg.TimerDisp));
        memset(ControlMsg.TimerDispTemp,0,sizeof(ControlMsg.TimerDispTemp));
        TimerDispCheck();
        ControlMsg.TpMode = AUTO_TIMER;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_MENU_COUNTER)
      {
        memcpy(&CountMsg, &(RobotCfg.Count), sizeof(COUNT_MSG));
        ControlMsg.TpMode = AUTO_COUNTER;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      //AUTO_MODE//AUTO_MODE//AUTO_MODE//AUTO_MODE//AUTO_MODE//AUTO_MODE//AUTO_MODE
    case AUTO_MODE:	
      AutoState(MODE_AUTO_OP);					//To continue to auto-operate even when the page changes
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_AUTO)
      {
        ControlMsg.TpMode = AUTO_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT)	InMenuUpButton(MAX_LINE_OUTLINE);
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT)	InMenuDownButton(MAX_LINE_OUTLINE, MAX_LINE_MENU, ControlMsg.ModeDispTemp);
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      //AUTO_COUNTER//AUTO_COUNTER//AUTO_COUNTER//AUTO_COUNTER//AUTO_COUNTER//AUTO_COUNTER
    case AUTO_COUNTER:
      AutoState(MODE_AUTO_OP);					//To continue to auto-operate even when the page changes
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_AUTO)
      {
        ControlMsg.TpMode = AUTO_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.LineNo > 0) ControlMsg.LineNo--;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.LineNo < MAX_LINE_TITLE) ControlMsg.LineNo++;
      }
      else if(ControlMsg.KeyValue == KEY_LONG_CLEAR)
      {
        if(ControlMsg.LineNo == 0) CountMsg.TotalCnt = 0;
        else if(ControlMsg.LineNo == 1) CountMsg.RejectCnt = 0;
        else if(ControlMsg.LineNo == 2) CountMsg.DetectFail = 0;
        memcpy(&(RobotCfg.Count), &CountMsg, sizeof(CountMsg));
        memcpy(&(MoldMsg.Count), &CountMsg, sizeof(CountMsg));
        //		  WriteMold(&MoldMsg);
        WriteRobotCfg(&RobotCfg);
        fPreviousPage = AUTO_COUNTER_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      //AUTO_TIMER//AUTO_TIMER//AUTO_TIMER//AUTO_TIMER//AUTO_TIMER//AUTO_TIMER//AUTO_TIMER
    case AUTO_TIMER:
      AutoState(MODE_AUTO_OP);					//To continue to auto-operate even when the page changes
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_AUTO)
      {
        ControlMsg.TpMode = AUTO_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(ControlMsg.EnableEdit == ENABLE_EDIT)
        {
          memcpy(&MotionDelayTemp, &(RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
          ControlMsg.EnableEdit = DISABLE_EDIT;
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuUpButton(MAX_LINE_TITLE);
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuDownButton(MAX_LINE_TITLE, MAX_LINE_OUTLINE, ControlMsg.TimerDispTemp);
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        ControlMsg.EnableEdit = DISABLE_EDIT;
        memcpy(&(RobotCfg.MotionDelay), &MotionDelayTemp, sizeof(MOTION_DELAY));
        memcpy(&CycleDelay, &MotionDelayTemp, sizeof(MOTION_DELAY));
        memcpy(&(MoldMsg.MotionDelay), &MotionDelayTemp, sizeof(MOTION_DELAY));
        //		  WriteMold(&MoldMsg);
        WriteRobotCfg(&RobotCfg);
        StepCheck();
        fPreviousPage = AUTO_TIMER_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(KeyIsNum(&ControlMsg.KeyValue))
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        ImportDelayTime();
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
		
		
		
      //AUTO_IO//AUTO_IO//AUTO_IO//AUTO_IO//AUTO_IO//AUTO_IO//AUTO_IO
    case AUTO_IO:
      AutoState(MODE_AUTO_OP);					//To continue to auto-operate even when the page changes
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_AUTO)
      {
        ControlMsg.TpMode = AUTO_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.PageNo > 0) ControlMsg.PageNo--;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.PageNo < IO_MAX_PAGE) ControlMsg.PageNo++;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT) ControlMsg.LineNo = IO_OUTPUT;
      else if(ControlMsg.KeyValue == KEY_ARROW_LEFT) ControlMsg.LineNo = IO_INPUT;
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
		
		
		
      //MANUAL_MODE//MANUAL_MODE//MANUAL_MODE//MANUAL_MODE//MANUAL_MODE//MANUAL_MODE
    case MANUAL_MODE:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ControlMsg.fDoHoming = 1;
        ChangeSCMode();
        ControlMsg.TpMode = MANUAL_HOMING;
        PageChange = PAGE_MANUAL;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(ControlMsg.EnableEdit == ENABLE_EDIT)
        {
          memcpy(&MotionModeTemp, &(RobotCfg.MotionMode), sizeof(MOTION_MODE));
          ControlMsg.EnableEdit = DISABLE_EDIT;
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuUpButton(MAX_LINE_OUTLINE);
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuDownButton(MAX_LINE_OUTLINE, MAX_LINE_MENU, ControlMsg.ModeDispTemp);
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        ControlMsg.EnableEdit = DISABLE_EDIT;
        memcpy(&(RobotCfg.MotionMode), &MotionModeTemp, sizeof(MOTION_MODE));
        memcpy(&(MoldMsg.MotionMode), &MotionModeTemp, sizeof(MOTION_MODE));
        memset(SeqMsg.SeqDispArr,0,sizeof(SeqMsg.SeqDispArr));
        memset(ControlMsg.StepDispTemp,0,sizeof(ControlMsg.StepDispTemp));
        memset(SeqMsg.SeqArr,0,sizeof(SeqMsg.SeqArr));
        MakeSequence();
        //		  WriteMold(&MoldMsg);
        WriteRobotCfg(&RobotCfg);
        fPreviousPage = MANUAL_MODE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_LEFT)
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        ImportMode();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT)
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        ImportMode();
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
		
		
      //MANUAL_COUNTER//MANUAL_COUNTER//MANUAL_COUNTER//MANUAL_COUNTER//MANUAL_COUNTER//MANUAL_COUNTER
    case MANUAL_COUNTER:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ControlMsg.TpMode = MANUAL_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.LineNo > 0) ControlMsg.LineNo--;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.LineNo < MAX_LINE_TITLE) ControlMsg.LineNo++;
      }
      else if(ControlMsg.KeyValue == KEY_LONG_CLEAR)
      {
        if(ControlMsg.LineNo == 0) CountMsg.TotalCnt = 0;
        else if(ControlMsg.LineNo == 1) CountMsg.RejectCnt = 0;
        else if(ControlMsg.LineNo == 2) CountMsg.DetectFail = 0;
        memcpy(&(RobotCfg.Count), &CountMsg, sizeof(CountMsg));
        memcpy(&(MoldMsg.Count), &CountMsg, sizeof(CountMsg));
        //		  WriteMold(&MoldMsg);
        WriteRobotCfg(&RobotCfg);
        ControlMsg.CommMode = MODE_SEQ;
        fPreviousPage = MANUAL_COUNTER_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
		
		
		
      //MANUAL_TIMER//MANUAL_TIMER//MANUAL_TIMER//MANUAL_TIMER//MANUAL_TIMER//MANUAL_TIMER
    case MANUAL_TIMER:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ChangeSCMode();
        ControlMsg.TpMode = MANUAL_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(ControlMsg.EnableEdit == ENABLE_EDIT)
        {
          memcpy(&MotionDelayTemp, &(RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
          ControlMsg.EnableEdit = DISABLE_EDIT;
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuUpButton(MAX_LINE_TITLE);
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuDownButton(MAX_LINE_TITLE, MAX_LINE_OUTLINE, ControlMsg.TimerDispTemp);
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        ControlMsg.EnableEdit = DISABLE_EDIT;
        memcpy(&(RobotCfg.MotionDelay), &MotionDelayTemp, sizeof(MOTION_DELAY));
        memcpy(&CycleDelay, &MotionDelayTemp, sizeof(MOTION_DELAY));
        memcpy(&(MoldMsg.MotionDelay), &MotionDelayTemp, sizeof(MOTION_DELAY));
        MakeSequence();
        //		  WriteMold(&MoldMsg);
        WriteRobotCfg(&RobotCfg);
        fPreviousPage = MANUAL_TIMER_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(KeyIsNum(&ControlMsg.KeyValue))
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        ImportDelayTime();
      }  
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
		
		
		
      //MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO
    case MANUAL_IO:
      ManualInterlock();
      if(ControlMsg.KeyReady == KEY_READY) break;
      ManualIO();
      if(ControlMsg.KeyValue == KEY_MENU_STOP) 
      {
        ClearPage();
        ControlMsg.TpMode = MANUAL_OPERATING;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.PageNo > 0) ControlMsg.PageNo--;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.PageNo < IO_MAX_PAGE) ControlMsg.PageNo++;         // ControlMsg.PageNo < 5
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT) ControlMsg.LineNo = IO_OUTPUT;
      else if(ControlMsg.KeyValue == KEY_ARROW_LEFT) ControlMsg.LineNo = IO_INPUT;
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
		
		
      //MANUAL_STEP_OPERATING//MANUAL_STEP_OPERATING//MANUAL_STEP_OPERATING//MANUAL_STEP_OPERATING
    case MANUAL_STEP_OPERATING:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ControlMsg.CommStateForDL = 2;
        ControlMsg.TpMode = MANUAL_OPERATING;
        SeqMsg.SeqDispPos = 0;
        ControlMsg.StepSeq = 0;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if (IOMsg.X18_MoldOpenComplete == SIGNAL_ON)
        {
          if (SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] == SEQ_END)
          {
            ControlMsg.StepSeq = 0;
            SeqMsg.SeqDispPos = 0;
          }
          if(SeqMsg.SeqPos == ControlMsg.StepSeq)
          {
            SeqMsg.SeqDispPos++;
            ControlMsg.StepSeq = SeqMsg.SeqDispArr[SeqMsg.SeqDispPos];
            //Last page and Last line --> page line reset. restart
            if ((ControlMsg.PageNo == ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX]/MAX_LINE_OUTLINE) &&
                (ControlMsg.LineNo == ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX]%MAX_LINE_OUTLINE-1))
              ClearPage();
            else if(ControlMsg.LineNo < MAX_LINE_TITLE)
            {
              //last page and last line check
              if ((ControlMsg.PageNo*MAX_LINE_OUTLINE  + ControlMsg.LineNo) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX] -1)
              {
                if (StepCursor == DISPLAY_ON) ControlMsg.LineNo++; 
              }
            }
            else
            { 
              //last page check
              if ((ControlMsg.PageNo == ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX]/MAX_LINE_OUTLINE -1) &&
                  (ControlMsg.LineNo == MAX_LINE_TITLE) && (ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX]%MAX_LINE_OUTLINE == 0))
                ClearPage();		//page, line reset
              else
              {
                //next page
                ControlMsg.PageNo++;
                ControlMsg.LineNo = 0;
              }
            }
          }
        }
        else ControlMsg.KeyValue = KEY_NONE;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      //MANUAL_CYCLE_OPERATINGMANUAL_CYCLE_OPERATINGMANUAL_CYCLE_OPERATINGMANUAL_CYCLE_OPERATING
    case MANUAL_CYCLE_OPERATING: 
      AutoState(MODE_CYCLE_OP);
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      //MANUAL_MOLD_SEARCH//MANUAL_MOLD_SEARCH//MANUAL_MOLD_SEARCH//MANUAL_MOLD_SEARCH//MANUAL_MOLD_SEARCH
    case MANUAL_MOLD_SEARCH:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ClearPage();
        ControlMsg.TpMode = MANUAL_OPERATING;
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        MoldMsg.MoldNo = MoldNoTemp;
        //Check Mold No
        if((MoldMsg.MoldNo != NEW_MOLD) && (CheckExistMoldFile(MoldMsg.MoldNo,MoldMsg.MoldName) == SD_DO_NOT_EXIST_FILE))
        {
          SetSysTick(&MoldOpenTempTick, TIME_MOLD_OPEN_ERROR);
          RobotCfg.ErrCode = MANUAL_ERR_NO_MOLDNO;
          ClearPage();
          ControlMsg.TpMode = MANUAL_ERROR;
          MoldMsg.MoldNo = 0;
          
          //Don't Exist Mold File. Move Cursor near MoldNo
          for (ExistMoldNoIndex = 1; ExistMoldNoIndex <= MoldMsg.MoldCount-2; ExistMoldNoIndex++)
          {
            if ((abs(MoldNoTemp - MoldMsg.ExistMold[ExistMoldNoIndex]) > (abs(MoldNoTemp - MoldMsg.ExistMold[ExistMoldNoIndex+1]))))
              IndexTemp =  ExistMoldNoIndex+1;
          }
          ControlMsg.PageNo = IndexTemp/MAX_LINE_OUTLINE;
          ControlMsg.LineNo = IndexTemp%MAX_LINE_OUTLINE;
        }
        else
        {
          //Exist Mold File. Move Cursor MoldNo
          for(ExistMoldNoIndex = 0 ; ExistMoldNoIndex <= MoldMsg.MoldCount-1 ; ExistMoldNoIndex++)
          {
            if(MoldMsg.ExistMold[ExistMoldNoIndex] == MoldMsg.MoldNo)
            {
              ControlMsg.PageNo = ExistMoldNoIndex/MAX_LINE_OUTLINE;
              ControlMsg.LineNo = ExistMoldNoIndex%MAX_LINE_OUTLINE;
            }
          }
          ControlMsg.TpMode = MANUAL_MOLD_MANAGE;
        }
        ExistMoldNoIndex = 1;
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR) MoldNoTemp = 0;
      else if(KeyIsNum(&ControlMsg.KeyValue))
      {
        MoldNoTemp = ((MoldNoTemp)*10 + KeyNum(&ControlMsg.KeyValue));
        if(MoldNoTemp > MAX_MOLDNO_LENGTH) MoldNoTemp = MoldNoTemp % (MAX_MOLDNO_LENGTH +1);
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      
      break;
      //MANUAL_MOLD_MANAGE//MANUAL_MOLD_MANAGE//MANUAL_MOLD_MANAGE//MANUAL_MOLD_MANAGE
    case MANUAL_MOLD_MANAGE:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ClearPage();
        ControlMsg.TpMode = MANUAL_OPERATING;
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        //Open Mold No                                                                                                                                                                                            ];   //TITLE_LINE_MAX 3
        MoldMsg.MoldNo = MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+ControlMsg.LineNo];
        //Create New Mold(Mode Page)
        if(MoldMsg.MoldNo == NEW_MOLD)
        {
          MoldMsg.MoldNo = MoldNoTemp;
          MoldInit();
          memcpy(&MoldMsg.MotionMode, &(MotionModeTemp), sizeof(MOTION_MODE));
          memset(ControlMsg.ModeDisp,0,sizeof(ControlMsg.ModeDisp));
          memset(ControlMsg.ModeDispTemp,0,sizeof(ControlMsg.ModeDispTemp));
          ModeDispCheck(MoldMsg.MoldNo);
          CheckMoldNo = POSSIBLE_MOLD_NO;
          ControlMsg.TpMode = MANUAL_MOLD_MODE;
          ClearPage();
        }
		  
		  
		  //MoldMsg.MoldNo != NEW_MOLD
        else
        {
			 
          if((MoldMsg.MoldNo == 23) || (MoldMsg.MoldNo == 33))
          {
            if(RobotCfg.RobotType != ROBOT_TYPE_A) 
            {
              OpenMold(MoldMsg.MoldNo);
              fPreviousPage = MANUAL_OPENMOLD_SD;
				  ControlMsg.TpMode = SD_WRITE;
				  //				  WriteRobotCfg(&RobotCfg);
				  //				  ChangeSCMode();
				  //				  ControlMsg.fDoHoming = 1;
				  //				  ControlMsg.TpMode = MANUAL_HOMING;
				  //				  PageChange = PAGE_MANUAL;
				  //				  ClearPage();
				}
            else
            {
              RobotCfg.ErrCode = MANUAL_ERR_ROBOT_TYPE;
              ControlMsg.TpMode = MANUAL_ERROR;
            }
          }
			 
			 
			 
          else if((MoldMsg.MoldNo == 36) || (MoldMsg.MoldNo == 37))
          {
            if((RobotCfg.RobotType == ROBOT_TYPE_XC) || (RobotCfg.RobotType == ROBOT_TYPE_TWIN))
            {
              OpenMold(MoldMsg.MoldNo);
              fPreviousPage = MANUAL_OPENMOLD_SD;
              ControlMsg.TpMode = SD_WRITE;
              //				  WriteRobotCfg(&RobotCfg);
              //				  ChangeSCMode();
              //				  ControlMsg.fDoHoming = 1;
              //				  ControlMsg.TpMode = MANUAL_HOMING;
              //				  PageChange = PAGE_MANUAL;
              //				  ClearPage();
            }
            else
            {
              RobotCfg.ErrCode = MANUAL_ERR_ROBOT_TYPE;
              ControlMsg.TpMode = MANUAL_ERROR;
            }
          }
			 
			 
          else if((MoldMsg.MoldNo == 40) || (MoldMsg.MoldNo == 41) || (MoldMsg.MoldNo == 43) || (MoldMsg.MoldNo == 45) 
                  || (MoldMsg.MoldNo == 82)	 || (MoldMsg.MoldNo == 84))
          {
            if(RobotCfg.RobotType == ROBOT_TYPE_TWIN)
            {
              OpenMold(MoldMsg.MoldNo);
              fPreviousPage = MANUAL_OPENMOLD_SD;
              ControlMsg.TpMode = SD_WRITE;
              //				  WriteRobotCfg(&RobotCfg);
              //				  ChangeSCMode();
              //				  ControlMsg.fDoHoming = 1;
              //				  ControlMsg.TpMode = MANUAL_HOMING;
              //				  PageChange = PAGE_MANUAL;
              //				  ClearPage();
            }
            else //MoldMsg.MoldNo == 82, 84
            {
              RobotCfg.ErrCode = MANUAL_ERR_ROBOT_TYPE;
              ControlMsg.TpMode = MANUAL_ERROR;
            }
          }
			 
			 
          else if((MoldMsg.MoldNo == 22) || (MoldMsg.MoldNo == 32))	//IMMType
          {
            if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.IMMType == VERTI))
            {
              RobotCfg.ErrCode = MANUAL_ERR_ROBOT_TYPE;
              ControlMsg.TpMode = MANUAL_ERROR;
            }
            else
            {
              OpenMold(MoldMsg.MoldNo);
              fPreviousPage = MANUAL_OPENMOLD_SD;
              ControlMsg.TpMode = SD_WRITE;
              //				  WriteRobotCfg(&RobotCfg);
              //				  ChangeSCMode();
              //				  ControlMsg.fDoHoming = 1;
              //				  ControlMsg.TpMode = MANUAL_HOMING;
              //				  PageChange = PAGE_MANUAL;
              //				  ClearPage();
            }
          }
			 
          else
          {
            OpenMold(MoldMsg.MoldNo);
            fPreviousPage = MANUAL_OPENMOLD_SD;
            ControlMsg.TpMode = SD_WRITE;
            //				WriteRobotCfg(&RobotCfg);
            //				ChangeSCMode();
            //				ControlMsg.fDoHoming = 1;
            //				ControlMsg.TpMode = MANUAL_HOMING;
            //				PageChange = PAGE_MANUAL;
            //				ClearPage();
          }
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP) InMenuUpButton(MAX_LINE_TITLE);
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.LineNo < MAX_LINE_TITLE)
        {
          if ((ControlMsg.PageNo*MAX_LINE_OUTLINE  + ControlMsg.LineNo) != MoldMsg.MoldCount -1) ControlMsg.LineNo++;  
        }
        else
        { 
          if (ControlMsg.PageNo != MoldMsg.MoldCount/MAX_LINE_OUTLINE)
          {
            if ((ControlMsg.PageNo == MoldMsg.MoldCount/MAX_LINE_OUTLINE -1) &&
                (ControlMsg.LineNo == MAX_LINE_TITLE) && (MoldMsg.MoldCount%MAX_LINE_OUTLINE == 0));  //210303 이거뭐지?
            else
            {
              ControlMsg.PageNo++;
              ControlMsg.LineNo = 0;
            }
          }
        }
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR) ControlMsg.TpMode = MANUAL_MOLD_DELETE;
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
		
		
      //MANUAL_MOLD_MODE//MANUAL_MOLD_MODE//MANUAL_MOLD_MODE//MANUAL_MOLD_MODE//MANUAL_MOLD_MODE
    case MANUAL_MOLD_MODE:
      if(ControlMsg.KeyReady == KEY_READY) break;
		
		
		
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        //Mold No and Name default setting
        if(ControlMsg.EnableEdit == DISABLE_EDIT)
        {
          for(ExistMoldNoIndex = NEW_MOLD+1 ; ExistMoldNoIndex <= MoldMsg.MoldCount-1 ; ExistMoldNoIndex++)
          {
            if(MoldMsg.ExistMold[ExistMoldNoIndex] >= POSSIBLE_MOLD_NO)
            {
              if(MoldMsg.ExistMold[ExistMoldNoIndex] == CheckMoldNo) CheckMoldNo++;
              else break;
            }
          }
          MoldNoTemp = CheckMoldNo;
          NewMoldInit(MoldNoTemp);
          CheckMoldNo = 1;
          ClearPage();
          ControlMsg.TpMode = MANUAL_MOLD_NEW;
          memcpy(&(MoldMsg.MotionMode), &MotionModeTemp, sizeof(MOTION_MODE));
        }
      }
		
		
		
      else if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(ControlMsg.EnableEdit == ENABLE_EDIT)
        {
          memcpy(&MoldMsg.MotionMode, &(MotionModeTemp), sizeof(MOTION_MODE));
          ControlMsg.EnableEdit = DISABLE_EDIT;
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuUpButton(MAX_LINE_OUTLINE);
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuDownButton(MAX_LINE_OUTLINE, MAX_LINE_MENU, ControlMsg.ModeDispTemp);
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_LEFT)
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        ImportMode();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT)
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        ImportMode();
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        ControlMsg.EnableEdit = DISABLE_EDIT;
        memcpy(&(MoldMsg.MotionMode), &MotionModeTemp, sizeof(MOTION_MODE));
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      //MANUAL_MOLD_NEW//MANUAL_MOLD_NEW//MANUAL_MOLD_NEW//MANUAL_MOLD_NEW//MANUAL_MOLD_NEW
    case MANUAL_MOLD_NEW:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        if(UnderbarPos == NO_BLINK)     //1
        {
          //Mold Load
          MoldMsg.MoldNo = MoldNoTemp;
          for(uint8_t MoldNameChar =0 ; MoldNameChar<= MAX_MOLDNAME_LENGTH ; MoldNameChar++)
          {
            MoldMsg.MoldName[MoldNameChar] = NewMoldName[MoldNameChar];
          }
          WriteMold(&MoldMsg);
          //			 OpenMold(MoldMsg.MoldNo);
          //			 WriteRobotCfg(&RobotCfg);
          ChangeSCMode();
          
          //Ascending order Mold List
          if(MoldMsg.ExistMold[MoldMsg.MoldCount-1] < MoldMsg.MoldNo)		//Last Mold File
          {
            MoldMsg.ExistMold[MoldMsg.MoldCount] = MoldMsg.MoldNo;
            for(uint8_t MoldNameChar =0 ; MoldNameChar <= MAX_MOLDNAME_LENGTH ; MoldNameChar++)
            {
              MoldMsg.ExistMoldName[MoldMsg.MoldCount][MoldNameChar] = MoldMsg.MoldName[MoldNameChar];
            }						
          }
          else
          {
            for (ExistMoldNoIndex = 1; ExistMoldNoIndex <= MoldMsg.MoldCount-2; ExistMoldNoIndex++)
            {
              if ((MoldMsg.ExistMold[ExistMoldNoIndex] < MoldNoTemp) && (MoldNoTemp < MoldMsg.ExistMold[ExistMoldNoIndex+1]))
                IndexTemp =  ExistMoldNoIndex+1;
            }
            ExistMoldNoTemp = MoldMsg.ExistMold[IndexTemp];
            MoldMsg.ExistMold[IndexTemp] = MoldMsg.MoldNo;
            
            for(uint8_t MoldNameChar =0 ; MoldNameChar <= MAX_MOLDNAME_LENGTH ; MoldNameChar++)
            {
              ExistMoldNameTemp[MoldNameChar] = MoldMsg.ExistMoldName[IndexTemp][MoldNameChar];
              MoldMsg.ExistMoldName[IndexTemp][MoldNameChar] = MoldMsg.MoldName[MoldNameChar];
            }
            
            for(uint16_t IndexOffset = 1; IndexOffset <= MoldMsg.MoldCount-1; IndexOffset+=2)
            {
              ExistMoldNoTempTemp = MoldMsg.ExistMold[IndexTemp+IndexOffset];
              MoldMsg.ExistMold[IndexTemp+IndexOffset] = ExistMoldNoTemp;
              ExistMoldNoTemp = MoldMsg.ExistMold[IndexTemp+IndexOffset+1];
              MoldMsg.ExistMold[IndexTemp+IndexOffset+1] = ExistMoldNoTempTemp;
              
              for(uint8_t MoldNameChar = 0; MoldNameChar <= MAX_MOLDNAME_LENGTH; MoldNameChar++)
              {
                ExistMoldNameTempTemp[MoldNameChar] = MoldMsg.ExistMoldName[IndexTemp+IndexOffset][MoldNameChar];
                MoldMsg.ExistMoldName[IndexTemp+IndexOffset][MoldNameChar] = ExistMoldNameTemp[MoldNameChar];
                ExistMoldNameTemp[MoldNameChar] = MoldMsg.ExistMoldName[IndexTemp+IndexOffset+1][MoldNameChar];
                MoldMsg.ExistMoldName[IndexTemp+IndexOffset+1][MoldNameChar] = ExistMoldNameTempTemp[MoldNameChar];
              }
            }
          }
          OpenMold(MoldMsg.MoldNo);
          MoldMsg.MoldCount++;
          IndexTemp = 0;
          ExistMoldNoIndex = 1;
          UnderbarPos = 0;
          ControlMsg.fDoHoming = 1;
          fPreviousPage = MANUAL_NEWMOLD_SD;
          //			 ControlMsg.TpMode = MANUAL_HOMING;
          PageChange = PAGE_MANUAL;
          MoldNamePos = 0;
          MoldNameCharPos = 5;
          ClearPage();
          memset(NewMoldNameTemp,0,sizeof(NewMoldNameTemp));
          memset(&RobotCfg.Count,0,sizeof(RobotCfg.Count));
          memset(&CountMsg,0,sizeof(CountMsg));
          ControlMsg.TpMode = SD_WRITE;
        }
        else
        {
          UnderbarPos = 0;
          ClearPage();
          ControlMsg.TpMode = MANUAL_OPERATING;
        }
      }
      //Numbering MoldNo
      else if(KeyIsNum(&ControlMsg.KeyValue))
      {
        if (UnderbarPos == MOLDNO_BLINK)
        {
          MoldNoTemp = ((MoldNoTemp)*10 + KeyNum(&ControlMsg.KeyValue));
			 //MoldNoTemp>999
          if(MoldNoTemp > MAX_MOLDNO_LENGTH) MoldNoTemp = MoldNoTemp % (MAX_MOLDNO_LENGTH +1);
        }
      }
      //Naming MoldName
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(UnderbarPos == MOLENAME_UNDERBAR_BLINK)
        {
          MoldNameCharPos++;
			 //MoldNameCharPos
          if(MoldNameCharPos > MAX_MOLDNAME_CHAR_POS) MoldNameCharPos = MIN_MOLDNAME_CHAR_POS;
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(UnderbarPos == MOLENAME_UNDERBAR_BLINK)
        {
          if ((MoldNameCharPos == MIN_MOLDNAME_CHAR_POS) && (ControlMsg.KeyValue == KEY_ARROW_DOWM)) 
            MoldNameCharPos = MAX_MOLDNAME_CHAR_POS + 1;
          MoldNameCharPos--;
        }
      }
		
      //Underbar Position
      else if(ControlMsg.KeyValue == KEY_ARROW_LEFT)
      {
        if(UnderbarPos == NO_BLINK) UnderbarPos = MOLENAME_UNDERBAR_BLINK;
        else if(UnderbarPos == MOLENAME_UNDERBAR_BLINK)
        {
          NewMoldNameTemp[MoldNamePos] = MoldNameCharPos;
          if ((MoldNamePos == MIN_MOLDNAME_POS) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
            MoldNamePos = MAX_MOLDNAME_POS;
          MoldNamePos--;
          MoldNameCharPos = NewMoldNameTemp[MoldNamePos];
        }
      }
		
		
      else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT)
      {
        if(UnderbarPos == NO_BLINK) UnderbarPos = MOLENAME_UNDERBAR_BLINK;
        else if(UnderbarPos == MOLENAME_UNDERBAR_BLINK)
        {
          NewMoldNameTemp[MoldNamePos] = MoldNameCharPos;
          MoldNamePos++;
          if(MoldNamePos > MAX_MOLDNAME_POS -1) MoldNamePos = MIN_MOLDNAME_POS;
          MoldNameCharPos = NewMoldNameTemp[MoldNamePos];
        }
      }
		
		
      else if(ControlMsg.KeyValue == KEY_ENTER) 
      {
        if(CheckExistMoldFile(MoldNoTemp,MoldMsg.MoldName) == SD_FILE_EXIST)
          MoldNoTemp = 0;
        if(((MIN_FIX_MOLD_NO <= MoldNoTemp) && (MoldNoTemp <= MAX_FIX_MOLD_NO)) || (MoldNoTemp == NEW_MOLD))
          MoldNoTemp = NEW_MOLD;
        else if(UnderbarPos == MOLENAME_UNDERBAR_BLINK)
        {
          NewMoldNameTemp[MoldNamePos] = MoldNameCharPos;
          UnderbarPos = NO_BLINK;
        }
        else UnderbarPos = NO_BLINK;
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(UnderbarPos == MOLDNO_BLINK)
          MoldNoTemp = 0;
        else if(UnderbarPos == MOLENAME_UNDERBAR_BLINK)
        {
          NewMoldNameTemp[0] = 38;				//' '
          NewMoldNameTemp[1] = 38;				//' '
          NewMoldNameTemp[2] = 38;				//' '
          NewMoldNameTemp[3] = 38;				//' '
          NewMoldNameTemp[4] = 38;				//' '
          NewMoldNameTemp[5] = 38;				//' '
          NewMoldNameTemp[6] = 38;				//' '
          NewMoldNameTemp[7] = 38;				//' '
          MoldNameCharPos = 38;
        }
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_MOLD_DELETE:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ClearPage();
        ControlMsg.TpMode = MANUAL_OPERATING;
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        if((RobotCfg.MoldNo == MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+ControlMsg.LineNo])
           || (MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+ControlMsg.LineNo] < POSSIBLE_MOLD_NO))
        {
          RobotCfg.ErrCode = MANUAL_ERR_MOLD_DELETE;
          ControlMsg.TpMode = MANUAL_ERROR;
        }
        else
        {
          DeleteMoldFile(MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+ControlMsg.LineNo]);
          MoldMsg.ExistMold[0] = NEW_MOLD;										//NewMold MoldNO 0
          ControlMsg.TpMode = MANUAL_MOLD_DELETE_ING;
        }
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_MOLD_DELETE_ING:
      if(CheckMoldNo <= MAX_MOLDNO_LENGTH)
      {
        SdRes = CheckExistMoldFile(CheckMoldNo,CheckMoldName);	
        if(SdRes == SD_FILE_EXIST)
        {
          MoldMsg.ExistMold[ExistMoldNoIndex] = CheckMoldNo;
          for(uint8_t MoldNameChar=0; MoldNameChar<=MAX_MOLDNAME_LENGTH ; MoldNameChar++)
          {
            MoldMsg.ExistMoldName[ExistMoldNoIndex][MoldNameChar] = CheckMoldName[MoldNameChar];
          }
          ExistMoldNoIndex++;
        }
        CheckMoldNo++;
        if(CheckMoldNo == MAX_MOLDNO_LENGTH+1) MoldMsg.MoldCount = ExistMoldNoIndex;
      }
      if((MoldMsg.MoldCount == ExistMoldNoIndex))
      {
        CheckMoldNo = 1;
        ExistMoldNoIndex = 1;
        ControlMsg.TpMode = MANUAL_OPERATING;
        ClearPage();
      }
      if(SettingTemp.DelMoldData == USE)
      {
        RobotCfg.ErrCode = MANUAL_ERR_REBOOTING;
        ControlMsg.TpMode = MANUAL_ERROR;
        SettingTemp.DelMoldData = NO_USE;
        memcpy(&(RobotCfg.Setting), &SettingTemp, sizeof(MANUFACTURER_SETTING));
        WriteRobotCfg(&RobotCfg);
      }
      break;
      
    case MANUAL_VERSION:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP) ControlMsg.TpMode = MANUAL_OPERATING;
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case SETTING_MANUFACTURER:
      if(ControlMsg.EnableEdit == DISABLE_EDIT)
        if(ChkExpireSysTick(&RTCTempTick)) SettingTemp.Time = GetTime();
      if (UnderbarPos == 0) UnderbarPos = YEAR_POS;
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ClearPage();
        UnderbarPos = 0;
        ControlMsg.CommMode = MODE_SEQ;
        if(ControlMsg.fHomingDone == HOMING_START)
        {
          ControlMsg.TpMode = MANUAL_HOMING;
          PageChange = PAGE_MANUAL;
        }
        else if(ControlMsg.fHomingDone == HOMING_DONE) 
        {
          ControlMsg.TpMode = MANUAL_OPERATING;
          PageChange = PAGE_NONE;
        }
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(ControlMsg.EnableEdit == ENABLE_EDIT)
        {
          memcpy(&SettingTemp, &(RobotCfg.Setting), sizeof(MANUFACTURER_SETTING));
          ControlMsg.EnableEdit = DISABLE_EDIT;
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT)
        {
          //Date
          if (ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3)
          {
            UnderbarPos--;
            if (UnderbarPos == DATE_POS) ControlMsg.LineNo--;
          }
          //Time
          else if (ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2)
          {
            UnderbarPos--;
            if (UnderbarPos == 0) ControlMsg.LineNo--;
          }
          else if(ControlMsg.LineNo > 0) ControlMsg.LineNo--;
          else
          {
            if(ControlMsg.PageNo > 0)
            {
              ControlMsg.PageNo--;
              ControlMsg.LineNo = MAX_LINE_OUTLINE;
            }
          }
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT)
        {
          if(ControlMsg.LineNo < MAX_LINE_OUTLINE)
          {
            //Time
            if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2) 
            {
              if (UnderbarPos < HOUR_POS) UnderbarPos++;
              if (UnderbarPos == HOUR_POS) ControlMsg.LineNo++;
            }
            if (!((ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3) || (ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2))) ControlMsg.LineNo++;
            //IMMType
            if((RobotCfg.RobotType == ROBOT_TYPE_XC) || (RobotCfg.RobotType == ROBOT_TYPE_X))
            {
              if(ControlMsg.PageNo == SETTING_MAX_PAGE_IMMTYPE && ControlMsg.LineNo > 0) ControlMsg.LineNo--;	
            }
            else 
            {
              if(ControlMsg.PageNo == SETTING_MAX_PAGE && ControlMsg.LineNo > 2) ControlMsg.LineNo--;
            }
          }
          //Date
          else if (ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3)
          {
            UnderbarPos++;
            if (UnderbarPos == SECOND_POS+1)
            {  
              UnderbarPos = SECOND_POS;
              ControlMsg.LineNo = 0;
              ControlMsg.PageNo++;
            }
          }
          else
          {
            //IMMType
            if((RobotCfg.RobotType == ROBOT_TYPE_XC) ||  (RobotCfg.RobotType == ROBOT_TYPE_X))
            {
              if(ControlMsg.PageNo < SETTING_MAX_PAGE_IMMTYPE)
              {
                ControlMsg.PageNo++;
                ControlMsg.LineNo = 0;
              }
            }
            else
            {
              if(ControlMsg.PageNo < SETTING_MAX_PAGE)
              {
                ControlMsg.PageNo++;
                ControlMsg.LineNo = 0;
              }
            }
          }
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_LEFT)
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 0)
        {
          if(SettingTemp.ErrorTime == 0) SettingTemp.ErrorTime = 10;
          SettingTemp.ErrorTime--;
        }
        else if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 1) SettingTemp.ItlFullAuto ^= USE;
        else if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 2) SettingTemp.ItlSaftyDoor ^= USE;
        else if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 3) SettingTemp.ItlAutoInjection ^= USE;
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 0) SettingTemp.ItlReject ^= USE;
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 1)
        {
          if(SettingTemp.ProcessTime == 0) SettingTemp.ProcessTime = 100;
          SettingTemp.ProcessTime--;
        }
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2)
        {
          if(ControlMsg.CursorPos == 0) ControlMsg.CursorPos = 3;
          ControlMsg.CursorPos--;
        }
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3)
        {
          if(ControlMsg.CursorPos == 0) ControlMsg.CursorPos = 3;
          ControlMsg.CursorPos--;				  
        }
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 0) SettingTemp.DelMoldData ^= USE;
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 1) SettingTemp.DelErrHistory ^= USE;
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 2) SettingTemp.DoorSignalChange ^= USE;
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 3) SettingTemp.IMMType ^= USE;				//IMMType
        else if((ControlMsg.PageNo == 3 && ControlMsg.LineNo == 0) && (SettingTemp.IMMType == USE)) SettingTemp.RotationState ^= NO_ROTATION;		//IMMType
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT)
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 0)
        {
          SettingTemp.ErrorTime++;
          if(SettingTemp.ErrorTime > MAX_TIME_FIND_ERR) SettingTemp.ErrorTime = 0;				  
        }
        else if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 1) SettingTemp.ItlFullAuto ^= USE;
        else if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 2) SettingTemp.ItlSaftyDoor ^= USE;
        else if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 3) SettingTemp.ItlAutoInjection ^= USE;
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 0) SettingTemp.ItlReject ^= USE;
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 1)
        {
          SettingTemp.ProcessTime++;
          if(SettingTemp.ProcessTime > MAX_TIME_PROCESS) SettingTemp.ProcessTime = 0;
        }
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2)
        {
          ControlMsg.CursorPos++;
          if(ControlMsg.CursorPos == 3) ControlMsg.CursorPos = 0;
        }
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3)
        {
          ControlMsg.CursorPos++;
          if(ControlMsg.CursorPos == 3) ControlMsg.CursorPos = 0;				  
        }
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 0) SettingTemp.DelMoldData ^= USE;
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 1) SettingTemp.DelErrHistory ^= USE;
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 2) SettingTemp.DoorSignalChange ^= USE;
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 3) SettingTemp.IMMType ^= USE;				//IMMType
        else if((ControlMsg.PageNo == 3 && ControlMsg.LineNo == 0) && (SettingTemp.IMMType == USE)) SettingTemp.RotationState ^= NO_ROTATION;		//IMMType
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 0)
        {
          if(SettingTemp.DelMoldData == USE)  
          {
            for(uint16_t i=1 ; i < MAX_MOLDNO_LENGTH+1; i++)
            {
              if((POSSIBLE_MOLD_NO-1 < MoldMsg.ExistMold[i]) && (RobotCfg.MoldNo != MoldMsg.ExistMold[i]))
                DeleteMoldFile(MoldMsg.ExistMold[i]);
            }
            ControlMsg.TpMode = MANUAL_MOLD_DELETE_ING;
          }
        }
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 1)
        {
          if(SettingTemp.DelErrHistory == USE)  
          {
            ClearError(); 
            SettingTemp.DelErrHistory = NO_USE;
          }
        }
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2)
        {
          if((SettingTemp.Time.Date.Month < 13) && (SettingTemp.Time.Date.Date < 32))
          {
            SetTime(&(SettingTemp.Time));
            ControlMsg.EnableEdit = DISABLE_EDIT;
          }
        }
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3)
        {
          if((SettingTemp.Time.Time.Hours < 23) && (SettingTemp.Time.Time.Minutes < 60) && (SettingTemp.Time.Time.Seconds < 60))
          {
            SetTime(&(SettingTemp.Time));
            ControlMsg.EnableEdit = DISABLE_EDIT;
          }
        }
        //IMMType
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 3)
        {
          if(SettingTemp.IMMType == NO_USE) SettingTemp.RotationState = ROTATION;
        }
        
        ControlMsg.EnableEdit = DISABLE_EDIT;
        memcpy(&(RobotCfg.Setting), &SettingTemp, sizeof(MANUFACTURER_SETTING));
        memset(SeqMsg.SeqDispArr,0,sizeof(SeqMsg.SeqDispArr));
        memset(ControlMsg.StepDispTemp,0,sizeof(ControlMsg.StepDispTemp));
        memset(SeqMsg.SeqArr,0,sizeof(SeqMsg.SeqArr));
        if(RobotCfg.Setting.RotationState == NO_ROTATION) IMMTypeINIT(RobotCfg.MoldNo);	//IMMType
        MakeSequence();
        SdRes = WriteRobotCfg(&RobotCfg);
        if(SdRes == SD_OK) WriteRobotCfgBuakUp(&RobotCfg);
        ControlMsg.fDoHoming = 1;
        ControlMsg.CommMode = MODE_INIT;
      }
      else if(KeyIsNum(&ControlMsg.KeyValue))
      {
        if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2)
        {
          ControlMsg.EnableEdit = ENABLE_EDIT;
          if(ControlMsg.EnableEdit == ENABLE_EDIT)
          {
            if(UnderbarPos == YEAR_POS)
              SettingTemp.Time.Date.Year = ((SettingTemp.Time.Date.Year)*10 + KeyNum(&ControlMsg.KeyValue)) % 100;
            else if(UnderbarPos == MONTH_POS)
              SettingTemp.Time.Date.Month = ((SettingTemp.Time.Date.Month)*10 + KeyNum(&ControlMsg.KeyValue)) % 100;
            else if(UnderbarPos == DATE_POS)
              SettingTemp.Time.Date.Date = ((SettingTemp.Time.Date.Date)*10 + KeyNum(&ControlMsg.KeyValue)) % 100;
          }
        }
        if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3)
        {
          ControlMsg.EnableEdit = ENABLE_EDIT;
          if(ControlMsg.EnableEdit == ENABLE_EDIT)
          {
            if(UnderbarPos == HOUR_POS)
              SettingTemp.Time.Time.Hours = ((SettingTemp.Time.Time.Hours)*10 + KeyNum(&ControlMsg.KeyValue)) % 100;
            else if(UnderbarPos == MINUTE_POS)
              SettingTemp.Time.Time.Minutes = ((SettingTemp.Time.Time.Minutes)*10 + KeyNum(&ControlMsg.KeyValue)) % 100;
            else if(UnderbarPos == SECOND_POS)
              SettingTemp.Time.Time.Seconds = ((SettingTemp.Time.Time.Seconds)*10 + KeyNum(&ControlMsg.KeyValue)) % 100;
          }
        }
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;	  
      
    case MANUAL_ERROR_LIST:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ControlMsg.TpMode = MANUAL_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.PageNo > 0)
          ControlMsg.PageNo--;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if (ControlMsg.PageNo != ErrMsg.ErrorCount-1)
          ControlMsg.PageNo++;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case AUTO_ERROR_LIST:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ControlMsg.TpMode = MANUAL_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.PageNo > 0)
          ControlMsg.PageNo--;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if (ControlMsg.PageNo != ErrMsg.ErrorCount-1)
          ControlMsg.PageNo++;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_ERROR:
      if(RobotCfg.ErrCode == MANUAL_ERR_REBOOTING) break;
      if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(RobotCfg.ErrCode == MANUAL_ERR_NO_MOLDNO) ControlMsg.TpMode = MANUAL_MOLD_MANAGE;
        else if (RobotCfg.ErrCode != MANUAL_ERR_SAFTYDOOR_CHANGE) ControlMsg.TpMode = MANUAL_OPERATING;
        RobotCfg.ErrorState = ERROR_STATE_NONE;
        ClearPage();
      }
      if(RobotCfg.ErrCode == MANUAL_ERR_NO_MOLDNO)
      {
        if (ControlMsg.KeyValue == KEY_ENTER) ControlMsg.TpMode = MANUAL_MOLD_MANAGE;
        if (!(ChkExpireSysTick(&MoldOpenTempTick))) break;
        RobotCfg.ErrorState = ERROR_STATE_NONE;
        ControlMsg.TpMode = MANUAL_MOLD_MANAGE;
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_SAFTYDOOR_CHANGE)
      {
        if (IOMsg.X1A_SaftyDoor == SIGNAL_ON)
        {
          if(ControlMsg.fHomingDone == HOMING_DONE) 
          {
            ControlMsg.CommStateForDL = 2;
            ControlMsg.TpMode = MANUAL_OPERATING;
            PageChange = PAGE_NONE;
          }
          else 
          {
            ChangeSCMode();
            ControlMsg.TpMode = MANUAL_HOMING;
            PageChange = PAGE_MANUAL;
          }
        }
      }
      if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case OCCUR_ERROR:
      break;
      
    case SD_BACKUP:
      if (!(ChkExpireSysTick(&SDTempTick))) break;
      //		WriteMold(&MoldMsg);
      SdRes = WriteMold(&MoldMsg);
      if(SdRes == SD_OK)
      {
        if(fPreviousPage == MANUAL_PAGE_SD) 
        {
          ControlMsg.TpMode = MANUAL_OPERATING;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
        else if(fPreviousPage == MANUAL_MODE_SD) 
        {
          ControlMsg.TpMode = MANUAL_MODE;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
        else if(fPreviousPage == MANUAL_COUNTER_SD) 
        {
          ControlMsg.TpMode = MANUAL_COUNTER;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
        else if(fPreviousPage == MANUAL_TIMER_SD) 
        {
          ControlMsg.TpMode = MANUAL_TIMER;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
        else if(fPreviousPage == MANUAL_HOMING_SD) 
        {
          ControlMsg.TpMode = MANUAL_HOMING;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          break;
        }
        else if(fPreviousPage == MANUAL_ERROR_SD) 
        {
          ControlMsg.TpMode = OCCUR_ERROR;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          break;
        }
        else if(fPreviousPage == AUTO_PAGE_SD) 
        {
          ControlMsg.TpMode = AUTO_OPERATING;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          break;
        }
        else if(fPreviousPage == AUTO_COUNTER_PAGE_SD) 
        {
          ControlMsg.TpMode = AUTO_COUNTER;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          break;
        }
        else if(fPreviousPage == AUTO_TIMER_PAGE_SD) 
        {
          ControlMsg.TpMode = AUTO_TIMER;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
      }
      //		else 
      //		{
      //		  //Retry #3, Delay 300ms
      //		  if(!(ChkExpireSysTick(&SDTempTick))) break;
      //		  BootingRetry++;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);
      //		  if(BootingRetry < BOOTING_ERROR_COUNT) SdRes = WriteMold(&MoldMsg);
      //		  else
      //		  {
      //			 BootingRetry = 0;
      //			 RobotCfg.ErrorState = ERROR_STATE_OCCUR;
      //			 RobotCfg.ErrCode = ERRCODE_SD_NOT_FILE;
      //		  }
      //		}
      break;
      
    case SD_WRITE:
      if (!(ChkExpireSysTick(&SDTempTick))) break;
      //		WriteRobotCfg(&RobotCfg);
      SdRes = WriteRobotCfg(&RobotCfg);
      if(SdRes == SD_OK)
      {
        if(fPreviousPage == MANUAL_OPENMOLD_SD) 
        {
          ChangeSCMode();
          fPreviousPage = WAITING_SD;
          ControlMsg.fDoHoming = 1;
          ControlMsg.TpMode = MANUAL_HOMING;
          PageChange = PAGE_MANUAL;
          ClearPage();
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
        else if(fPreviousPage == MANUAL_NEWMOLD_SD) 
        {
          ControlMsg.TpMode = MANUAL_HOMING;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
      }
		//		else 
		//		{
		//		  if(fPreviousPage == MANUAL_OPENMOLD_SD) 
		//		  {
		//			 ChangeSCMode();
		//			 fPreviousPage = WAITING_SD;
		//			 ControlMsg.fDoHoming = 1;
		//			 ControlMsg.TpMode = MANUAL_HOMING;
		//			 PageChange = PAGE_MANUAL;
		//			 ClearPage();
		//			 break;
		//		  }
		//		  else if(fPreviousPage == MANUAL_NEWMOLD_SD) 
		//		  {
		//			 ControlMsg.TpMode = MANUAL_HOMING;
		//			 fPreviousPage = WAITING_SD;
		//			 SetSysTick(&SDTempTick, TIME_SC_DELAY);
		//			 break;
		//		  }
		
		//Retry #3, Delay 300ms
		//		  if(!(ChkExpireSysTick(&SDTempTick))) break;
		//		  BootingRetry++;
		//		  SetSysTick(&SDTempTick, TIME_SC_DELAY);
		//		  if(BootingRetry < BOOTING_ERROR_COUNT) SdRes = WriteRobotCfg(&RobotCfg);
		//		  else
		//		  {
		//			 BootingRetry = 0;
		//			 RobotCfg.ErrorState = ERROR_STATE_OCCUR;
		//			 RobotCfg.ErrCode = ERRCODE_SD_NOT_FILE;
		//		  }
		//		}
      break;
    default:
      break;
    }
}


