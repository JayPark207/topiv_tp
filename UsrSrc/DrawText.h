/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DRAWTEXT_H
#define __DRAWTEXT_H

#ifdef	__cplusplus
extern "C" {
#endif


#include "main.h"   
#include "interface.h"
#include "AppLCD.h"
#include "page_msg.h"

  extern void Lcd_Printf(uint32_t x, uint32_t y, uint32_t w, char * str, uint32_t align, uint8_t clear_flag);

#endif	/* __DRAWTEXT_H */