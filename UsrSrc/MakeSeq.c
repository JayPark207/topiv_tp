
/* Includes -------------------------------------------------------------------*/
#include <string.h>
#include "stdlib.h"
#include "AppMain.h"
#include "SDCard.h"
#include "AppKey.h"
#include "ActiveRun.h"
#include "MakeSeq.h"


void MakeSequence(void)
{
  uint8_t MakeSeqPos = 0;
  uint8_t StepSEQPos = 0;
  uint8_t AutoSetpSEQPos = 0;
  
  //Interlock
  if (RobotCfg.Function.Ejector == FUNC_ON)
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_EJECTOR_Y2B_OFF;
  else if (RobotCfg.Function.Ejector == FUNC_OFF)
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_EJECTOR_Y2B_ON;
  
  SeqMsg.SeqArr[MakeSeqPos++] = ITLO_CYCLE_START_Y29_ON_FIRST;
  SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_ON_FIRST;
  SeqMsg.SeqArr[MakeSeqPos++] = ITLI_MOLD_OPENED_X18_OFF;
  SeqMsg.SeqArr[MakeSeqPos++] = ITLO_CYCLE_START_Y29_OFF;
  
  if (RobotCfg.Setting.ItlAutoInjection == USE)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = ITLI_AUTO_INJECTION_X19_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = ITLI_AUTO_INJECTION_X19_OFF;
  }
  
  SeqMsg.SeqArr[MakeSeqPos++] = ITLI_MOLD_OPENED_X18_ON;
  
  if (RobotCfg.Setting.ItlFullAuto == USE)
	 
    SeqMsg.SeqArr[MakeSeqPos++] = ITLI_FULLAUTO_X1H_ON;
  
  SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_OFF;
  
  if (RobotCfg.MotionMode.OutsideWait == USE)
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SWING_RETURN_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING_RETURN;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_RETURN_DONE_X15_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_DONE_X14_OFF;
#ifdef SWING_IO_OFF
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_OFF;
#endif
  }
  //DOWN
  SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
  ControlMsg.StepDispTemp[AutoSetpSEQPos++] = DOWN_STEP;
  SeqMsg.SeqArr[MakeSeqPos++] = DELAY_DOWN;
  if (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_OFF;
  }
  else if (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_OFF;
  }
  else if (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_OFF;
  }
  //Vauum ON
  if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainVaccum == USE))
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_ON;										
  //L type Kick 
  if ((RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE))
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = KICK_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_KICK;					
    
    if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
    {
      if(RobotCfg.MotionMode.MainArmType == ARM_L_TYPE)
      {
        if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
        else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
      }
    }
    if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
    {
      if (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE)
      {
        if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
        else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
      }
    }
  }
  //Ejector
  if (RobotCfg.Function.Ejector == FUNC_ON)
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = EJECTOR_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_EJECTOR;
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_EJECTOR_Y2B_ON;
  }
  //TakeOut
  SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
  ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_STEP;
  SeqMsg.SeqArr[MakeSeqPos++] = DELAY_CHUCK;
  
  if((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainChuck == USE))
  {
    if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
      SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_ON; 
  }
  if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
  {
    if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
      SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_ON;
  }
  
  //Kick Return L type OR Kick U type
  if((RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE))
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = KICK_RETURN_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_KICK_RETURN;
    if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainArmType == ARM_L_TYPE))
    {
      if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
      else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
    }
    if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE))
    {
      if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
      else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
    }
  }
  
  
  //Kick U type
  if ((RobotCfg.MotionMode.MainArmType == ARM_U_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE))
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = KICK_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_KICK;
    if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainArmType == ARM_U_TYPE))
    {
      if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
      else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
    }
    if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE))
    {
      if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
      else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
    }
  }
  //InMold Release
  if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
       && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_INMOLD)
                                                       || ((RobotCfg.Setting.ItlReject == USE)
                                                           && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
           || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_INMOLD)
                                                           || ((RobotCfg.Setting.ItlReject == USE)
                                                               && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))))
      || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_INMOLD)))
  {
    //Detection
    if (RobotCfg.Function.Detection == FUNC_ON)
    {
      if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
      {
        if ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_INMOLD)
                                                       || ((RobotCfg.Setting.ItlReject == USE)
                                                           && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_CHUCK_OK_X16_ON; 
        if ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_INMOLD)
                                                        || ((RobotCfg.Setting.ItlReject == USE)
                                                            && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_VACCUM_CHECK_X17_ON;
      }
      if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_INMOLD))
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_CHUCK_OK_X1F_ON;
    }
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_RELEASE_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
  }
  if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
      && ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_INMOLD)
                                                     || ((RobotCfg.Setting.ItlReject == USE)
                                                         && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON)))))
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
  if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
      && ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_INMOLD)
                                                      || ((RobotCfg.Setting.ItlReject == USE)
                                                          && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON)))))
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
  if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_INMOLD))
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_OFF;
  //UP
  SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
  ControlMsg.StepDispTemp[AutoSetpSEQPos++] = UP_STEP;
  SeqMsg.SeqArr[MakeSeqPos++] = DELAY_UP;
  if (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_ON;
  }
  else if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_ON;
  }
  else if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_ON;
  }
  //OutsideWait NO Use
  if (RobotCfg.MotionMode.OutsideWait == NO_USE)
  {
    //Detection
    if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (((RobotCfg.MotionMode.ChuckOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.ChuckOff != RELEASE_POS_NO_USE)) || ((RobotCfg.MotionMode.VaccumOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.VaccumOff != RELEASE_POS_NO_USE))
                                                              || ((RobotCfg.MotionMode.ChuckRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.ChuckRejectPos != RELEASE_POS_NO_USE))
                                                                || ((RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_NO_USE)))
        || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && ((RobotCfg.MotionMode.SubArmOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.SubArmOff != RELEASE_POS_NO_USE))))
    {
      if (RobotCfg.Function.Detection == FUNC_ON)
      {
        if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
        {
          if ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.ChuckOff != RELEASE_POS_NO_USE)))
            SeqMsg.SeqArr[MakeSeqPos++] = INPUT_CHUCK_OK_X16_ON; 
          if ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.VaccumOff != RELEASE_POS_NO_USE)))
            SeqMsg.SeqArr[MakeSeqPos++] = INPUT_VACCUM_CHECK_X17_ON;
        }
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && ((RobotCfg.MotionMode.SubArmOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.SubArmOff != RELEASE_POS_NO_USE)))
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_CHUCK_OK_X1F_ON;
      }
    }
    //Interlock ON
    if (RobotCfg.Function.Ejector == FUNC_ON)
    {
      SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_OFF;
      SeqMsg.SeqArr[MakeSeqPos++] = ITLO_EJECTOR_Y2B_OFF;
    }
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_CYCLE_START_Y29_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_ON;
  }
  else if (RobotCfg.MotionMode.OutsideWait == USE)
  {
    //Interlock ON
    if (RobotCfg.Function.Ejector == FUNC_ON)
    {
      SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_OFF;
      SeqMsg.SeqArr[MakeSeqPos++] = ITLO_EJECTOR_Y2B_OFF;
    }
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_CYCLE_START_Y29_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_ON;
  }
  //IMMType
  if ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.MainRotateChuck == USE) && (RobotCfg.Setting.IMMType == VERTI) && (RobotCfg.Setting.RotationState == NO_ROTATION)
      && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_ChuRo)
                                                      || ((RobotCfg.Setting.ItlReject == USE)
                                                          && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_ChuRo) && (RobotCfg.Function.Reject == SIGNAL_ON) && (IOMsg.X1B_Reject == SIGNAL_ON))))
          || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_ChuRo)
                                                          || ((RobotCfg.Setting.ItlReject == USE)
                                                              && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_ChuRo) && (RobotCfg.Function.Reject == SIGNAL_ON) && (IOMsg.X1B_Reject == SIGNAL_ON))))))
  {
    //Chuck Rotation
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos-1;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_ROTATION_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_ROTATE_CHUCK_Y24_ON;
    
    //IMMType Verti type Chuck Rotation OFF
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_RELEASE_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
    
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
    
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_ROTATION_RETURN_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_CHUCK_ROTATE_RETURN;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_ROTATE_CHUCK_Y24_OFF;
  }
  else
  {
    //Not In Mold Release
    if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (((RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.ChuckOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.ChuckOff != RELEASE_POS_NO_USE)) 
                                                              || ((RobotCfg.MotionMode.MainVaccum == USE) && (RobotCfg.MotionMode.VaccumOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.VaccumOff != RELEASE_POS_NO_USE))
                                                                || ((RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.ChuckRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.ChuckRejectPos != RELEASE_POS_NO_USE))
                                                                  || ((RobotCfg.MotionMode.MainVaccum == USE) && (RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_NO_USE)))
        || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && ((RobotCfg.MotionMode.SubArmOff != RELEASE_POS_INMOLD)) && (RobotCfg.MotionMode.SubArmOff != RELEASE_POS_NO_USE)))
    {
      //Kick MS L type
      if((RobotCfg.RobotType == ROBOT_TYPE_TWIN) && (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS))
      {
        if ((RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE))
        {
          SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
          ControlMsg.StepDispTemp[AutoSetpSEQPos++] = KICK_STEP;
          SeqMsg.SeqArr[MakeSeqPos++] = DELAY_KICK;
          
          if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)
          {
            if(RobotCfg.MotionMode.MainArmType == ARM_L_TYPE)
            {
              if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
                SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
              else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
                SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
            }
            if (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE)
            {
              if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
                SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
              else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
                SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
            }
          }
        }
      }
      //Rotation
      SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
      ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SWING_STEP;
      SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING;
      SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_OFF;
      SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_ON;
      SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_DONE_X14_ON;
      SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_RETURN_DONE_X15_OFF;
#ifdef SWING_IO_OFF
      SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_OFF;
#endif
      //OutsideWait USE
      if (RobotCfg.MotionMode.OutsideWait == USE)
      {
        //Detection
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && ((RobotCfg.MotionMode.ChuckOff != RELEASE_POS_INMOLD) || (RobotCfg.MotionMode.VaccumOff != RELEASE_POS_INMOLD)
                                                                  || (RobotCfg.MotionMode.ChuckRejectPos != RELEASE_POS_INMOLD) 
                                                                    || (RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_INMOLD))
            || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff != RELEASE_POS_INMOLD)))
        {
          if (RobotCfg.Function.Detection == FUNC_ON)
          {
            if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
            {
              if (RobotCfg.MotionMode.MainChuck == USE)
                SeqMsg.SeqArr[MakeSeqPos++] = INPUT_CHUCK_OK_X16_ON; 
              if (RobotCfg.MotionMode.MainVaccum == USE)
                SeqMsg.SeqArr[MakeSeqPos++] = INPUT_VACCUM_CHECK_X17_ON;
            }
            if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
              SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_CHUCK_OK_X1F_ON;
          }
        }
        //Interlock
        if (RobotCfg.Function.Ejector == FUNC_ON)
        {
          SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_OFF;
          SeqMsg.SeqArr[MakeSeqPos++] = ITLO_EJECTOR_Y2B_OFF;
        }
        SeqMsg.SeqArr[MakeSeqPos++] = ITLO_CYCLE_START_Y29_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_ON;
      }
      //Chuck Rotation
      if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainRotateChuck == USE)) 
          && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff != RELEASE_POS_INMOLD)
                                                          || ((RobotCfg.Setting.ItlReject == USE)
                                                              && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
              || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff != RELEASE_POS_INMOLD)
                                                              || ((RobotCfg.Setting.ItlReject == USE)
                                                                  && (RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))))
      {
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos-1;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_ROTATION_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_ROTATE_CHUCK_Y24_ON;
      }
      //IMMType Verti type Chuck Rotation OFF
      if (((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.MainRotateChuck == USE) && (RobotCfg.Setting.IMMType == VERTI) && (RobotCfg.Setting.RotationState == ROTATION))
          && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_ChuRo)
                                                          || ((RobotCfg.Setting.ItlReject == USE)
                                                              && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_ChuRo) && (RobotCfg.Function.Reject == FUNC_ON) && (IOMsg.X1B_Reject == SIGNAL_ON))))
              || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_ChuRo)
                                                              || ((RobotCfg.Setting.ItlReject == USE)
                                                                  && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_ChuRo) && (RobotCfg.Function.Reject == FUNC_ON) && (IOMsg.X1B_Reject == SIGNAL_ON))))))
      {
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_RELEASE_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        if ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_ChuRo)
                                                       || ((RobotCfg.Setting.ItlReject == USE)
                                                           && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_ChuRo) && (RobotCfg.Function.Reject == FUNC_ON))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
        if ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_ChuRo)
                                                        || ((RobotCfg.Setting.ItlReject == USE)
                                                            && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_ChuRo) && (RobotCfg.Function.Reject == FUNC_ON))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
      }
      //Special case
      if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && 
         ((RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_OUTSIDE)) 
           && ((RobotCfg.MotionMode.MainVaccum == USE) && (RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN))
             && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN))
      {
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_DOWN_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_DOWN2ND;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_RELEASE_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_DOWN_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_DOWN2ND;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_RELEASE_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_UP_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_UP2ND;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_ON;
      }
      else if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && 
              ((RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP))
                && ((RobotCfg.MotionMode.MainVaccum == USE) && (RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN))
                  && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN))
      {
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_DOWN_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_DOWN2ND;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_RELEASE_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_UP_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_UP2ND;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_ON;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_RELEASE_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_UP_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_UP2ND;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_ON;
      }
      else
      {
        //Rotation Release
        if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
             && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_OUTSIDE)
                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                 && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_OUTSIDE) && (RobotCfg.Function.Reject == FUNC_ON))))
                 || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_OUTSIDE)
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_OUTSIDE) && (RobotCfg.Function.Reject == FUNC_ON))))))
            || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_OUTSIDE)))
        {
          SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
          ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_RELEASE_STEP;
          SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        }
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
            && ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_OUTSIDE)
                                                           || ((RobotCfg.Setting.ItlReject == USE)
                                                               && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_OUTSIDE) && (RobotCfg.Function.Reject == FUNC_ON)))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
            && ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_OUTSIDE)
                                                            || ((RobotCfg.Setting.ItlReject == USE)
                                                                && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_OUTSIDE) && (RobotCfg.Function.Reject == FUNC_ON)))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_OUTSIDE))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_OFF;
        //2nd Down
        if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
             && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN)
                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                 && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN)
                                                                   && (RobotCfg.Function.Reject == FUNC_ON))))
                 || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN)
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN)
                                                                       && (RobotCfg.Function.Reject == FUNC_ON))))))
            || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN)))
        {
          SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
          ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_DOWN_STEP;
          SeqMsg.SeqArr[MakeSeqPos++] = DELAY_DOWN2ND;
        }
        if (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) 
        {
          if (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN)
                                                          || ((RobotCfg.Setting.ItlReject == USE) 
                                                              && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN) 
                                                                && (RobotCfg.Function.Reject == FUNC_ON))))
              || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN)
                                                              || ((RobotCfg.Setting.ItlReject == USE)
                                                                  && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN)
                                                                    && (RobotCfg.Function.Reject == FUNC_ON)))))
          {
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_ON;
            SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_OFF;
          }
          if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN))
          {
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_ON;
            SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_OFF;
          }
        }
        else if ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) 
                 && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN)
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN)
                                                                       && (RobotCfg.Function.Reject == FUNC_ON))))
                     || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN)
                                                                     || ((RobotCfg.Setting.ItlReject == USE)
                                                                         && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN)
                                                                           && (RobotCfg.Function.Reject == FUNC_ON))))))
        {
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_ON;
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_OFF;
        }
        else if ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN))
        {
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_ON;
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_OFF;
        }
        //Nipper ON
        //  if ((RobotCfg.MotionMode.MainNipper == USE) && (RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.MainVaccum == NO_USE))
        //  {
        //	 SeqMsg.SeqArr[MakeSeqPos++] = DELAY_NIPPER;
        //	 SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_NIPPER_Y26_ON;
        //  }
        //2nd Down Release
        if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
             && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN)
                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                 && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN) && (RobotCfg.Function.Reject == FUNC_ON))))
                 || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN)
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN) && (RobotCfg.Function.Reject == FUNC_ON))))))
            || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN)))
        {
          SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
          ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_RELEASE_STEP;
          SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        }
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
            && ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN)
                                                           || ((RobotCfg.Setting.ItlReject == USE)
                                                               && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN) && (RobotCfg.Function.Reject == FUNC_ON)))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
            && ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN)
                                                            || ((RobotCfg.Setting.ItlReject == USE)
                                                                && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN) && (RobotCfg.Function.Reject == FUNC_ON)))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_OFF;
        //2nd UP
        if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
             && (((RobotCfg.MotionMode.MainChuck == USE) && (((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP)) 
                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                 && ((RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndUP)) 
                                                                   && (RobotCfg.Function.Reject == FUNC_ON))))
                 || ((RobotCfg.MotionMode.MainVaccum == USE) && (((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN) ||(RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndUP))
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && ((RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndUP))
                                                                       && (RobotCfg.Function.Reject == FUNC_ON))))))
            || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && ((RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndUP))))
        {
          SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
          ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_UP_STEP;
          SeqMsg.SeqArr[MakeSeqPos++] = DELAY_UP2ND;
        }
        if (((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) 
             && (((RobotCfg.MotionMode.MainChuck == USE) && (((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP)) 
                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                 && ((RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndUP)) 
                                                                   && (RobotCfg.Function.Reject == FUNC_ON))))
                 || ((RobotCfg.MotionMode.MainVaccum == USE) && (((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN) ||(RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndUP))
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && ((RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndUP))
                                                                       && (RobotCfg.Function.Reject == FUNC_ON))))))
            || ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && ((RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndUP))))
        {
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_OFF;
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_OFF;
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_ON;
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_ON;
        }
        else if ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) 
                 && (((RobotCfg.MotionMode.MainChuck == USE) && (((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP)) 
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && ((RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndUP)) 
                                                                       && (RobotCfg.Function.Reject == FUNC_ON))))
                     || ((RobotCfg.MotionMode.MainVaccum == USE) && (((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN) ||(RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndUP))
                                                                     || ((RobotCfg.Setting.ItlReject == USE)
                                                                         && ((RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndUP))
                                                                           && (RobotCfg.Function.Reject == FUNC_ON))))))
        {
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_OFF;
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_ON;
        }
        else if ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) && ((RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndUP)))
        {
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_OFF;
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_ON;
        }
        //Nipper OFF
        //  if ((RobotCfg.MotionMode.MainNipper == USE) && (RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.MainVaccum == NO_USE))
        //	 SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_NIPPER_Y26_OFF;
        
        //2nd UP Release
        if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP)
                                                                                                               || ((RobotCfg.Setting.ItlReject == USE) 
                                                                                                                   && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndUP) && (RobotCfg.Function.Reject == FUNC_ON))))
                                                                   || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndUP)
                                                                                                                   || ((RobotCfg.Setting.ItlReject == USE)
                                                                                                                       && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndUP) && (RobotCfg.Function.Reject == FUNC_ON))))))
            || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndUP)))
        {
          SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
          ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_RELEASE_STEP;
          SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        }
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP)
                                                                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                                                                 && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndUP) && (RobotCfg.Function.Reject == FUNC_ON)))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndUP)
                                                                                                              || ((RobotCfg.Setting.ItlReject == USE)
                                                                                                                  && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndUP) && (RobotCfg.Function.Reject == FUNC_ON)))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndUP))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_OFF;
      }
      
      //Converyor
      SeqMsg.SeqArr[MakeSeqPos++] = ITLO_CONVEYOR_Y2C_ON;
      
      //Chuck Rotation Return
      if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainRotateChuck == USE)) 
          && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff != RELEASE_POS_INMOLD)
                                                          || ((RobotCfg.Setting.ItlReject == USE)
                                                              && (RobotCfg.MotionMode.ChuckRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
              || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff != RELEASE_POS_INMOLD)
                                                              || ((RobotCfg.Setting.ItlReject == USE)
                                                                  && (RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))))
      {
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_ROTATION_RETURN_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_CHUCK_ROTATE_RETURN;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_ROTATE_CHUCK_Y24_OFF;
      }
      //SwingReturn
      if (RobotCfg.MotionMode.OutsideWait == NO_USE)
      {
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SWING_RETURN_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING_RETURN;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_RETURN_DONE_X15_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_DONE_X14_OFF;
#ifdef SWING_IO_OFF
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_OFF;
#endif
      }
    }
  }
  //Twin L type OR U type Kick Return
  if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)
  {
    if ((RobotCfg.MotionMode.MainArmType == ARM_U_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE) ||
        (RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE))
    {
      SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
      ControlMsg.StepDispTemp[AutoSetpSEQPos++] = KICK_RETURN_STEP;
      SeqMsg.SeqArr[MakeSeqPos++] = DELAY_KICK_RETURN;
      if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
      {
        if (RobotCfg.MotionMode.MainArmType == ARM_U_TYPE)
        {
          if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
          else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
        }
        else if (RobotCfg.MotionMode.MainArmType == ARM_L_TYPE)
        {
          if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
          else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
        }
      }
      if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
      {
        if (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE)
        {
          if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
          else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
        }
        else if (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE)
        {
          if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
          else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
        }
      }
    }
  }
  else 
  {
    //U type Kick Return
    if (((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.MainArmType == ARM_U_TYPE)) || 
        ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE)))
    {
      SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
      ControlMsg.StepDispTemp[AutoSetpSEQPos++] = KICK_RETURN_STEP;
      SeqMsg.SeqArr[MakeSeqPos++] = DELAY_KICK_RETURN;
      
      if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
      {
        if(RobotCfg.MotionMode.MainArmType == ARM_U_TYPE)
        {
          if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
          else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
        }
      }
      if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
      {
        if (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE)
        {
          if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
          else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
        }
      }
    }
  }
  //Outside Wait Swing
  if ((((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) 
        && (((RobotCfg.MotionMode.MainChuck == USE) && (((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_INMOLD) || (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_NO_USE))
                                                        || ((RobotCfg.Setting.ItlReject == USE)
                                                            && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
            && ((RobotCfg.MotionMode.MainVaccum == USE) && (((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_INMOLD) || (RobotCfg.MotionMode.VaccumOff == RELEASE_POS_NO_USE))
                                                            || ((RobotCfg.Setting.ItlReject == USE)
                                                                && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))))
       && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_INMOLD))
      && (RobotCfg.MotionMode.OutsideWait == USE))
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SWING_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_DONE_X14_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_RETURN_DONE_X15_OFF;
#ifdef SWING_IO_OFF
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_OFF;
#endif
  }
  else if ((((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) 
             && (((RobotCfg.MotionMode.MainChuck == USE) && (((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_INMOLD) || (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_NO_USE))
                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                 && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
                 || ((RobotCfg.MotionMode.MainVaccum == USE) && (((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_INMOLD) || (RobotCfg.MotionMode.VaccumOff == RELEASE_POS_NO_USE))
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON)))))))
           && (RobotCfg.MotionMode.OutsideWait == USE))
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SWING_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_DONE_X14_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_RETURN_DONE_X15_OFF;
#ifdef SWING_IO_OFF
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_OFF;
#endif
  }
  else if (((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_INMOLD))
           && (RobotCfg.MotionMode.OutsideWait == USE))
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SWING_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_DONE_X14_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_RETURN_DONE_X15_OFF;
#ifdef SWING_IO_OFF
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_OFF;
#endif
  }
  
  SeqMsg.SeqDispArr[StepSEQPos++] = SEQ_END;
  SeqMsg.SeqArr[MakeSeqPos] = SEQ_END;
  //Total Step Count
  ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX] = AutoSetpSEQPos;          //�� 19? TOTAL_COUNT_INDEX�� ����
}
