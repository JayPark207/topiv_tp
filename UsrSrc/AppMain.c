#include <stdio.h>// 12_03
#include <string.h>
#include <stdlib.h>
#include "AppMain.h"
#include "SDCard.h"
#include "AppKey.h"

ERROR_MSG ErrMsg;
ERROR_MSG ErrMsgTemp[100];

CONTROL_MSG ControlMsg;
CONTROL_MSG *ControlMsgTEST;          //안쓰임

ROBOT_CONFIG RobotCfg;   //
IO_MSG IOMsg;          

MOTION_DELAY MotionDelayTemp;
MOTION_DELAY CycleDelay;															//For Timer in Cycle page
MOTION_MODE MotionModeTemp;

MANUFACTURER_SETTING SettingTemp;

SEQ_MSG SeqMsg;       //
MOLD_MSG MoldMsg;      //
COUNT_MSG CountMsg;     //

//extern FATFS SDFatFs;						
//FIL TestFile;		//안쓰임			
//DIR TestDir;			//안쓰임			
//SDRES SdRes;        //ActiveRun.c, 전역 안해도됨
//SDRES SdResTemp;   //ActiveRun.c, 전역 안해도됨

uint8_t StepCursor = 0;																//For Display arrow StepPage
char NewMoldName[MAX_MOLDNAME_LENGTH];											//For New Mold Name
char NewMoldNameTemp[MAX_MOLDNAME_LENGTH];									//For New Mold Name Temp. change one character
uint8_t MoldNameCharPos = 5;														//For Char_Input_Data[]  'F'
uint8_t MoldNamePos = 0;															//For NewMoldName[]
uint16_t MoldNoTemp = 0;															//For New Mold No
uint8_t UnderbarPos = 0;  
//uint8_t BuzzerState = 0;

uint8_t SCStepPos=0; //printf for문 용 (안쓰임)

uint8_t ClickCount = 0;   //(안쓰임)

uint8_t AutoStepDelay[20];
uint8_t AutoStep[20];
uint8_t AutoStepPos = 0;
uint64_t AutoTempTick = 0;

uint64_t SDTempTick = 0;
uint8_t fPreviousPage = 0;
uint8_t AutoInjectionTemp = 0;
//uint32_t byteswritten, bytesread;
//FIL* fp;
//	FILINFO fno;
//	FRESULT fr;
//	FRESULT res;

//extern void ActiveRun(TP_MODE* TpMode);   //ActiveRun.c functions -> AppMain.h
//extern void ChangeSCMode(void);
//extern uint8_t KeyNum(uint32_t* key);

//
//uint8_t ReadType(void)   //ActiveRun.c
//{
//  return (uint8_t)(((GPIOB->IDR >> 14) & 0x03) ^ 0x03);
//}
//uint8_t KeyIsNum(uint32_t* key)    //ActiveRun.c
//{
//  uint8_t ret;
//  switch(*key)
//  {
//  case KEY_NO_0:
//  case KEY_NO_1:
//  case KEY_NO_2:
//  case KEY_NO_3:
//  case KEY_NO_4:
//  case KEY_NO_5:
//  case KEY_NO_6:
//  case KEY_NO_7:
//  case KEY_NO_8:
//  case KEY_NO_9:
//    ret = KEY_IS_NUM;
//    break;
//  default:
//    ret = KEY_IS_NOT_NUM;
//    break;
//  }
//  return ret;
//}
//uint8_t KeyNum(uint32_t* key)    //ActiveRun.c로 이동
//{
//  uint8_t ret;
//  switch(*key)
//  {
//  case KEY_NO_0:
//    ret = 0;
//    break;
//  case KEY_NO_1:
//    ret = 1;
//    break;
//  case KEY_NO_2:
//    ret = 2;
//    break;
//  case KEY_NO_3:
//    ret = 3;
//    break;
//  case KEY_NO_4:
//    ret = 4;
//    break;
//  case KEY_NO_5:
//    ret = 5;
//    break;
//  case KEY_NO_6:
//    ret = 6;
//    break;
//  case KEY_NO_7:
//    ret = 7;
//    break;
//  case KEY_NO_8:
//    ret = 8;
//    break;
//  case KEY_NO_9:
//    ret = 9;
//    break;
//  default:
//    ret = 0xFF;
//    break;
//  }
//  return ret;
//}

//void ChangeSCMode(void)      //ActiveRun.c로 이동
//{
//  ControlMsg.CommStateForDL = 0;
//  ControlMsg.CommMode = MODE_INIT;
//}

void ClearPage(void)          //ActiveRun.c AppError.c 사용중  
{
  ControlMsg.PageNo = 0;
  ControlMsg.LineNo = 0;
  ControlMsg.CursorPos = 0;
  ControlMsg.EnableEdit = DISABLE_EDIT;
  ControlMsg.KeyValue = 0;
}

void ClearAuto(void)          //ActiveRun.c AppError.c 사용중
{
  ClearPage();
  ControlMsg.fDoHoming = 0;
  AutoStepPos = 0;
  AutoTempTick = 0;
  ControlMsg.AutoPageNo = 0;
  ControlMsg.AutoLineNo = 0;
  memset(AutoStep,0,sizeof(AutoStep));
  
  //ChangeSCMode();  //210304 Draw 설계중
  
  //  ControlMsg.TpMode = MANUAL_OPERATING;
}

//void ChangeLanguage(void)   //ActiveRun.c로 이동
//{
//  if(RobotCfg.Language == LANGUAGE_ENG) RobotCfg.Language = LANGUAGE_KOR;
//  else if(RobotCfg.Language == LANGUAGE_KOR) RobotCfg.Language = LANGUAGE_ENG;
//  WriteRobotCfg(&RobotCfg);
//}

void TimerDispCheck(void)     //ActiveRun.c로 이동예정
{
  //According to ModeSetting TimerPageSetting
  uint8_t TimerCounter = 0;
  
  ControlMsg.TimerDisp[DOWN_TIME] = USE;  
  ControlMsg.TimerDisp[KICK_TIME] = USE;
  ControlMsg.TimerDisp[EJECTOR_TIME] = USE;
  ControlMsg.TimerDisp[CHUCK_TIME] = USE;
  ControlMsg.TimerDisp[KICK_RETURN_TIME] = USE;
  ControlMsg.TimerDisp[UP_TIME] = USE;
  ControlMsg.TimerDisp[SWING_TIME] = USE;
  ControlMsg.TimerDisp[SECOND_DOWN_TIME] = USE;
  ControlMsg.TimerDisp[RELEASE_TIME] = USE;
  ControlMsg.TimerDisp[SECOND_UP_TIME] = USE;
  
  if (RobotCfg.MotionMode.MainRotateChuck == USE)
    ControlMsg.TimerDisp[CHUCK_ROTATION_TIME] = USE;
  else
    ControlMsg.TimerDisp[CHUCK_ROTATION_TIME] = NO_USE;
  
  ControlMsg.TimerDisp[SWING_RETURN_TIME] = USE;
  ControlMsg.TimerDisp[NIPPER_TIME] = NO_USE;
  ControlMsg.TimerDisp[CONVEYOR_TIME] = USE;
  
  //Total Timer Menu Count
  for (uint8_t TimeDispSetting = 0 ; TimeDispSetting < MAX_INDEX ; TimeDispSetting++)
  {
    if (ControlMsg.TimerDisp[TimeDispSetting] == USE)
    {
      ControlMsg.TimerDispTemp[TimerCounter] = TimeDispSetting;
      TimerCounter = TimerCounter+1;
    }
  }
  ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX] = TimerCounter;
}

//IMMType
void IMMTypeINIT(uint16_t MoldNo)     //ActiveRun.c로 이동예정X
{
  if((RobotCfg.Setting.IMMType == VERTI) && (RobotCfg.Setting.RotationState == NO_ROTATION))
  {
    if((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC))
    {
      if((MoldNo == 23) || (MoldNo == 33) || (MoldNo == 37))
      {
        MotionModeTemp.ChuckOff = RELEASE_POS_ChuRo;
        MotionModeTemp.ChuckRejectPos = RELEASE_POS_ChuRo;
        memcpy(&(RobotCfg.MotionMode.ChuckOff), &MotionModeTemp.ChuckOff, sizeof(MotionModeTemp.ChuckOff));
        memcpy(&(RobotCfg.MotionMode.ChuckRejectPos), &MotionModeTemp.ChuckRejectPos, sizeof(MotionModeTemp.ChuckRejectPos));
        memcpy(&(MoldMsg.MotionMode.ChuckOff), &MotionModeTemp.ChuckOff, sizeof(MotionModeTemp.ChuckOff));
        memcpy(&(MoldMsg.MotionMode.ChuckRejectPos), &MotionModeTemp.ChuckRejectPos, sizeof(MotionModeTemp.ChuckRejectPos));
        if(MoldNo != NEW_MOLD)
        {
          WriteRobotCfg(&RobotCfg);
          WriteMold(&MoldMsg);
        }
      }
    }
    if(RobotCfg.RobotType == ROBOT_TYPE_XC)
    {
      if((MoldNo == 36) || (MoldNo == 37))
      {
        MotionModeTemp.VaccumOff = RELEASE_POS_ChuRo;
        MotionModeTemp.VaccumRejectPos = RELEASE_POS_ChuRo;
        memcpy(&(RobotCfg.MotionMode.VaccumOff), &MotionModeTemp.VaccumOff, sizeof(MotionModeTemp.VaccumOff));
        memcpy(&(RobotCfg.MotionMode.VaccumRejectPos), &MotionModeTemp.VaccumRejectPos, sizeof(MotionModeTemp.VaccumRejectPos));
        memcpy(&(MoldMsg.MotionMode.VaccumOff), &MotionModeTemp.VaccumOff, sizeof(MotionModeTemp.VaccumOff));
        memcpy(&(MoldMsg.MotionMode.VaccumRejectPos), &MotionModeTemp.VaccumRejectPos, sizeof(MotionModeTemp.VaccumRejectPos));
        if(MoldNo != NEW_MOLD)
        {
          WriteRobotCfg(&RobotCfg);
          WriteMold(&MoldMsg);
        }
      }
    }
  }
  else if((RobotCfg.Setting.IMMType == STANDARD) || (RobotCfg.Setting.RotationState == ROTATION))
  {
    if((RobotCfg.RobotType == ROBOT_TYPE_XC) || (RobotCfg.RobotType == ROBOT_TYPE_X))
    {
      if((MoldNo == 23) || (MoldNo == 33) || (MoldNo == 37))
      {
        MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
        MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
        memcpy(&(RobotCfg.MotionMode.ChuckOff), &MotionModeTemp.ChuckOff, sizeof(MotionModeTemp.ChuckOff));
        memcpy(&(RobotCfg.MotionMode.ChuckRejectPos), &MotionModeTemp.ChuckRejectPos, sizeof(MotionModeTemp.ChuckRejectPos));
        memcpy(&(RobotCfg.MotionMode.ChuckRejectPos), &MotionModeTemp.ChuckRejectPos, sizeof(MotionModeTemp.ChuckRejectPos));
        memcpy(&(MoldMsg.MotionMode.ChuckRejectPos), &MotionModeTemp.ChuckRejectPos, sizeof(MotionModeTemp.ChuckRejectPos));
        if(MoldNo != NEW_MOLD)
        {
          WriteRobotCfg(&RobotCfg);
          WriteMold(&MoldMsg);
        }
      }
    }
    if(RobotCfg.RobotType == ROBOT_TYPE_XC)
    {
      if((MoldNo == 36) || (MoldNo == 37))
      {
        MotionModeTemp.VaccumOff = RELEASE_POS_2ndDOWN;
        MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
        memcpy(&(RobotCfg.MotionMode.VaccumOff), &MotionModeTemp.VaccumOff, sizeof(MotionModeTemp.VaccumOff));
        memcpy(&(RobotCfg.MotionMode.VaccumRejectPos), &MotionModeTemp.VaccumRejectPos, sizeof(MotionModeTemp.VaccumRejectPos));
        memcpy(&(RobotCfg.MotionMode.VaccumRejectPos), &MotionModeTemp.VaccumRejectPos, sizeof(MotionModeTemp.VaccumRejectPos));
        memcpy(&(MoldMsg.MotionMode.VaccumRejectPos), &MotionModeTemp.VaccumRejectPos, sizeof(MotionModeTemp.VaccumRejectPos));
        if(MoldNo != NEW_MOLD)
        {
          WriteRobotCfg(&RobotCfg);
          WriteMold(&MoldMsg);
        }
      }
    }
  }
}

void ModeDispCheck(uint16_t MoldNoMode)   //ActiveRun.c로 이동예정
{
  //According to MoldNo ModeSettingValue
  uint8_t ModeCounter = 0;
  
  if(MoldNoMode == 22)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = USE;
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE;
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;
  }
  else if(MoldNoMode == 32)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 23)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   	
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 33)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   					
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }                                                          
  else if(MoldNoMode == 36)
  {                                                          
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   					
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = USE;       	
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 37)
  {                                                          
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   					
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = USE;       	
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 40)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 41)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 43)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 45)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   					
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        			
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;       
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 82)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   					
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = NO_USE;   			
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        			
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;       
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 84)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = NO_USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  } 
  else
  {
    //According to RobotType ModeSettingValue
    if (RobotCfg.RobotType == ROBOT_TYPE_TWIN)   
    {
      ControlMsg.ModeDisp[ARMSET_MODE] = USE;        			
      ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = USE;        	
      ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        		
      ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;    
    }                                                       
    else                                                    
    {
      ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;            
      ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;       
      ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;          
      ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE; 
	 }
    
    ControlMsg.ModeDisp[CHUCK_MODE] = USE;
    
    if ((RobotCfg.RobotType == ROBOT_TYPE_XC) || (RobotCfg.RobotType == ROBOT_TYPE_TWIN))  
    {
      ControlMsg.ModeDisp[VACUUM_MODE] = USE;         		
      ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = USE;       
      ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = USE;    
    }                                                       
    else                                                    
    {                                                       
      ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;            
      ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;    
      ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE; 
    }
    
    if (RobotCfg.RobotType != ROBOT_TYPE_A)      
      ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = USE;       
    else                                                    
      ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;    
    
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   					
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = USE;   			
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = USE;  	
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = USE;
  }
  
  //IMMType
  IMMTypeINIT(MoldNoMode);
  
  //Total Mode Menu Count
  for (uint8_t ModeDispSetting = 0 ; ModeDispSetting < MAX_INDEX ; ModeDispSetting++)          // 0~19 MAX_INDEX 20 
  {
    if (ControlMsg.ModeDisp[ModeDispSetting] == USE)
    {
      ControlMsg.ModeDispTemp[ModeCounter] = ModeDispSetting;
      ModeCounter = ModeCounter+1;
    }
  }
  ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX] = ModeCounter;             //19 Last index에 ModeCounter 값 넣음
}

void MoldInit(void)                     //ActiveRun.c로 이동예정X, mold.c 생성예정
{
  //According to MoldNo Default Mode Setting Value
  if ((MoldMsg.MoldNo == 22) || (MoldMsg.MoldNo == 32))
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;
    MotionModeTemp.MainChuck = USE;
    MotionModeTemp.MainVaccum = NO_USE;
    MotionModeTemp.MainRotateChuck = NO_USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    if (MoldMsg.MoldNo == 22)
      MotionModeTemp.MainArmType = ARM_U_TYPE;
    else if (MoldMsg.MoldNo == 32)
      MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_NO_USE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NO_USE;
    MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
    MotionModeTemp.SubArmOff = RELEASE_POS_NO_USE;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
  }
  else if ((MoldMsg.MoldNo == 23) || (MoldMsg.MoldNo == 33))
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;
    MotionModeTemp.MainChuck = USE;
    MotionModeTemp.MainVaccum = NO_USE;
    MotionModeTemp.MainRotateChuck = USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    if (MoldMsg.MoldNo == 23) 
      MotionModeTemp.MainArmType = ARM_U_TYPE;
    else if (MoldMsg.MoldNo == 33)
      MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_NO_USE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NO_USE;
    MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
    MotionModeTemp.SubArmOff = RELEASE_POS_NO_USE;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
  }
  else if ((MoldMsg.MoldNo == 36) || (MoldMsg.MoldNo == 37))
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;
    if (MoldMsg.MoldNo == 36)
      MotionModeTemp.MainChuck = NO_USE;
    else if (MoldMsg.MoldNo == 37)
      MotionModeTemp.MainChuck = USE;
    MotionModeTemp.MainVaccum = USE;
    MotionModeTemp.MainRotateChuck = USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_NO_USE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NO_USE;
    if (MoldMsg.MoldNo == 36)
      MotionModeTemp.ChuckOff = RELEASE_POS_NO_USE;
    else if (MoldMsg.MoldNo == 37)
      MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.SubArmOff = RELEASE_POS_NO_USE;
    if (MoldMsg.MoldNo == 36)
      MotionModeTemp.ChuckRejectPos = RELEASE_POS_NO_USE;
    else if (MoldMsg.MoldNo == 37)
      MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
  }
  else if (MoldMsg.MoldNo == 40)
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MS;
    MotionModeTemp.MainChuck = USE;
    MotionModeTemp.MainVaccum = NO_USE;
    MotionModeTemp.MainRotateChuck = NO_USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_L_TYPE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
    MotionModeTemp.SubArmOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
  }
  else if (MoldMsg.MoldNo == 41)
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MS;
    MotionModeTemp.MainChuck = USE;
    MotionModeTemp.MainVaccum = NO_USE;
    MotionModeTemp.MainRotateChuck = USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_L_TYPE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.SubArmOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
  }
  else if (MoldMsg.MoldNo == 43)
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MS;
    MotionModeTemp.MainChuck = NO_USE;
    MotionModeTemp.MainVaccum = USE;
    MotionModeTemp.MainRotateChuck = USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_L_TYPE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
    MotionModeTemp.SubArmOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
  }
  else if (MoldMsg.MoldNo == 45)
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MS;
    MotionModeTemp.MainChuck = USE;
    MotionModeTemp.MainVaccum = USE;
    MotionModeTemp.MainRotateChuck = USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_L_TYPE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.SubArmOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
  }
  else if ((MoldMsg.MoldNo == 82) || (MoldMsg.MoldNo == 84))
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_SUB;
    MotionModeTemp.MainChuck = NO_USE;
    MotionModeTemp.MainVaccum = NO_USE;
    MotionModeTemp.MainRotateChuck = NO_USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    MotionModeTemp.MainArmType = ARM_NO_USE;
    if (MoldMsg.MoldNo == 82)
      MotionModeTemp.SubArmType = ARM_U_TYPE;
    else if (MoldMsg.MoldNo == 84)
      MotionModeTemp.SubArmType = ARM_L_TYPE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_NO_USE;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.ChuckOff = RELEASE_POS_NO_USE;
    MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
    MotionModeTemp.SubArmOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_NO_USE;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
  }
  else  //MoldMsg.MoldNo == 0인 경우
  {
    if(RobotCfg.RobotType == ROBOT_TYPE_TWIN)
    {
      MotionModeTemp.MotionArm = ROBOT_MAIN_MS;
      MotionModeTemp.MainChuck = USE;
      MotionModeTemp.MainVaccum = USE;
      MotionModeTemp.MainRotateChuck = USE;
      MotionModeTemp.MainNipper = NO_USE;
      MotionModeTemp.OutsideWait = NO_USE;
      MotionModeTemp.MainArmType = ARM_L_TYPE;
      MotionModeTemp.SubArmType = ARM_L_TYPE;
      MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
      MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
      MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.VaccumOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.SubArmOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
      MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
    }
    else if(RobotCfg.RobotType == ROBOT_TYPE_A)
    {
      MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;
      MotionModeTemp.MainChuck = USE;
      MotionModeTemp.MainVaccum = 0xFF;
      MotionModeTemp.MainRotateChuck = 0xFF;
      MotionModeTemp.MainNipper = 0xFF;
      MotionModeTemp.OutsideWait = NO_USE;
      MotionModeTemp.MainArmType = ARM_L_TYPE;
      MotionModeTemp.SubArmType = ARM_NO_USE;
      MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
      MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NO_USE;
      MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
      MotionModeTemp.SubArmOff = RELEASE_POS_NO_USE;
      MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
      MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
    }
    else if(RobotCfg.RobotType == ROBOT_TYPE_X)
    {
      MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;
      MotionModeTemp.MainChuck = USE;
      MotionModeTemp.MainVaccum = 0xFF;
      MotionModeTemp.MainRotateChuck = USE;
      MotionModeTemp.MainNipper = 0xFF;
      MotionModeTemp.OutsideWait = NO_USE;
      MotionModeTemp.MainArmType = ARM_L_TYPE;
      MotionModeTemp.SubArmType = ARM_NO_USE;
      MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
      MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NO_USE;
      MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
      MotionModeTemp.SubArmOff = RELEASE_POS_NO_USE;
      MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
      MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
    }
    else if(RobotCfg.RobotType == ROBOT_TYPE_XC)
    {
      MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;
      MotionModeTemp.MainChuck = USE;
      MotionModeTemp.MainVaccum = USE;
      MotionModeTemp.MainRotateChuck = USE;
      MotionModeTemp.MainNipper = 0xFF;
      MotionModeTemp.OutsideWait = NO_USE;
      MotionModeTemp.MainArmType = ARM_L_TYPE;
      MotionModeTemp.SubArmType = ARM_NO_USE;
      MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
      MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NO_USE;
      MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.VaccumOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.SubArmOff = RELEASE_POS_NO_USE;
      MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
      MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
    }
  }
  //Dealy Default 0.5s
  for(uint8_t DelayPos = 0 ; DelayPos < sizeof(MoldMsg.MotionDelay) ; DelayPos++)
  {
    (*(&(MoldMsg.MotionDelay.DownDelay) + DelayPos)) = 5;
  }
  MoldMsg.Function.Buzzer = USE;
  MoldMsg.Function.Detection = USE;
  MoldMsg.Function.Ejector = NO_USE;
  MoldMsg.Function.Reject = NO_USE;
}

void NewMoldInit(uint16_t NewMoldNo)	//ActiveRun.c로 이동예정	 
{
  //NEW Mold No & Name Init
  //Default Mold Name
  NewMoldNameTemp[0] = 5;						//'F'
  NewMoldNameTemp[1] = 8;						//'I'
  NewMoldNameTemp[2] = 11;						//'L'
  NewMoldNameTemp[3] = 4;						//'E'
  
  //Default Mold No
  if(NewMoldNo == 0)
  {
    NewMoldNameTemp[4] = 27;					//'1'
    NewMoldNameTemp[5] = 26;					//'0'
    NewMoldNameTemp[6] = 26;					//'0'
  }
  else
  {
    if(NewMoldNo/100 == 1)
      NewMoldNameTemp[4] = 27;				//'1'
    else if(NewMoldNo/100 == 2)
      NewMoldNameTemp[4] = 28;				//'2'
    else if(NewMoldNo/100 == 3)
      NewMoldNameTemp[4] = 29;				//'3'
    else if(NewMoldNo/100 == 4)
      NewMoldNameTemp[4] = 30;				//'4'
    else if(NewMoldNo/100 == 5)
      NewMoldNameTemp[4] = 31;				//'5'
    else if(NewMoldNo/100 == 6)
      NewMoldNameTemp[4] = 32;				//'6'
    else if(NewMoldNo/100 == 7)
      NewMoldNameTemp[4] = 33;				//'7'
    else if(NewMoldNo/100 == 8)
      NewMoldNameTemp[4] = 34;				//'8'
    else if(NewMoldNo/100 == 9)
      NewMoldNameTemp[4] = 35;				//'9'
    
    if(((NewMoldNo%100)/10) == 0)
      NewMoldNameTemp[5] = 26;				//'0'
    else if(((NewMoldNo%100)/10) == 1)
      NewMoldNameTemp[5] = 27;				//'1'
    else if(((NewMoldNo%100)/10) == 2)
      NewMoldNameTemp[5] = 28;				//'2'
    else if(((NewMoldNo%100)/10) == 3)
      NewMoldNameTemp[5] = 29;				//'3'
    else if(((NewMoldNo%100)/10) == 4)
      NewMoldNameTemp[5] = 30;				//'4'
    else if(((NewMoldNo%100)/10) == 5)
      NewMoldNameTemp[5] = 31;				//'5'
    else if(((NewMoldNo%100)/10) == 6)
      NewMoldNameTemp[5] = 32;				//'6'
    else if(((NewMoldNo%100)/10) == 7)
      NewMoldNameTemp[5] = 33;				//'7'
    else if(((NewMoldNo%100)/10) == 8)
      NewMoldNameTemp[5] = 34;				//'8'
    else if(((NewMoldNo%100)/10) == 9)
      NewMoldNameTemp[5] = 35;				//'9'
    
    if(((NewMoldNo%100)%10) == 0)
      NewMoldNameTemp[6] = 26;				//'0'
    else if(((NewMoldNo%100)%10) == 1)
      NewMoldNameTemp[6] = 27;				//'1'
    else if(((NewMoldNo%100)%10) == 2)
      NewMoldNameTemp[6] = 28;				//'2'
    else if(((NewMoldNo%100)%10) == 3)
      NewMoldNameTemp[6] = 29;				//'3'
    else if(((NewMoldNo%100)%10) == 4)
      NewMoldNameTemp[6] = 30;				//'4'
    else if(((NewMoldNo%100)%10) == 5)
      NewMoldNameTemp[6] = 31;				//'5'
    else if(((NewMoldNo%100)%10) == 6)
      NewMoldNameTemp[6] = 32;				//'6'
    else if(((NewMoldNo%100)%10) == 7)
      NewMoldNameTemp[6] = 33;				//'7'
    else if(((NewMoldNo%100)%10) == 8)
      NewMoldNameTemp[6] = 34;				//'8'
    else if(((NewMoldNo%100)%10) == 9)
      NewMoldNameTemp[6] = 35;				//'9'
  }
  NewMoldNameTemp[7] = 38;						//' '
  NewMoldNameTemp[8] = '\0';					//null
}

void InMenuUpButton(uint8_t Max_Line)    //ActiveRun.c로 이동예정
{
  if(ControlMsg.LineNo > 0) ControlMsg.LineNo--;
  else
  {
    if(ControlMsg.PageNo > 0)
    {
      ControlMsg.PageNo--;
      ControlMsg.LineNo = Max_Line;
    }
  }
}

void InMenuDownButton(uint8_t Max_Line , uint8_t Max_Page, uint8_t *DispTempArr)  //ActiveRun.c로 이동예정
{
  //Last line check
  if(ControlMsg.LineNo < Max_Line)	
  {
    //last page and last line check
    if ((ControlMsg.PageNo*Max_Page  + ControlMsg.LineNo) != DispTempArr[TOTAL_COUNT_INDEX] -1)
      ControlMsg.LineNo++;  //next line
  }
  else
  { 
    //last page check
    if (ControlMsg.PageNo != DispTempArr[TOTAL_COUNT_INDEX]/Max_Page)
    {
      //last page and last line check
      if ((ControlMsg.PageNo == DispTempArr[TOTAL_COUNT_INDEX]/Max_Page -1) &&
          (ControlMsg.LineNo == Max_Line) && (DispTempArr[TOTAL_COUNT_INDEX]%Max_Page == 0));
      else
      {
        //next page
        ControlMsg.PageNo++;
        ControlMsg.LineNo = 0;
      }
    }
  }
}
void ImportMode(void)                //ActiveRun.c로 이동예정
{
  if(ControlMsg.KeyValue == KEY_ARROW_LEFT)
  {
    if((ControlMsg.ModeDisp[ARMSET_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == ARMSET_MODE))
    {
      if((MotionModeTemp.MotionArm == ROBOT_MAIN_MAIN) && (ControlMsg.KeyValue == KEY_ARROW_LEFT))
        MotionModeTemp.MotionArm = ROBOT_MAIN_MS;
      else MotionModeTemp.MotionArm--;
    }
    else if((ControlMsg.ModeDisp[CHUCK_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == CHUCK_MODE))
    {
      MotionModeTemp.MainChuck ^= USE;
    }
    else if((ControlMsg.ModeDisp[VACUUM_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == VACUUM_MODE))
    {
      MotionModeTemp.MainVaccum ^= USE;
    }
    else if((ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == CHUCK_ROTATION_MODE))
    {
      MotionModeTemp.MainRotateChuck ^= USE;
    }
    else if((ControlMsg.ModeDisp[OUTSIDE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == OUTSIDE_MODE))
    {
      //IMMType
      if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.RotationState == NO_ROTATION))
        MotionModeTemp.OutsideWait = NO_USE;
      else
        MotionModeTemp.OutsideWait ^= USE;
    }
    else if((ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_TAKEOUT_MODE))
    {
      if((MotionModeTemp.MainArmType == ARM_L_TYPE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT))
        MotionModeTemp.MainArmType = ARM_I_TYPE;
      else MotionModeTemp.MainArmType--;
    }
    else if((ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == SUB_TAKEOUT_MODE))
    {
      if((MotionModeTemp.SubArmType == ARM_L_TYPE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT))
        MotionModeTemp.SubArmType = ARM_I_TYPE;
      else MotionModeTemp.SubArmType--;
    }
    else if((ControlMsg.ModeDisp[MAIN_DOWN_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_DOWN_MODE))
    {
      if((MotionModeTemp.MainArmDownPos == ARM_DOWNPOS_NOZZLE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
        MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
      else MotionModeTemp.MainArmDownPos--;
    }
    else if((ControlMsg.ModeDisp[SUB_DOWN_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == SUB_DOWN_MODE))
    {
      if((MotionModeTemp.SubArmDownPos == ARM_DOWNPOS_NOZZLE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
        MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_CLAMP;
      else MotionModeTemp.SubArmDownPos--;
    }
    else if((ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_CHUCK_RELEASE_MODE))
    {
      //IMMType
      if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.ChuckOff = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          if((MotionModeTemp.ChuckOff == RELEASE_POS_OUTSIDE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
            MotionModeTemp.ChuckOff = RELEASE_POS_ChuRo;
          else MotionModeTemp.ChuckOff--; 
        }
      }
      else
      {
        if((MotionModeTemp.ChuckOff == RELEASE_POS_INMOLD) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
          MotionModeTemp.ChuckOff = RELEASE_POS_2ndUP;
        else MotionModeTemp.ChuckOff--;
      }
    }
    else if((ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == VACUUM_RELEASE_MODE))
    {
      //IMMType
      if((RobotCfg.RobotType == ROBOT_TYPE_XC) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.VaccumOff = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          if((MotionModeTemp.VaccumOff == RELEASE_POS_OUTSIDE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
            MotionModeTemp.VaccumOff = RELEASE_POS_ChuRo;
          else MotionModeTemp.VaccumOff--;
        }
      }
      else
      {
        if((MotionModeTemp.VaccumOff == RELEASE_POS_INMOLD) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
          MotionModeTemp.VaccumOff = RELEASE_POS_2ndUP;
        else MotionModeTemp.VaccumOff--;
      }
    }
    else if((ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == SUB_CHUCK_RELEASE_MODE))
    {
      if((MotionModeTemp.SubArmOff == RELEASE_POS_INMOLD) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
        MotionModeTemp.SubArmOff = RELEASE_POS_2ndUP;
      else MotionModeTemp.SubArmOff--;
    }
    else if((ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_CHUCK_REJECT_MODE))
    {
      //IMMType
      if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.ChuckRejectPos = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          if((MotionModeTemp.ChuckRejectPos == RELEASE_POS_OUTSIDE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
            MotionModeTemp.ChuckRejectPos = RELEASE_POS_ChuRo;
          else MotionModeTemp.ChuckRejectPos--;
        }
      }
      else
      {
        if((MotionModeTemp.ChuckRejectPos == RELEASE_POS_INMOLD) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
          MotionModeTemp.ChuckRejectPos = RELEASE_POS_2ndUP;
        else MotionModeTemp.ChuckRejectPos--;
      }
    }
    else if((ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_VAUUM_REJECT_MODE))
    {
      //IMMType
      if((RobotCfg.RobotType == ROBOT_TYPE_XC) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.VaccumRejectPos = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          if((MotionModeTemp.VaccumRejectPos == RELEASE_POS_OUTSIDE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
            MotionModeTemp.VaccumRejectPos = RELEASE_POS_ChuRo;
          else MotionModeTemp.VaccumRejectPos--;
        }
      }
      else
      {
        if((MotionModeTemp.VaccumRejectPos == RELEASE_POS_INMOLD) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
          MotionModeTemp.VaccumRejectPos = RELEASE_POS_2ndUP;
        else MotionModeTemp.VaccumRejectPos--;
      }
    }
  }
  else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT)
  {
    if((ControlMsg.ModeDisp[ARMSET_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == ARMSET_MODE))
    {
      MotionModeTemp.MotionArm++;
      if(MotionModeTemp.MotionArm > ROBOT_MAIN_MS) MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;				  
    }
    else if((ControlMsg.ModeDisp[CHUCK_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == CHUCK_MODE))
    {
      MotionModeTemp.MainChuck ^= USE;
    }
    else if((ControlMsg.ModeDisp[VACUUM_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == VACUUM_MODE))
    {
      MotionModeTemp.MainVaccum ^= USE;
    }
    else if((ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == CHUCK_ROTATION_MODE))
    {
      MotionModeTemp.MainRotateChuck ^= USE;
    }
    else if((ControlMsg.ModeDisp[OUTSIDE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == OUTSIDE_MODE))
    {
      //IMMType
      if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.RotationState == NO_ROTATION))
        MotionModeTemp.OutsideWait = NO_USE;
      else
        MotionModeTemp.OutsideWait ^= USE;
    }
    else if((ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_TAKEOUT_MODE))
    {
      MotionModeTemp.MainArmType++;
      if(MotionModeTemp.MainArmType > ARM_I_TYPE) MotionModeTemp.MainArmType = ARM_L_TYPE;
    }
    else if((ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == SUB_TAKEOUT_MODE))
    {
      MotionModeTemp.SubArmType++;
      if(MotionModeTemp.SubArmType > ARM_I_TYPE) MotionModeTemp.SubArmType = ARM_L_TYPE;
    }
    else if((ControlMsg.ModeDisp[MAIN_DOWN_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_DOWN_MODE))
    {
      MotionModeTemp.MainArmDownPos++;
      if(MotionModeTemp.MainArmDownPos > ARM_DOWNPOS_CLAMP) MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_NOZZLE;
    }
    else if((ControlMsg.ModeDisp[SUB_DOWN_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == SUB_DOWN_MODE))
    {
      MotionModeTemp.SubArmDownPos++;
      if(MotionModeTemp.SubArmDownPos > ARM_DOWNPOS_CLAMP) MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
    }
    else if((ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_CHUCK_RELEASE_MODE))
    {
      //IMMType
      if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.ChuckOff = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          MotionModeTemp.ChuckOff++;
          if(MotionModeTemp.ChuckOff > RELEASE_POS_ChuRo) MotionModeTemp.ChuckOff = RELEASE_POS_OUTSIDE;
        }
      }
      else
      {
        MotionModeTemp.ChuckOff++;
        if(MotionModeTemp.ChuckOff > RELEASE_POS_2ndUP) MotionModeTemp.ChuckOff = RELEASE_POS_INMOLD;
      }
    }
    else if((ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == VACUUM_RELEASE_MODE))
    {
      //IMMType
      if((RobotCfg.RobotType == ROBOT_TYPE_XC) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.VaccumOff = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          MotionModeTemp.VaccumOff++;
          if(MotionModeTemp.VaccumOff > RELEASE_POS_ChuRo) MotionModeTemp.VaccumOff = RELEASE_POS_OUTSIDE;
        }
      }
      else
      {
        MotionModeTemp.VaccumOff++;
        if(MotionModeTemp.VaccumOff > RELEASE_POS_2ndUP) MotionModeTemp.VaccumOff = RELEASE_POS_INMOLD;
      }
    }
    else if((ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == SUB_CHUCK_RELEASE_MODE))
    {
      MotionModeTemp.SubArmOff++;
      if(MotionModeTemp.SubArmOff > RELEASE_POS_2ndUP) MotionModeTemp.SubArmOff = RELEASE_POS_INMOLD;
    }
    else if((ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_CHUCK_REJECT_MODE))
    {
      //IMMType
      if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.ChuckRejectPos = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          MotionModeTemp.ChuckRejectPos++;
          if(MotionModeTemp.ChuckRejectPos > RELEASE_POS_ChuRo) MotionModeTemp.ChuckRejectPos = RELEASE_POS_OUTSIDE;
        }
      }
      else
      {
        MotionModeTemp.ChuckRejectPos++;
        if(MotionModeTemp.ChuckRejectPos > RELEASE_POS_2ndUP) MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
      }
    }
    else if((ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_VAUUM_REJECT_MODE))
    {
      //IMMType
      if((RobotCfg.RobotType == ROBOT_TYPE_XC) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.VaccumRejectPos = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          MotionModeTemp.VaccumRejectPos++;
          if(MotionModeTemp.VaccumRejectPos > RELEASE_POS_ChuRo) MotionModeTemp.VaccumRejectPos = RELEASE_POS_OUTSIDE;
        }
      }
      else
      {
        MotionModeTemp.VaccumRejectPos++;
        if(MotionModeTemp.VaccumRejectPos > RELEASE_POS_2ndUP) MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
      }
	 }
  }
}

void StepCheck(void)
{//0~19 20번 반복 MAX_INDEX 20
  for (uint8_t AutoStepSetting = 0 ; AutoStepSetting < MAX_INDEX ; AutoStepSetting++)
  {
    if (ControlMsg.StepDispTemp[AutoStepSetting] == DOWN_STEP)
    {
      AutoStep[AutoStepSetting] = DOWN_TIME;         //0
      AutoStepDelay[AutoStepSetting] = CycleDelay.DownDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == KICK_STEP)
    {
      AutoStep[AutoStepSetting] = KICK_TIME;         //1
      AutoStepDelay[AutoStepSetting] = CycleDelay.KickDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == EJECTOR_STEP)
    {
      AutoStep[AutoStepSetting] = EJECTOR_TIME;         //2
      AutoStepDelay[AutoStepSetting] = CycleDelay.EjectorDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == CHUCK_STEP)
    {
      AutoStep[AutoStepSetting] = CHUCK_TIME;         //3
      AutoStepDelay[AutoStepSetting] = CycleDelay.ChuckDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == KICK_RETURN_STEP)
    {
      AutoStep[AutoStepSetting] = KICK_RETURN_TIME;         //4
      AutoStepDelay[AutoStepSetting] = CycleDelay.KickReturnDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == UP_STEP)
    {
      AutoStep[AutoStepSetting] = UP_TIME;         //5
      AutoStepDelay[AutoStepSetting] = CycleDelay.UpDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == SWING_STEP)
    {
      AutoStep[AutoStepSetting] = SWING_TIME;         //6
      AutoStepDelay[AutoStepSetting] = CycleDelay.SwingDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == CHUCK_ROTATION_STEP)
    {
      AutoStep[AutoStepSetting] = 0xff;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == SECOND_DOWN_STEP)
    {
      AutoStep[AutoStepSetting] = SECOND_DOWN_TIME;         //7
      AutoStepDelay[AutoStepSetting] = CycleDelay.Down2ndDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == CHUCK_RELEASE_STEP)
    {
      AutoStep[AutoStepSetting] = RELEASE_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.OpenDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == SECOND_UP_STEP)
    {
      AutoStep[AutoStepSetting] = SECOND_UP_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.Up2ndDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == CHUCK_ROTATION_RETURN_STEP)
    {
      AutoStep[AutoStepSetting] = CHUCK_ROTATION_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.ChuckRotateReturnDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == SWING_RETURN_STEP)
    {
      AutoStep[AutoStepSetting] = SWING_RETURN_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.SwingReturnDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == NIPPERCUT_STEP)
    {
      AutoStep[AutoStepSetting] = NIPPER_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.NipperOnDelay;
    }
    else if (SWING_RETURN_STEP < ControlMsg.StepDispTemp[AutoStepSetting])
    {
      AutoStep[AutoStepSetting] = 0xff;
      AutoStepDelay[AutoStepSetting] = 0xff;
    }
  }
}



void OpenMold(uint16_t MoldNo)        //ActiveRun.c서 사용
{
  ReadMold(&MoldMsg,MoldNo);
  memcpy(&(RobotCfg.Count), &(MoldMsg.Count), sizeof(COUNT_MSG));
  memcpy(&(RobotCfg.Function), &(MoldMsg.Function), sizeof(FUNC_MSG));
  memcpy(&(RobotCfg.MoldName), &(MoldMsg.MoldName), sizeof(MoldMsg.MoldName));
  memcpy(&(RobotCfg.MoldNo), &(MoldMsg.MoldNo), sizeof(MoldMsg.MoldNo));
  memcpy(&(RobotCfg.MotionDelay), &(MoldMsg.MotionDelay), sizeof(MOTION_DELAY));
  memcpy(&(RobotCfg.MotionMode), &(MoldMsg.MotionMode), sizeof(MOTION_MODE));
  TimerDispCheck();
  ModeDispCheck(MoldMsg.MoldNo);
  StepCheck();
  MakeSequence();
}

void WriteFunction(void)             //ActiveRun.c서 사용
{
  memcpy(&(MoldMsg.Function), &RobotCfg.Function, sizeof(FUNC_MSG));
  memset(SeqMsg.SeqDispArr,0,sizeof(SeqMsg.SeqDispArr));
  memset(ControlMsg.StepDispTemp,0,sizeof(ControlMsg.StepDispTemp));
  memset(SeqMsg.SeqArr,0,sizeof(SeqMsg.SeqArr));
  //  WriteMold(&MoldMsg);
  WriteRobotCfg(&RobotCfg);
  MakeSequence();
}

void ResetAutoSetting(void)           //AppMain.c서 사용
{
  StepCheck();
  ControlMsg.AutoPageNo = 0;
  ControlMsg.AutoLineNo = 0;
  AutoStepPos = 0;
}

void AutoState(uint8_t AutoState)     //AppMain.c, ActivRun.c서 사용
{
  static enum
  {
    AUTO_INIT = 0,
    AUTO_OPERATION,
    AUTO_NEXT_STEP,
    AUTO_TIMER_RESET,
  }AutoLoopState = AUTO_INIT;
  
  
  //  SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
  //  SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING;
  //  
  
  switch(AutoLoopState)  
  {
	 //AUTO_INIT//AUTO_INIT//AUTO_INIT//AUTO_INIT//AUTO_INIT//AUTO_INIT//AUTO_INIT//AUTO_INIT//AUTO_INIT
  case AUTO_INIT:
	 break;
	 
    //AUTO_TIMER_RESET//AUTO_TIMER_RESET//AUTO_TIMER_RESET//AUTO_TIMER_RESET//AUTO_TIMER_RESET//AUTO_TIMER_RESET//AUTO_TIMER_RESET
  case AUTO_TIMER_RESET:
    SetSysTick(&AutoTempTick,TIME_CYCLE);
    AutoLoopState = AUTO_OPERATION;
    break;
    //AUTO_NEXT_STEP//AUTO_NEXT_STEP//AUTO_NEXT_STEP//AUTO_NEXT_STEP//AUTO_NEXT_STEP//AUTO_NEXT_STEP//AUTO_NEXT_STEP//AUTO_NEXT_STEP
  case AUTO_NEXT_STEP:
    break;
  }
  
  
  
  //Count
  if (AutoState == MODE_AUTO_OP) 
  {
    if((CountMsg.TotalCnt != RobotCfg.Count.TotalCnt) || (CountMsg.DetectFail != RobotCfg.Count.DetectFail) || (CountMsg.RejectCnt != RobotCfg.Count.RejectCnt))
    {
      CountMsg.TotalCnt = RobotCfg.Count.TotalCnt;
      CountMsg.DetectFail = RobotCfg.Count.DetectFail;
      CountMsg.RejectCnt = RobotCfg.Count.RejectCnt;
      
      //		memcpy(&(MoldMsg.Count), &CountMsg, sizeof(CountMsg));
      //		//		WriteMold(&MoldMsg);
      //		WriteRobotCfg(&RobotCfg);
      //		if(ControlMsg.TpMode == AUTO_OPERATING) 
      //		{
      //		  fPreviousPage = AUTO_PAGE_SD;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);	
      //		  ControlMsg.TpMode = SD_BACKUP;
      //		}
      //		else if(ControlMsg.TpMode == AUTO_COUNTER) 
      //		{
      //		  fPreviousPage = AUTO_COUNTER_PAGE_SD;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);	
      //		  ControlMsg.TpMode = SD_BACKUP;
      //		}
      //		else if(ControlMsg.TpMode == AUTO_TIMER) 
      //		{
      //		  fPreviousPage = AUTO_TIMER_PAGE_SD;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);	
      //		  ControlMsg.TpMode = SD_BACKUP;
      //		}
    }
    if((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_OFF))
    {
      memcpy(&(MoldMsg.Count), &CountMsg, sizeof(CountMsg));
      //		WriteMold(&MoldMsg);
      WriteRobotCfg(&RobotCfg);
      if(ControlMsg.TpMode == AUTO_OPERATING) 
      {
        fPreviousPage = MANUAL_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      //		else if(ControlMsg.TpMode == AUTO_COUNTER) 
      //		{
      //		  fPreviousPage = MANUAL_PAGE_SD;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);	
      //		  ControlMsg.TpMode = SD_BACKUP;
      //		}
      //		else if(ControlMsg.TpMode == AUTO_TIMER) 
      //		{
      //		  fPreviousPage = MANUAL_PAGE_SD;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);	
      //		  ControlMsg.TpMode = SD_BACKUP;
      //		}
      ClearAuto();
      AutoLoopState = AUTO_INIT;
    }
  }
  
  
  //STOP
  if(ControlMsg.KeyValue == KEY_MENU_STOP)
  {
    memcpy(&(MoldMsg.Count), &CountMsg, sizeof(CountMsg));
    //		WriteMold(&MoldMsg);
    WriteRobotCfg(&RobotCfg);
    if(ControlMsg.TpMode == AUTO_OPERATING || ControlMsg.TpMode == AUTO_MODE || ControlMsg.TpMode == AUTO_COUNTER || 
       ControlMsg.TpMode == AUTO_IO || ControlMsg.TpMode == AUTO_TIMER || ControlMsg.TpMode == AUTO_ERROR_LIST)//201123
    {
      fPreviousPage = MANUAL_PAGE_SD;
      SetSysTick(&SDTempTick, TIME_SC_DELAY);	
      ControlMsg.TpMode = SD_BACKUP;
    }
    else if(ControlMsg.TpMode == MANUAL_CYCLE_OPERATING)	//201123 ??
    {
      ControlMsg.TpMode = MANUAL_OPERATING;
      fPreviousPage = WAITING_SD;
      SetSysTick(&SDTempTick, TIME_SC_DELAY);	
    }
    //	 else if(ControlMsg.TpMode == AUTO_COUNTER) 
    //	 {
    //		fPreviousPage = MANUAL_PAGE_SD;
    //		SetSysTick(&SDTempTick, TIME_SC_DELAY);	
    //		ControlMsg.TpMode = SD_BACKUP;
    //	 }
    //	 else if(ControlMsg.TpMode == AUTO_TIMER) 
    //	 {
    //		fPreviousPage = MANUAL_PAGE_SD;
    //		SetSysTick(&SDTempTick, TIME_SC_DELAY);	
    //		ControlMsg.TpMode = SD_BACKUP;
    //	 }
    ClearAuto();
    AutoLoopState = AUTO_INIT;
  }
}

void ManualInterlock(void)              //ActiveRun.c서 사용
{
  IOMsg.Y2B_Ejector = SIGNAL_ON;
  if(RobotCfg.MotionMode.OutsideWait == USE)
  {
    if(IOMsg.X15_SwingReturnOk == SIGNAL_ON) IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
    else if (IOMsg.X14_SwingOk == SIGNAL_ON)	IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
  }
  else if(RobotCfg.MotionMode.OutsideWait == NO_USE)
  {
    if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)
    {
      if((IOMsg.X15_SwingReturnOk == SIGNAL_ON) && ((IOMsg.Y20_Down == SIGNAL_ON) || (IOMsg.Y2D_SubUp == SIGNAL_ON)))
        IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
      else IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
    }
    else if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN)
    {
      if((IOMsg.X15_SwingReturnOk == SIGNAL_ON) && (IOMsg.Y20_Down == SIGNAL_ON))
        IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
      else IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
    }
    else if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB)
    {
      if((IOMsg.X15_SwingReturnOk == SIGNAL_ON) && (IOMsg.Y2D_SubUp == SIGNAL_ON))
        IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
      else IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
    }
  }
#ifdef SWING_IO_OFF
  if(IOMsg.X15_SwingReturnOk == SIGNAL_ON) IOMsg.Y2F_SwingReturn = SIGNAL_OFF;
  else if(IOMsg.X14_SwingOk == SIGNAL_ON) IOMsg.Y23_Swing = SIGNAL_OFF;
#endif
  if((RobotCfg.Setting.ItlAutoInjection == USE) && (RobotCfg.Setting.ItlSaftyDoor == USE))
  {
    if(IOMsg.X19_AutoInjection == SIGNAL_ON) AutoInjectionTemp = SIGNAL_ON;
    if((AutoInjectionTemp == SIGNAL_ON) && (IOMsg.X18_MoldOpenComplete == SIGNAL_ON))
    {
      IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
      if(IOMsg.X1A_SaftyDoor == SIGNAL_OFF) 
      {
        AutoInjectionTemp = SIGNAL_OFF;
        IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
      }
    }
  }
  //  if((RobotCfg.Setting.ItlSaftyDoor == NO_USE) && (IOMsg.X18_MoldOpenComplete == SIGNAL_ON) && (IOMsg.Y2A_MoldOpenClose == SIGNAL_OFF))
  //  {
  //	 if((RobotCfg.MotionMode.OutsideWait == NO_USE) && (IOMsg.X11_MainArmUpComplete == SIGNAL_ON))
  //		IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
  //	 else if((RobotCfg.MotionMode.OutsideWait == USE) && (IOMsg.X11_MainArmUpComplete == SIGNAL_ON) && (IOMsg.X14_SwingOk))
  //		IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
  //  }
}
//printf("%d - %d /// ",RobotCfg.Setting.IMMType,RobotCfg.Setting.RotationState);
void ManualIO(void)
{
  //KEY 1
  if(ControlMsg.KeyValue == KEY_MAIN_CHUCK)
  {
    if(((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)) 
       && (RobotCfg.MotionMode.MainChuck == USE))
      IOMsg.Y22_Chuck ^= SIGNAL_ON;
  }
  
  //KEY 2
  else if(ControlMsg.KeyValue == KEY_MAIN_VACUUM)
  {
    if((RobotCfg.RobotType == ROBOT_TYPE_TWIN) || (RobotCfg.RobotType == ROBOT_TYPE_XC))
    {
      if(((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)) 
         && (RobotCfg.MotionMode.MainVaccum == USE))
        IOMsg.Y25_Vaccum ^= SIGNAL_ON;
    }
  }
  
  //KEY 3
  else if(ControlMsg.KeyValue == KEY_MAIN_CHUCK_ROTATE)
  {
    if(RobotCfg.RobotType != ROBOT_TYPE_A)
    {
      if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && (IOMsg.Y21_Kick == SIGNAL_OFF) && (IOMsg.Y24_ChuckRotation == SIGNAL_OFF))
      {
        RobotCfg.ErrCode = MANUAL_ERR_CHYCK_RO;
        ControlMsg.TpMode = MANUAL_ERROR;
      }
      else
      {
        if(((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)) 
           && (RobotCfg.MotionMode.MainRotateChuck == USE))
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE))
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
          {
            if(!((IOMsg.X15_SwingReturnOk == SIGNAL_ON) && (IOMsg.Y20_Down == SIGNAL_ON)))
              IOMsg.Y24_ChuckRotation ^= SIGNAL_ON;
          }
        }
      }
    }
  }
  
  //KEY 4
  else if(ControlMsg.KeyValue == KEY_MAIN_UPDOWN)
  {
    if(!((IOMsg.X18_MoldOpenComplete == SIGNAL_OFF) && (IOMsg.X15_SwingReturnOk == SIGNAL_ON)))
    {
      if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN))
      {
		  
		  
        if(RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) 
        {
          if(((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y21_Kick == SIGNAL_OFF))
             || ((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE) && (IOMsg.Y21_Kick == SIGNAL_ON)))
          {
            if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
               || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            {
              if(!((IOMsg.X15_SwingReturnOk == SIGNAL_ON) && (IOMsg.Y24_ChuckRotation == SIGNAL_ON)))
                IOMsg.Y20_Down ^= SIGNAL_ON;
            }
          }
        }
        else 
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
          {
            if(!((IOMsg.X15_SwingReturnOk == SIGNAL_ON) && (IOMsg.Y24_ChuckRotation == SIGNAL_ON)))
              IOMsg.Y20_Down ^= SIGNAL_ON;
          }
        }
      }
    }
    else if(IOMsg.X18_MoldOpenComplete == SIGNAL_OFF)
    {
      RobotCfg.ErrCode = ERRCODE_MO_DOWN;
      RobotCfg.ErrorState = ERROR_STATE_OCCUR;
    }
  }
  
  //KEY 5
  else if(ControlMsg.KeyValue == KEY_MAIN_FW)
  {
    if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && (IOMsg.Y21_Kick == SIGNAL_ON) && (IOMsg.Y24_ChuckRotation == SIGNAL_ON))
    {
      RobotCfg.ErrCode = MANUAL_ERR_CHYCK_ROR;
      ControlMsg.TpMode = MANUAL_ERROR;
    }
    else
    {
      if((RobotCfg.RobotType == ROBOT_TYPE_TWIN) && (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS))
      {
        if((IOMsg.X11_MainArmUpComplete == SIGNAL_OFF) && (IOMsg.X1G_SubUpComplete == SIGNAL_ON))
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE))
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            IOMsg.Y21_Kick ^= SIGNAL_ON;
        }
        else if((IOMsg.X11_MainArmUpComplete == SIGNAL_OFF) && (IOMsg.X1G_SubUpComplete == SIGNAL_OFF))
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE))
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
          {
            IOMsg.Y21_Kick ^= SIGNAL_ON;
            IOMsg.Y2E_SubKick ^= SIGNAL_ON;
          }
        }
        else if((IOMsg.X11_MainArmUpComplete == SIGNAL_ON) && (IOMsg.X1G_SubUpComplete == SIGNAL_ON))
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
          {
            IOMsg.Y21_Kick ^= SIGNAL_ON;
            IOMsg.Y2E_SubKick ^= SIGNAL_ON;
          }
        }
        else if((IOMsg.X11_MainArmUpComplete == SIGNAL_ON) && (IOMsg.X1G_SubUpComplete == SIGNAL_OFF))
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            IOMsg.Y2E_SubKick ^= SIGNAL_ON;
        }
      }
      else 
      {
        if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) 
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            IOMsg.Y21_Kick ^= SIGNAL_ON;
        }
        else if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) 
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            IOMsg.Y2E_SubKick ^= SIGNAL_ON;
        }
      }
    }
  }
  
  //KEY 6 회전
  else if (ControlMsg.KeyValue == KEY_SWING)
  {
    //IMMType
	if (RobotCfg.Setting.RotationState == NO_ROTATION)
    {
      RobotCfg.ErrCode = ERRCODE_SWING;
      RobotCfg.ErrorState = ERROR_STATE_OCCUR;
    }
    else
    {
      if(((RobotCfg.RobotType == ROBOT_TYPE_TWIN) && (IOMsg.X11_MainArmUpComplete == SIGNAL_ON) && (IOMsg.X1G_SubUpComplete == SIGNAL_ON) && (IOMsg.Y20_Down == SIGNAL_OFF) && (IOMsg.Y2D_SubUp == SIGNAL_OFF))
         || ((RobotCfg.RobotType != ROBOT_TYPE_TWIN) && (IOMsg.X11_MainArmUpComplete == SIGNAL_ON) && (IOMsg.Y20_Down == SIGNAL_OFF)))
      {
        if(RobotCfg.MotionMode.OutsideWait == NO_USE)    //외부대기X
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
          {
            if(IOMsg.Y23_Swing == SIGNAL_ON) 
            {
              IOMsg.Y23_Swing = SIGNAL_OFF;
              IOMsg.Y2F_SwingReturn = SIGNAL_ON;
            }
            else if(IOMsg.Y2F_SwingReturn == SIGNAL_ON) 
            {
              IOMsg.Y2F_SwingReturn = SIGNAL_OFF;
              IOMsg.Y23_Swing = SIGNAL_ON;
            }
            else if((IOMsg.Y2F_SwingReturn == SIGNAL_OFF) && (IOMsg.Y23_Swing == SIGNAL_OFF))
            {
              if(IOMsg.X14_SwingOk == SIGNAL_ON) IOMsg.Y2F_SwingReturn = SIGNAL_ON;
              else if(IOMsg.X15_SwingReturnOk == SIGNAL_ON) IOMsg.Y23_Swing = SIGNAL_ON;
            }
          }
        }
        else if(RobotCfg.MotionMode.OutsideWait == USE)  //외부대기이면
        {
          if(IOMsg.X18_MoldOpenComplete == SIGNAL_ON)    //사출기 IO 형개완료 신호 ON이면
          {
            if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
               || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            {
              if(IOMsg.Y23_Swing == SIGNAL_ON) 
              {
                IOMsg.Y23_Swing = SIGNAL_OFF;
                IOMsg.Y2F_SwingReturn = SIGNAL_ON;
              }
              else if(IOMsg.Y2F_SwingReturn == SIGNAL_ON) 
              {
                IOMsg.Y2F_SwingReturn = SIGNAL_OFF;
                IOMsg.Y23_Swing = SIGNAL_ON;
              }
              else if((IOMsg.Y2F_SwingReturn == SIGNAL_OFF) && (IOMsg.Y23_Swing == SIGNAL_OFF))
              {
                if(IOMsg.X14_SwingOk == SIGNAL_ON) IOMsg.Y2F_SwingReturn = SIGNAL_ON;
                else if(IOMsg.X15_SwingReturnOk == SIGNAL_ON) IOMsg.Y23_Swing = SIGNAL_ON;
              }
            }
          }
        }
      }
    }
  }
  
  //런너측 상하강 버튼
  else if(ControlMsg.KeyValue == KEY_RUNNER_UPDOWN)
  {
    if(!((IOMsg.X18_MoldOpenComplete == SIGNAL_OFF) && (IOMsg.X15_SwingReturnOk == SIGNAL_ON)))
    {
      if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB))
      {
        if(RobotCfg.MotionMode.SubArmType == ARM_L_TYPE) 
        {
          if(((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y2E_SubKick == SIGNAL_ON))
             || ((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE) && (IOMsg.Y2E_SubKick == SIGNAL_OFF)))
          {
            if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
               || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
              IOMsg.Y2D_SubUp ^= SIGNAL_ON;
          }
        }
        else 
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            IOMsg.Y2D_SubUp ^= SIGNAL_ON;
        }
      }
    }
    else if(IOMsg.X18_MoldOpenComplete == SIGNAL_OFF)
    {
      RobotCfg.ErrCode = ERRCODE_MO_DOWN;
      RobotCfg.ErrorState = ERROR_STATE_OCCUR;
    }
  }
  
  //런너측 집게 버튼
  else if(ControlMsg.KeyValue == KEY_RUNNER_GRIPPER)
  {
    if(RobotCfg.RobotType == ROBOT_TYPE_TWIN)
    {
      if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)) 
        IOMsg.Y27_SubGrip ^= SIGNAL_ON;
    }
  }
  //Nipper
  //			 if(RobotCfg.RobotType == ROBOT_TYPE_XN)
  //			 {
  //				if(ControlMsg.KeyValue == KEY_RUNNER_UPDOWN)
  //				{
  //				  if(RobotCfg.MotionMode.MainNipper == USE) IOMsg.Y26_Nipper ^= SIGNAL_ON;
  //				} 
  //			 }
}

void LoopAppMain(void)
{
//  //Tick
  static uint64_t RTCTempTick = 0;
  static uint64_t LoadingTempTick = 0;
  static uint64_t SCTempTick = 0;

  static enum
  {
    INIT = 0,
    LOAD_SETTINGS = 1,
    ACTIVE_RUN = 2,
    MANUAL_OPERATING_COMM_LCD = 3,
  }MainLoopState = INIT; 
  
  switch(MainLoopState)
  {
	 //INIT//INIT//INIT//INIT//INIT//INIT//INIT//INIT//INIT//INIT//INIT//INIT//INIT//INIT
  case INIT:
    SetSysTick(&LoadingTempTick, TIME_BOOTING);
    SetSysTick(&RTCTempTick,TIME_RTC); 
    SetSysTick(&SCTempTick, TIME_SC_DELAY);
    
    ControlMsg.TpMode = INIT_READ_TYPE;
    ControlMsg.IsChangedLCD = NEED_CHANGE;
    
    MainLoopState = LOAD_SETTINGS; 
    break;
	 
    //LOAD_SETTINGS//LOAD_SETTINGS//LOAD_SETTINGS//LOAD_SETTINGS//LOAD_SETTINGS//LOAD_SETTINGS//LOAD_SETTINGS
  case LOAD_SETTINGS:
    ControlMsg.TpMode = INIT_READ_TYPE;
    ControlMsg.IsChangedLCD = NEED_CHANGE;
    MainLoopState = ACTIVE_RUN; 
    break;
	 
    //ACTIVE_RUN//ACTIVE_RUN//ACTIVE_RUN//ACTIVE_RUN//ACTIVE_RUN//ACTIVE_RUN//ACTIVE_RUN//ACTIVE_RUN//ACTIVE_RUN
  case ACTIVE_RUN:
//	 ActiveRun(&ControlMsg.TpMode);    //아래 코드

	 ActiveRun();    //아래 코드

    ControlMsg.IsChangedLCD = NEED_CHANGE;        //1
    //		MainLoopState = MANUAL_OPERATING_COMM_LCD;   //주석처리돼있음
    break;
    
  case MANUAL_OPERATING_COMM_LCD:                  //이쪽으로 넘어가는 경우가 없어보임
    //		RefreshState();
    //		ControlMsg.TpMode = MANUAL_OPERATING;
    ControlMsg.IsChangedLCD = NEED_CHANGE;
    MainLoopState = ACTIVE_RUN;
    break;
  }
  ControlMsg.KeyReady = KEY_READY;
}
