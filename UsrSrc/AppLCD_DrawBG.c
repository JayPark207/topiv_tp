/* Includes -------------------------------------------------------------------*/
#include "AppLCD_DrawBG.h"
#include "PageBackground.h"
#include <string.h>

extern uint8_t Display_Scr[1024];

extern ROBOT_CONFIG RobotCfg;
extern CONTROL_MSG ControlMsg;
extern IO_MSG IOMsg;
extern SEQ_MSG SeqMsg;
extern ERROR_MSG ErrMsg;

void Draw_Background(void)
{
  switch(ControlMsg.TpMode)
  {
  case INIT_READ_TYPE:
  case INIT_DISP_LOGO:
  case INIT_LOAD_SD:
  case INIT_SC:
	 memcpy(Display_Scr, Background_Graphic + (BACKGROUND_LOGO * 1024), 1024);   //BACKGROUND_LOGO 0     0~1023
	 break;
	 
	 
  case AUTO_MESSAGE:
  case AUTO_MODE:	
  case MANUAL_MODE:
  case MANUAL_MOLD_MODE:
  case SETTING_MANUFACTURER:
  case MANUAL_VERSION:
	 memcpy(Display_Scr, Background_Graphic + (BACKGROUND_OUTLINE * 1024), 1024);  //BACKGROUND_OUTLINE 1  //1024~2047
	 break;
	 
  case OCCUR_ERROR:   
	 memcpy(Display_Scr, Background_Graphic + (BACKGROUND_ERROR * 1024), 1024);    //BACKGROUND_ERROR 2     //2048~3071
	 break;
	 
  case AUTO_OPERATING:
  case AUTO_COUNTER:
  case AUTO_TIMER:
  case MANUAL_COUNTER:
  case MANUAL_TIMER:
  case MANUAL_STEP_OPERATING:
  case MANUAL_CYCLE_OPERATING:
  case MANUAL_MOLD_MANAGE:
  case MANUAL_MOLD_NEW:
  case MANUAL_MOLD_DELETE:
	 memcpy(Display_Scr, Background_Graphic + (BACKGROUND_TOP_HALF * 1024), 1024);    //BACKGROUND_TOP_HALF 3   3072~4095
	 break;
	 
  case MANUAL_MOLD_SEARCH:
	 memcpy(Display_Scr, Background_Graphic + (BACKGROUND_TOP_HALF * 1024), 1024);
	 break;
	 
  case MANUAL_OPERATING:
	 memcpy(Display_Scr, Background_Graphic + (BACKGROUND_HALF * 1024), 1024);     //BACKGROUND_HALF 4   4096~5119
	 break;
	 
  case AUTO_IO:
  case MANUAL_IO:
	 memcpy(Display_Scr, Background_Graphic + (BACKGROUND_IO * 1024), 1024);         //BACKGROUND_IO 5    5120~6143
	 break;
	 
  case MANUAL_ERROR:
  case MANUAL_HOMING : 
  case MANUAL_MOLD_DELETE_ING:
	 memcpy(Display_Scr, Background_Graphic + (BACKGROUND_OUTLINE * 1024), 1024);      //BACKGROUND_OUTLINE  1
	 break;
	 
  default:
	 break;
  }
  
}