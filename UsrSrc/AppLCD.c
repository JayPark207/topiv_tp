#include "main.h"
#include "AppLCD.h"
#include <string.h>
#include "PageBackground.h"
#include "Icon.h"
#include "page_msg.h"
#include "ksc2kssm.h"
#include "Han10x10.h"
#include "rom8x8.h"
#include <stdarg.h>
#include "AppKey.h"
#include "AppLCD_DrawBG.h"
#include "AppLCD_DrawIcon.h"
#include "AppLCD_DrawText.h"


extern IO_MSG IOMsg;
extern ERROR_MSG ErrMsg;
extern ERROR_MSG ErrMsgTemp[100];
extern CONTROL_MSG ControlMsg;
extern ROBOT_CONFIG RobotCfg;
extern MOTION_DELAY MotionDelayTemp;
extern MOTION_MODE MotionModeTemp;
//extern MANUFACTURER_SETTING SettingTemp;
extern MOTION_DELAY CycleDelay;
extern MOLD_MSG MoldMsg;

extern char NewMoldName[MAX_MOLDNAME_LENGTH];
extern char NewMoldNameTemp[MAX_MOLDNAME_LENGTH];
extern uint8_t MoldNameCharPos;
extern uint8_t MoldNamePos;


extern uint8_t AutoStepDelay[20];
extern void Draw_Background(void);
extern void Draw_Icon(uint32_t, uint32_t, uint32_t, uint8_t, uint8_t);
extern void Draw_Icon1(void);

char StrTemp[6][5];

char MoldNameStr0[10];
char MoldNameStr1[10];
char MoldNameStr2[10];

uint8_t Display_Scr[1024];
uint8_t lcd_init = 0;
//uint8_t rom8x8_bits[];

uint8_t* Font;

void ControlBacklight(uint8_t cmd)
{
  if(cmd)
	 HAL_GPIO_WritePin(LCD_BACKLIGHT_GPIO_Port, LCD_BACKLIGHT_Pin, GPIO_PIN_SET);
  else
	 HAL_GPIO_WritePin(LCD_BACKLIGHT_GPIO_Port, LCD_BACKLIGHT_Pin, GPIO_PIN_RESET);
}



void Lcd_Display_On_Off(uint32_t board, uint8_t on_off)
{
  uint32_t addr;
  
  addr = (uint32_t)(board | LCD_ENABLE | LCD_WRITE | LCD_INST);
  
  //	printf("On_Off address = %x, board = %X\n", addr, board);
  *(volatile uint8_t *)addr = (uint8_t)(on_off);
}
uint8_t LCD_ReadStatus(uint32_t board)		//board = LCD_CS1 or LCD_CS2
{
  uint32_t addr;
  uint8_t data;
  
  addr = (uint32_t)(board | LCD_ENABLE | LCD_READ | LCD_INST);
  data = *(volatile uint8_t *)addr;
  
  return data;
}
void Lcd_Write_Display_Data(uint32_t board, uint8_t data)
{
  uint32_t addr;
  
  addr = (uint32_t)(board | LCD_ENABLE | LCD_WRITE | LCD_DATA);
  *(volatile uint8_t *)addr = data;
}

void Lcd_Page_Address_Set(uint32_t board, uint8_t page_addr)
{
  uint32_t addr;
  
  addr = (uint32_t)(board | LCD_ENABLE | LCD_WRITE | LCD_INST);
  
  *(volatile uint8_t *)addr = (uint8_t)(LCD_INST_X_ADDR | page_addr);
}

void Lcd_Display_Start_Line(uint32_t board, uint8_t start_line)
{
  uint32_t addr;
  
  addr = (uint32_t)(board | LCD_ENABLE | LCD_WRITE | LCD_INST);
  *(volatile uint8_t *)addr = (uint8_t)(LCD_INST_Z_ADDR | start_line);
}

void Lcd_Set_Address(uint32_t board, uint8_t address)
{
  uint32_t addr;
  addr = (uint32_t)(board | LCD_ENABLE | LCD_WRITE | LCD_INST);
  
  *(volatile uint8_t *)addr = (uint8_t)(LCD_INST_Y_ADDR | address);
}

void LCD_WaitBusy(uint32_t board)	//board = LCD_CS1 or LCD_CS2
{
  uint8_t data;
  uint8_t chk = 0;
  
  while(chk == 0)
  {
	 data = LCD_ReadStatus(board);
	 if((data & LCD_INST_STATUS_BUSY) == 0)
	 {
		chk = 1;
	 }
  }
}
void Draw_Clear_Box(uint8_t x, uint8_t y, uint8_t width, uint8_t height)
{
  uint8_t cur_page, end_page;
  uint8_t i, j;
  
  if(((y % 8) == 0) && ((height % 8) == 0))
  {
	 cur_page = y / 8;
	 end_page = cur_page + (height / 8);
	 if(cur_page == 0)
	 {
		memset((Display_Scr + x), 0x01, width);
		cur_page++;
	 }
	 if(end_page == 8)
	 {
		memset((Display_Scr + 896 + x), 0x80, width);
		end_page--;
	 }
	 for(i = cur_page ; i < end_page ; i++)
	 {
		memset((Display_Scr + (i * 128) + x), 0x00, width);
	 }
  }
  else
  {
	 for(i = 0 ; i < height ; i++)
	 {
		cur_page = (y - (y % 8)) / 8;
		for(j = 0 ; j < width ; j++)
		{
		  *(Display_Scr + (cur_page * 128) + x + j) = (uint8_t)((*(Display_Scr + (cur_page * 128) + x + j) & (~(0x01 << (y % 8)))));
		}
		y++;
	 }
  }
}

uint8_t LCD_Draw(uint8_t left, uint8_t top, uint8_t width, uint8_t height)
{
  uint8_t status;
  uint32_t x, y;
  //森須坦軒
  if(top >= 64)
  {
	 return ERROR;
  }
  top = (uint8_t)((top - (top % 8)) / 8);
  
  if(left >= 128)
  {
	 return ERROR;
  }
  
  if((width > 128) || (width < left))
  {
	 return ERROR;
  }
  
  if((height > 64) || (height < top))
  {
	 return ERROR;
  }
  
  
  
  if((height % 8) != 0)
  {
	 height = (uint8_t)((height - (height % 8) + 8) / 8);
  }
  else
  {
	 height = (uint8_t)(height / 8);
  }
  
  if(left >= 64)
  {
	 status = 1;
  }
  else if(width <= 64)     //width亜 64左陥 拙精井酔亜 蒸嬢左績
  {
	 status = 0;
  }
  else
  {
	 status = 2;
  }
  
  //照宜焼亜澗牛敗
  if(status == 0)
  {
	 for(y = top ; y < height ; y++)
	 {
		LCD_WaitBusy(LCD_CS1);
		Lcd_Page_Address_Set(LCD_CS1, y);
		LCD_WaitBusy(LCD_CS1);
		Lcd_Set_Address(LCD_CS1, left);
		
		for(x = left ; x < width ; x++)
		{
		  LCD_WaitBusy(LCD_CS1);
		  Lcd_Write_Display_Data(LCD_CS1, *(volatile uint8_t *)(Display_Scr + (y * 128) + x));
		}
	 }
  }
  //left>=64
  else if(status == 1)
  {
	 for(y = top ; y < height ; y++)
	 {
		LCD_WaitBusy(LCD_CS2);
		Lcd_Page_Address_Set(LCD_CS2, y);
		LCD_WaitBusy(LCD_CS2);
		Lcd_Set_Address(LCD_CS2, (left - 64));
		
		for(x = (left - 64) ; x < (width - 64) ; x++)
		{
		  LCD_WaitBusy(LCD_CS2);
		  Lcd_Write_Display_Data(LCD_CS2, *(volatile uint8_t *)(Display_Scr + (y * 128) + (x + 64)));
		}
	 }
  }
  else
  {
	 for(y = top ; y < height ; y++)
	 {
		LCD_WaitBusy(LCD_CS1);
		Lcd_Page_Address_Set(LCD_CS1, y);
		LCD_WaitBusy(LCD_CS1);
		Lcd_Set_Address(LCD_CS1, left);
		
		for(x = left ; x < 64 ; x++)
		{
		  LCD_WaitBusy(LCD_CS1);
		  Lcd_Write_Display_Data(LCD_CS1, *(volatile uint8_t *)(Display_Scr + (y * 128) + x));
		}
	 }
	 
	 for(y = top ; y < height ; y++)
	 {
		LCD_WaitBusy(LCD_CS2);
		Lcd_Page_Address_Set(LCD_CS2, y);
		LCD_WaitBusy(LCD_CS2);
		Lcd_Set_Address(LCD_CS2, 0);
		
		for(x = 0 ; x < (width - 64) ; x++)
		{
		  LCD_WaitBusy(LCD_CS2);
		  Lcd_Write_Display_Data(LCD_CS2, *(volatile uint8_t *)(Display_Scr + (y * 128) + (x + 64)));
		}
	 }
  }
  return SUCCESS;
}


//////////////////////////////////////////////  廃越  //////////////////////////////////////////////
void Or_Hancode(uint8_t * addr, uint8_t * buffer)
{
  int32_t i;
  
  for(i = 28 ; i >= 0 ; i -= 4)
  {
	 *(uint32_t *)(buffer + i) |= *(uint32_t *)(addr + i);
  }
}
void Copy_Hancode(uint8_t * addr, uint8_t * buffer)
{
  memcpy(buffer, addr, 32);
}


void Hancode(uint16_t code, uint8_t *buffer)
{
  uint8_t first2[] = {0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
  uint8_t middle2[] = {0, 0, 0, 1, 2, 3, 4, 5, 0, 0, 6, 7, 8, 9, 10, 11, 0, 0, 12, 13, 14, 15, 16, 17, 0, 0, 18, 19, 20, 21};
  uint8_t last2[] = {0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 0, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27};
  uint8_t cho[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 1, 2, 4, 4, 4, 2, 1, 3, 0};
  uint8_t cho2[] = {0, 5, 5, 5, 5, 5, 5, 5, 5, 6, 7, 7, 7, 6, 6, 7, 7, 7, 6, 6, 7, 5};
  uint8_t jong[] = {0, 0, 2, 0, 2, 1, 2, 1, 2, 3, 0, 2, 1, 3, 3, 1, 2, 1, 3, 3, 1, 1};
  
  uint32_t first, middle, last;
  uint32_t offset;
  uint32_t value;
  uint8_t * hangle_addr;
  
  value = 32;
  hangle_addr = Hangle10x10;
  
  first = first2[(code >> 10) & 0x1F];			
  middle = middle2[(code >> 5) & 0x1F];			
  last = last2[(code) & 0x1F];					
  
  if(last == 0)
  {
	 offset = cho[middle] * 20 * value;
	 offset += first * value;
	 Copy_Hancode((hangle_addr + offset), buffer);
	 
	 if((first == 1) || (first == 24))
	 {
		offset = 160 * value;
	 }
	 else
	 {
		offset = 182 * value;
	 }
	 offset += middle * value;
	 Or_Hancode((hangle_addr + offset), buffer);
  }
  else
  {
	 offset = cho2[middle] * 20 * value;
	 offset += first * value;
	 Copy_Hancode((hangle_addr + offset), buffer);
	 
	 if((first == 1) || (first == 24))
	 {
		offset = 204 * value;
	 }
	 else
	 {
		offset = 226 * value;
	 }
	 offset += middle * value;
	 Or_Hancode((hangle_addr + offset), buffer);
	 offset = (248 * value) + (jong[middle] * 28 * value);
	 offset += last * value;
	 Or_Hancode((hangle_addr + offset), buffer);
  }
}


/****************************************************/
/*	func:	Draw_Hanfont										 */
/*--------------------------------------------------*/
/*	return	void												 */
/****************************************************/

void Draw_Hanfont(uint32_t x, uint32_t y, uint8_t *image)
{
  uint32_t cur_page;
  int32_t i;
  
  y = (y / 16) * 2;
  cur_page = y;
  cur_page *= 128;
  cur_page += x;
  for(i = 9 ; i >= 0 ; i--)
  {
	 *(Display_Scr + cur_page + i) |= *(image + i);
  }
  
  cur_page = y + 1;
  cur_page *= 128;
  cur_page += x;
  image += 16;
  for(i = 9 ; i >= 0 ; i--)
  {
	 *(Display_Scr + cur_page + i) |= *(image + i);
  }
}

void Get_Textbits(uint32_t ch, uint8_t *retmap)
{
  uint8_t *	bits;
  
  if(ch >= 256)
  {
	 ch = 0;
  }
  bits = Font + (ch * 16);
  memcpy(retmap, bits, 16);
}

void Draw_Font(uint32_t x, uint32_t y, uint8_t *table)
{
  uint32_t cur_page;
  int32_t i;
  
  y = (y / 16) * 2;
  cur_page = y;
  cur_page *= 128;
  cur_page += x;
  for(i = 7 ; i >= 0 ; i--)
  {
	 *(Display_Scr + cur_page + i) |= *(table + i);
  }
  
  cur_page = y + 1;
  cur_page *= 128;
  cur_page += x;
  table += 8;
  for(i = 7 ; i >= 0 ; i--)
  {
	 *(Display_Scr + cur_page + i) |= *(table + i);
  }
}

void Lcd_Printf(uint32_t x, uint32_t y, uint32_t w, char * str, uint32_t align, uint8_t clear_flag)
{
  uint16_t code = 0;
  
  uint16_t test = 0;
  uint16_t ustemp = 0;
  uint16_t base_offset = 0xB0A1;
  uint16_t base_char = 0;
  uint8_t temp[32];
  
  uint8_t n = 0, e = 0;
  char * text = str;
  uint8_t bitmap[16];
  
  if(clear_flag == REDRAW_ON)
  {
	 Draw_Clear_Box(x, (y / 16) * 16, w, 16);
  }
  
  Font = rom8x8_bits;
  //  Font = Hangle10x10;
  while(*text)
  {
	 code = *text++;
	 if(code >= 128)
	 {
		text++;
		n++;
	 }
	 else
	 {
		e++;
	 }
  }
  
  if(align == RIGHT_ALIGN)
  {
	 x = x + w - ((n * 10) + (e * 8));
  }
  else if(align == CENTER_ALIGN)
  {
	 x = x + ((w - ((n * 10) + (e * 8))) / 2);
  }
  else  //?
  {
	 
  }
  
  while(*str)
  {
	 code = *str++;
	 
	 //	 Get_Textbits(code, bitmap);
	 ////	 Draw_Font(x, y, bitmap);
	 //	 Draw_Hanfont(x, y, bitmap);
	 //	 x += 8;
	 if(code >= 128)
	 {
		code *= 256;
		code |= *str++;
		
		ustemp = code & 0x0F00;
		if(code > 0xBFFE)
		{
		  base_offset = 0xC0A1;
		  base_char = 1504;
		}
		else
		{
		  base_offset = 0xB0A1;
		  base_char = 0;
		}
		
		test = code - (base_offset | ustemp) + (base_char + (94 * (ustemp >> 8)));
		code = *(hcode + test);
		
		Hancode(code, temp);
		Draw_Hanfont(x, y, temp);
		x += 10;
	 }
	 else
	 {
		Get_Textbits(code, bitmap);
		Draw_Font(x, y, bitmap);
		x += 8;
	 }
  }
}

uint8_t Lcd_Initial(void)
{
  uint8_t error;
  
  LCD_WaitBusy(LCD_CS1);
  LCD_WaitBusy(LCD_CS2);
  
  Draw_Clear_Box(0, 0, 128, 64);
  memset(Display_Scr, 0, 1024);
  
  //	  LCD_Draw(0, 0, 128, 64);
  
  error = LCD_Draw(0, 0, 128, 64);
  
  if(error == 0)       //
	 return error;
  
  
  LCD_WaitBusy(LCD_CS1);
  Lcd_Display_Start_Line(LCD_CS1, 0x00);
  LCD_WaitBusy(LCD_CS2);
  Lcd_Display_Start_Line(LCD_CS2, 0x00);
  
  LCD_WaitBusy(LCD_CS1);
  Lcd_Display_On_Off(LCD_CS1, LCD_INST_DISPLAY_ON);     //0x60000008
  LCD_WaitBusy(LCD_CS2);
  Lcd_Display_On_Off(LCD_CS2, LCD_INST_DISPLAY_ON);     //0x60000010   0x3F
  
  //	 printf("Initialized LCD......\n");
  return error;
}


void Language_Initial(void)
{
  if(RobotCfg.Language == LANGUAGE_KOR)
  {          //double pointer
	 Page_Name_List = Page_Name_List_Kor;
	 Factory_Setup_Msg = Factory_Setup_Msg_Kor;
	 Factory_Setup_Option = Factory_Setup_Option_Kor;
	 File_Search_Msg = File_Search_Msg_Kor;
	 Delete_Page_Msg = Delete_Page_Msg_Kor;
	 Mode_Page_Msg = Mode_Page_Msg_Kor;
	 Mode_Page_Option = Mode_Page_Option_Kor;
	 IO_Page_Msg = IO_Page_Msg_Kor;
	 Timer_Page_Msg = Timer_Page_Msg_Kor;
	 Count_Setup_Page_Msg = Count_Setup_Page_Msg_Kor;
	 Step_Name_Table = Step_Name_Table_Kor;
	 Error_Pop_Page_Msg = Error_Pop_Page_Msg_Kor;
	 Massage_Pop_Msg = Massage_Pop_Msg_Kor;
	 Basic_Mode_Page_Msg = Basic_Mode_Page_Msg_Kor;
	 Basic_Mode_Page_Option = Basic_Mode_Page_Option_Kor;
	 Auto_Run_Popup_Msg = Auto_Run_Popup_Msg_Kor;
	 Error_Pop_Page_Msg = Error_Pop_Page_Msg_Kor;
	 Error_Msg = Error_Msg_Kor;
	 Error_Action_Msg = Error_Action_Msg_Kor;
	 Factory_Setup_Option_Setting = Factory_Setup_Option_SettingKor;
  }
  else if(RobotCfg.Language == LANGUAGE_ENG)//ENG
  {
	 Page_Name_List = Page_Name_List_Eng;
	 Factory_Setup_Msg = Factory_Setup_Msg_Eng;
	 Factory_Setup_Option = Factory_Setup_Option_Eng;
	 File_Search_Msg = File_Search_Msg_Eng;
	 Delete_Page_Msg = Delete_Page_Msg_Eng;
	 Mode_Page_Msg = Mode_Page_Msg_Eng;
	 Mode_Page_Option = Mode_Page_Option_Eng;
	 IO_Page_Msg = IO_Page_Msg_Eng;
	 Timer_Page_Msg = Timer_Page_Msg_Eng;
	 Count_Setup_Page_Msg = Count_Setup_Page_Msg_Eng;
	 Step_Name_Table = Step_Name_Table_Eng;
	 Error_Pop_Page_Msg = Error_Pop_Page_Msg_Eng;
	 Massage_Pop_Msg = Massage_Pop_Msg_Eng;
	 Basic_Mode_Page_Msg = Basic_Mode_Page_Msg_Eng;
	 Basic_Mode_Page_Option = Basic_Mode_Page_Option_Eng;
	 Auto_Run_Popup_Msg = Auto_Run_Popup_Msg_Eng;
	 Error_Pop_Page_Msg = Error_Pop_Page_Msg_Eng;
	 Error_Msg = Error_Msg_Eng;
	 Error_Action_Msg = Error_Action_Msg_Eng;
	 Factory_Setup_Option_Setting = Factory_Setup_Option_SettingEng;
  }
  
  //    else if(RobotCfg.Language == LANGUAGE_CHN)//CHN
  //  {
  //    Page_Name_List = Page_Name_List_China;
  //    Factory_Setup_Msg = Factory_Setup_Msg_China;
  //    Factory_Setup_Option = Factory_Setup_Option_China;
  //    File_Search_Msg = File_Search_Msg_China;
  //    Delete_Page_Msg = Delete_Page_Msg_China;
  //    Mode_Page_Msg = Mode_Page_Msg_China;
  //    Mode_Page_Option = Mode_Page_Option_China;
  //    IO_Page_Msg = IO_Page_Msg_China;
  //    Timer_Page_Msg = Timer_Page_Msg_China;
  //    Count_Setup_Page_Msg = Count_Setup_Page_Msg_China;
  //    Step_Name_Table = Step_Name_Table_China;
  //    Error_Pop_Page_Msg = Error_Pop_Page_Msg_China;
  //    Massage_Pop_Msg = Massage_Pop_Msg_China;
  //    Basic_Mode_Page_Msg = Basic_Mode_Page_Msg_China;
  //    Basic_Mode_Page_Option = Basic_Mode_Page_Option_China;
  //    Auto_Run_Popup_Msg = Auto_Run_Popup_Msg_China;
  //    Error_Pop_Page_Msg = Error_Pop_Page_Msg_China;
  //    Error_Msg = Error_Msg_China;
  //    Error_Action_Msg = Error_Action_Msg_China;
  //	 Factory_Setup_Option_Setting = Factory_Setup_Option_SettingdChina;
  //  }
}

void TimerSetting(void)
{
  for (uint8_t TimeDispSetting = 0 ; TimeDispSetting < 20 ; TimeDispSetting++)
  {
	 if (ControlMsg.TimerDispTemp[TimeDispSetting] == DOWN_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.DownDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == KICK_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.KickDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == EJECTOR_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.EjectorDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == CHUCK_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.ChuckDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == KICK_RETURN_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.KickReturnDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == UP_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.UpDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SWING_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.SwingDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SECOND_DOWN_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.Down2ndDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == RELEASE_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.OpenDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SECOND_UP_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.Up2ndDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == CHUCK_ROTATION_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.ChuckRotateReturnDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SWING_RETURN_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.SwingReturnDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == NIPPER_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.NipperOnDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == CONVEYOR_TIME)
		ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.ConveyorDelay;
  }
  
  if(ControlMsg.PageNo == 0)
  {

	 sprintf(StrTemp[0], "%1.1f", (float)((float)ControlMsg.TimerDelay[DOWN_TIME] / 10));
	 sprintf(StrTemp[1], "%1.1f", (float)((float)ControlMsg.TimerDelay[KICK_TIME] / 10));
	 sprintf(StrTemp[2], "%1.1f", (float)((float)ControlMsg.TimerDelay[EJECTOR_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 1)
  {

	 sprintf(StrTemp[0], "%1.1f", (float)((float)ControlMsg.TimerDelay[CHUCK_TIME] / 10));
	 sprintf(StrTemp[1], "%1.1f", (float)((float)ControlMsg.TimerDelay[KICK_RETURN_TIME] / 10));
	 sprintf(StrTemp[2], "%1.1f", (float)((float)ControlMsg.TimerDelay[UP_TIME] / 10));
	 
  }
  else if(ControlMsg.PageNo == 2)
  {

	 sprintf(StrTemp[0], "%1.1f", (float)((float)ControlMsg.TimerDelay[SWING_TIME] / 10));
	 sprintf(StrTemp[1], "%1.1f", (float)((float)ControlMsg.TimerDelay[SECOND_DOWN_TIME] / 10));
	 sprintf(StrTemp[2], "%1.1f", (float)((float)ControlMsg.TimerDelay[RELEASE_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 3)
  {

	 sprintf(StrTemp[0], "%1.1f", (float)((float)ControlMsg.TimerDelay[SECOND_UP_TIME] / 10));
	 sprintf(StrTemp[1], "%1.1f", (float)((float)ControlMsg.TimerDelay[CHUCK_ROTATION_TIME] / 10));
	 sprintf(StrTemp[2], "%1.1f", (float)((float)ControlMsg.TimerDelay[SWING_RETURN_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 4)
  {

	 sprintf(StrTemp[0], "%1.1f", (float)((float)ControlMsg.TimerDelay[NIPPER_TIME] / 10));
	 sprintf(StrTemp[1], "%1.1f", (float)((float)ControlMsg.TimerDelay[CONVEYOR_TIME] / 10));
	 
  }
  
  for (uint8_t TimeDispSetting = 0 ; TimeDispSetting < 20 ; TimeDispSetting++)
  {
	 if (ControlMsg.TimerDispTemp[TimeDispSetting] == DOWN_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.DownDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == KICK_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.KickDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == EJECTOR_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.EjectorDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == CHUCK_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.ChuckDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == KICK_RETURN_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.KickReturnDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == UP_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.UpDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SWING_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.SwingDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SECOND_DOWN_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.Down2ndDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == RELEASE_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.OpenDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SECOND_UP_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.Up2ndDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] ==しししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししししし CHUCK_ROTATION_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.ChuckRotateReturnDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SWING_RETURN_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.SwingReturnDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == NIPPER_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.NipperOnDelay;
	 else if (ControlMsg.TimerDispTemp[TimeDispSetting] == CONVEYOR_TIME)
		ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.ConveyorDelay;
  }
  
  if(ControlMsg.PageNo == 0)
  {
//	 sprintf(str3, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[DOWN_TIME] / 10));
//	 sprintf(str4, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[KICK_TIME] / 10));
//	 sprintf(str5, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[EJECTOR_TIME] / 10));
	 sprintf(StrTemp[3], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[DOWN_TIME] / 10));
	 sprintf(StrTemp[4], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[KICK_TIME] / 10));
	 sprintf(StrTemp[5], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[EJECTOR_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 1)
  {
//	 sprintf(str3, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[CHUCK_TIME] / 10));
//	 sprintf(str4, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[KICK_RETURN_TIME] / 10));
//	 sprintf(str5, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[UP_TIME] / 10));
	 sprintf(StrTemp[3], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[CHUCK_TIME] / 10));
	 sprintf(StrTemp[4], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[KICK_RETURN_TIME] / 10));
	 sprintf(StrTemp[5], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[UP_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 2)
  {
//	 sprintf(str3, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[SWING_TIME] / 10));
//	 sprintf(str4, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[SECOND_DOWN_TIME] / 10));
//	 sprintf(str5, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[RELEASE_TIME] / 10));
	 sprintf(StrTemp[3], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[SWING_TIME] / 10));
	 sprintf(StrTemp[4], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[SECOND_DOWN_TIME] / 10));
	 sprintf(StrTemp[5], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[RELEASE_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 3)
  {
//	 sprintf(str3, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[SECOND_UP_TIME] / 10));
//	 sprintf(str4, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[CHUCK_ROTATION_TIME] / 10));
//	 sprintf(str5, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[SWING_RETURN_TIME] / 10));
	 sprintf(StrTemp[3], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[SECOND_UP_TIME] / 10));
	 sprintf(StrTemp[4], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[CHUCK_ROTATION_TIME] / 10));
	 sprintf(StrTemp[5], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[SWING_RETURN_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 4)
  {
//	 sprintf(str3, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[NIPPER_TIME] / 10));
//	 sprintf(str4, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[CONVEYOR_TIME] / 10));
	 sprintf(StrTemp[3], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[NIPPER_TIME] / 10));
	 sprintf(StrTemp[4], "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[CONVEYOR_TIME] / 10));
  }
}

void StepTimerSetting(void)
{
  for (uint8_t CurStep = 0 ; CurStep <= sizeof(ControlMsg.StepDispTemp) ; CurStep++)  //[20] * 8bits(1byte)
  {
	 if (ControlMsg.StepDispTemp[CurStep] == DOWN_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.DownDelay;
	 else if (ControlMsg.StepDispTemp[CurStep] == KICK_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.KickDelay;
	 else if (ControlMsg.StepDispTemp[CurStep] == EJECTOR_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.EjectorDelay;
	 else if (ControlMsg.StepDispTemp[CurStep] == CHUCK_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.ChuckDelay;
	 else if (ControlMsg.StepDispTemp[CurStep] == KICK_RETURN_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.KickReturnDelay;
	 else if (ControlMsg.StepDispTemp[CurStep] == UP_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.UpDelay;
	 else if (ControlMsg.StepDispTemp[CurStep] == SWING_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.SwingDelay;
	 else if (ControlMsg.StepDispTemp[CurStep] == SECOND_DOWN_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.Down2ndDelay;
	 else if (ControlMsg.StepDispTemp[CurStep] == CHUCK_RELEASE_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.OpenDelay;
	 else if (ControlMsg.StepDispTemp[CurStep] == SECOND_UP_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.Up2ndDelay;
	 else if (ControlMsg.StepDispTemp[CurStep] == CHUCK_ROTATION_RETURN_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.ChuckRotateReturnDelay;
	 else if (ControlMsg.StepDispTemp[CurStep] == SWING_RETURN_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.SwingReturnDelay;
	 else if (ControlMsg.StepDispTemp[CurStep] == NIPPERCUT_STEP)
		ControlMsg.TimerDelay[CurStep] = RobotCfg.MotionDelay.NipperOnDelay;
	 else
		ControlMsg.TimerDelay[CurStep] = 0;
  }
  
  if((ControlMsg.PageNo == 0) || (ControlMsg.AutoPageNo == 0))
  {
	 sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[DOWN_STEP] / 10));
	 sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[KICK_STEP] / 10));
	 sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[EJECTOR_STEP] / 10));
  }
  if((ControlMsg.PageNo == 1) || (ControlMsg.AutoPageNo == 1))
  {
	 sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[CHUCK_STEP] / 10));
	 sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[KICK_RETURN_STEP] / 10));
	 sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[UP_STEP] / 10));
  }
  if((ControlMsg.PageNo == 2) || (ControlMsg.AutoPageNo == 2))
  {
	 sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[SWING_STEP] / 10));
	 sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[CHUCK_ROTATION_STEP] / 10));
	 sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[SECOND_DOWN_STEP] / 10));
  }
  if((ControlMsg.PageNo == 3) || (ControlMsg.AutoPageNo == 3))
  {
	 sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[NIPPERCUT_STEP] / 10));
	 sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[CHUCK_RELEASE_STEP] / 10));
	 sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[SECOND_UP_STEP] / 10));
  }
  if((ControlMsg.PageNo == 4) || (ControlMsg.AutoPageNo == 4))
  {
	 sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[CHUCK_ROTATION_RETURN_STEP] / 10));
	 sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[SWING_RETURN_STEP] / 10));
	 sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[14] / 10));
  }
  if((ControlMsg.PageNo == 5) || (ControlMsg.AutoPageNo == 5))
  {
	 sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[15] / 10));
	 sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[16] / 10));
	 sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[17] / 10));
  }
  if((ControlMsg.PageNo == 6) || (ControlMsg.AutoPageNo == 6))
  {
	 sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[18] / 10));
	 sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[19] / 10));
  }
  
  
  if((ControlMsg.PageNo == 0) || (ControlMsg.AutoPageNo == 0))
  {
	 sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[0] / 10));
	 sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[1] / 10));
	 sprintf(str5, "%1.1f", (float)((float)AutoStepDelay[2] / 10));
  }
  if((ControlMsg.PageNo == 1) || (ControlMsg.AutoPageNo == 1))
  {
	 sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[3] / 10));
	 sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[4] / 10));
	 sprintf(str5, "%1.1f", (float)((float)AutoStepDelay[5] / 10));
  }
  if((ControlMsg.PageNo == 2) || (ControlMsg.AutoPageNo == 2))
  {
	 sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[6] / 10));
	 sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[7] / 10));
	 sprintf(str5, "%1.1f", (float)((float)AutoStepDelay[8] / 10));
  }
  if((ControlMsg.PageNo == 3) || (ControlMsg.AutoPageNo == 3))
  {
	 sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[9] / 10));
	 sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[10] / 10));
	 sprintf(str5, "%1.1f", (float)((float)AutoStepDelay[11] / 10));
  }
  if((ControlMsg.PageNo == 4) || (ControlMsg.AutoPageNo == 4))
  {
	 sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[12] / 10));
	 sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[13] / 10));
	 sprintf(str5, "%1.1f", (float)((float)AutoStepDelay[14] / 10));
  }
  if((ControlMsg.PageNo == 5) || (ControlMsg.AutoPageNo == 5))
  {
	 sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[15] / 10));
	 sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[16] / 10));
	 sprintf(str5, "%1.1f", (float)((float)AutoStepDelay[17] / 10));
  }
  if((ControlMsg.PageNo == 6) || (ControlMsg.AutoPageNo == 6))
  {
	 sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[18]/ 10));
	 sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[19] / 10));
  }
  
  //  printf("ControlMsg.TimerDelayTemp[CHUCK_ROTATION_RETURN_STEP] : %d\n ",ControlMsg.TimerDelayTemp[CHUCK_ROTATION_RETURN_STEP]);
}

void ModeSetting(void)
{
  for (uint8_t ModeDispSetting = 0 ; ModeDispSetting < 20 ; ModeDispSetting++)
  {
	 if (ControlMsg.ModeDispTemp[ModeDispSetting] == ARMSET_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.MotionArm) + POINTER_MODE_MOTIONARM;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == CHUCK_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.MainChuck) + POINTER_MODE_USAGE;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == VACUUM_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.MainVaccum) + POINTER_MODE_USAGE;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == CHUCK_ROTATION_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.MainRotateChuck) + POINTER_MODE_USAGE;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == OUTSIDE_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.OutsideWait) + POINTER_MODE_USAGE;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == MAIN_TAKEOUT_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.MainArmType) + POINTER_MODE_MOTION;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == SUB_TAKEOUT_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.SubArmType) + POINTER_MODE_MOTION;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == MAIN_DOWN_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.MainArmDownPos) + POINTER_MODE_DOWN;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == SUB_DOWN_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.SubArmDownPos) + POINTER_MODE_DOWN;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == MAIN_CHUCK_RELEASE_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.ChuckOff) + POINTER_MODE_OPEN;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == VACUUM_RELEASE_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.VaccumOff) + POINTER_MODE_OPEN;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == SUB_CHUCK_RELEASE_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.SubArmOff) + POINTER_MODE_OPEN;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == MAIN_CHUCK_REJECT_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.ChuckRejectPos) + POINTER_MODE_REJECT;
	 else if (ControlMsg.ModeDispTemp[ModeDispSetting] == MAIN_VAUUM_REJECT_MODE)
		ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.VaccumRejectPos) + POINTER_MODE_REJECT;
  }
}

void LoopAppLCD(void)
{
  static char strMoldNoTemp[10];      //DrawText.c稽 姶
  
  static enum
  {
	 LCD_INIT = 0,
	 
	 CHECK_UPDATE,            //設 潤醤敗
	 DRAW_BACK_GROUND,
	 DRAW_ICON,
	 DRAW_TXT,
	 
	 LCD_DISTINGUISH_CHANGE,   //照床績
	 LCD_DRAW,                 //照床績
	 LCD_DISPLAY,
	 
	 LCD_OFF,					//照床績
	 LCD_ON,					   //照床績
	 
	 LCD_READ_STATUS,       //照床績
	 
	 LCD_PAGE_WRITE,        //照床績
	 
  }LCDLoopState = LCD_INIT; 
  
  
  switch(LCDLoopState)
  {
	 //LCD_INIT//LCD_INIT//LCD_INIT//LCD_INIT//LCD_INIT//LCD_INIT//LCD_INIT//LCD_INIT
  case LCD_INIT:
	 ControlBacklight(LCD_BACKLIGHT_ON);
	 HAL_GPIO_WritePin(LCD_RESET_GPIO_Port, LCD_RESET_Pin, GPIO_PIN_SET);		//NOW Disable
	 //	  HAL_GPIO_WritePin(LCD_RESET_GPIO_Port, LCD_RESET_Pin, GPIO_PIN_RESET);		//NOW Disable
	 
	 Lcd_Initial();
	 
	 LCDLoopState = CHECK_UPDATE;
	 break;
	 
	 
	 
	 //CHECK_UPDATE//CHECK_UPDATE//CHECK_UPDATE//CHECK_UPDATE//CHECK_UPDATE//CHECK_UPDATE
  case CHECK_UPDATE:
	 if(ControlMsg.IsChangedBL == NEED_CHANGE)//照床績
	 {        
		//		  if(ControlMsg.BackLight == ON) LCDLoopState = LCD_BL_ON;
		//		  else if(ControlMsg.BackLight == OFF) LCDLoopState = LCD_BL_OFF;
	 }
	 //	 printf("RobotCfg.Language : %d\n ",RobotCfg.Language);
	 Language_Initial();
	 
	 if(ControlMsg.IsChangedLCD == NEED_CHANGE) LCDLoopState = DRAW_BACK_GROUND;
	 
	 break;
	 
	 
	 
	 //DRAW_BACK_GROUND//DRAW_BACK_GROUND//DRAW_BACK_GROUND//DRAW_BACK_GROUND//DRAW_BACK_GROUND
  case DRAW_BACK_GROUND:
	 Draw_Background();         //210304
	 LCDLoopState = DRAW_ICON;
	 break;
	 
  case DRAW_ICON://DRAW_ICON DRAW_ICON DRAW_ICON DRAW_ICON DRAW_ICON DRAW_ICON DRAW_ICON DRAW_ICON
	 Draw_Icon1();              //210304
	 LCDLoopState = DRAW_TXT;
	 break;
	 
  case DRAW_TXT://DRAW_TXT DRAW_TXT DRAW_TXT DRAW_TXT DRAW_TXT DRAW_TXT DRAW_TXT DRAW_TXT DRAW_TXT 
	 Draw_Text(StrTemp);               //210304
	 LCDLoopState = LCD_DISPLAY;
	 break;
	 
  case LCD_DISPLAY:
	 LCD_Draw(0, 0, 128, 64);
	 ControlMsg.IsChangedLCD = NO_CHANGE;
	 LCDLoopState = CHECK_UPDATE;
	 break;
  }
}
