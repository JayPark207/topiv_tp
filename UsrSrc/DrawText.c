/* Includes -------------------------------------------------------------------*/
#include "DrawText.h"
#include <string.h>
#include "Icon.h"

extern uint8_t Display_Scr[1024];

extern ROBOT_CONFIG RobotCfg;
extern CONTROL_MSG ControlMsg;
extern IO_MSG IOMsg;
extern SEQ_MSG SeqMsg;
extern ERROR_MSG ErrMsg;
extern MANUFACTURER_SETTING SettingTemp;

extern uint8_t UnderbarPos;

void Draw_Text(char **StrTemp)
{
  uint8_t TimerLineCheck = 1;
  uint8_t ModeLineCheck = 1;
  uint8_t StepLineCheck = 1;
  uint8_t MoldLineCheck = 1;
  
  uint16_t ErrPage = 0;
  
  //NewMoldPage
  static uint8_t NewMoldBlink = 0;
  static uint64_t NewMoldTick = 0;
  
  static char str[10]={0,};
  
  //ErrorListPage
  static char Error0[10];          //mian  함수에 선언해놓고 포인터를 써야함 
  static char Error1[10];          //포인터 많아지면 파일나눠야함
  static char Error2[10];          //DrawText.c만 쓰임
  static char Error3[10];          //DrawText.c만 쓰임
  static char strMoldNoTemp[10];
  
  switch(ControlMsg.TpMode)
  {
  case INIT_READ_TYPE:
  case INIT_DISP_LOGO:
  case INIT_LOAD_SD:
  case INIT_SC:
	 Lcd_Printf(INIT_PAGE_MSG0_LEFT, INIT_PAGE_MSG0_BOT, INIT_PAGE_MSG0_WIDTH,
					Page_Name_List[INIT_PAGE_TEXT], CENTER_ALIGN, REDRAW_ON);     //INIT_PAGE_TEXT 1
	 break;
	 
	 
	 //MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING
  case MANUAL_OPERATING:
	 Lcd_Printf(MANUAL_PAGE_MSG0_LEFT, MANUAL_PAGE_MSG0_BOT, MANUAL_PAGE_MSG0_WIDTH,
					Page_Name_List[MANUAL_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(MANUAL_PAGE_MSG1_LEFT, MANUAL_PAGE_MSG1_BOT, MANUAL_PAGE_MSG1_WIDTH, 
					str, CENTER_ALIGN, REDRAW_OFF);
	 break;
	 
	 
	 //MANUAL_HOMINGMANUAL_HOMINGMANUAL_HOMINGMANUAL_HOMINGMANUAL_HOMINGMANUAL_HOMINGMANUAL_HOMINGMANUAL_HOMINGMANUAL_HOMING
  case MANUAL_HOMING : 
	 Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, Massage_Pop_Msg[0], LEFT_ALIGN, REDRAW_OFF);
	 break;
	 
	 
	 //AUTO_MESSAGE//AUTO_MESSAGE//AUTO_MESSAGE//AUTO_MESSAGE//AUTO_MESSAGE//AUTO_MESSAGE//AUTO_MESSAGE//AUTO_MESSAGE//AUTO_MESSAGE
  case AUTO_MESSAGE:
	 Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, Auto_Run_Popup_Msg[0], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, Auto_Run_Popup_Msg[1], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, Auto_Run_Popup_Msg[2], LEFT_ALIGN, REDRAW_OFF);
	 break;
	 
	 
	 //AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING
	 //210308수정해야함
  case AUTO_OPERATING:
	 StepTimerSetting();
	 
	 Lcd_Printf(AUTO_PAGE_MSG0_LEFT, AUTO_PAGE_MSG0_BOT, AUTO_PAGE_MSG0_WIDTH,
					Page_Name_List[FILE_AUTO_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(AUTO_PAGE_MSG1_LEFT, AUTO_PAGE_MSG1_BOT, AUTO_PAGE_MSG1_WIDTH, 
					str, CENTER_ALIGN, REDRAW_OFF);
	 
	 if (StepLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.AutoPageNo*TITLE_LINE_MAX) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
		{

		  Lcd_Printf(AUTO_PAGE_MSG2_LEFT, AUTO_PAGE_MSG2_BOT, AUTO_PAGE_MSG2_WIDTH, 
						 Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.AutoPageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(AUTO_PAGE_MSG2_OP0_LEFT, AUTO_PAGE_MSG2_OP0_BOT, AUTO_PAGE_MSG2_OP0_WIDTH, 
						 StrTemp[0], RIGHT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(AUTO_PAGE_MSG2_OP1_LEFT, AUTO_PAGE_MSG2_OP1_BOT, AUTO_PAGE_MSG2_OP1_WIDTH, 
						 StrTemp[3], RIGHT_ALIGN, REDRAW_OFF);
		}
		else StepLineCheck = DISPLAY_OFF;
	 }
	 
	 if (StepLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.AutoPageNo*TITLE_LINE_MAX+1) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(AUTO_PAGE_MSG3_LEFT, AUTO_PAGE_MSG3_BOT, AUTO_PAGE_MSG3_WIDTH,
						 Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.AutoPageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(AUTO_PAGE_MSG3_OP0_LEFT, AUTO_PAGE_MSG3_OP0_BOT, AUTO_PAGE_MSG3_OP0_WIDTH, 
						 StrTemp[1], RIGHT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(AUTO_PAGE_MSG3_OP1_LEFT, AUTO_PAGE_MSG3_OP1_BOT, AUTO_PAGE_MSG3_OP1_WIDTH, 
						 StrTemp[4], RIGHT_ALIGN, REDRAW_OFF);
		}
		else StepLineCheck = DISPLAY_OFF;
	 }
	 
	 
	 if (StepLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.AutoPageNo*TITLE_LINE_MAX+2) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(AUTO_PAGE_MSG4_LEFT, AUTO_PAGE_MSG4_BOT, AUTO_PAGE_MSG4_WIDTH,
						 Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.AutoPageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(AUTO_PAGE_MSG4_OP0_LEFT, AUTO_PAGE_MSG4_OP0_BOT, AUTO_PAGE_MSG4_OP0_WIDTH, 
						 StrTemp[2], RIGHT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(AUTO_PAGE_MSG4_OP1_LEFT, AUTO_PAGE_MSG4_OP1_BOT, AUTO_PAGE_MSG4_OP1_WIDTH, 
						 StrTemp[5], RIGHT_ALIGN, REDRAW_OFF);
		}
		else StepLineCheck = DISPLAY_OFF;
	 }
	 break;
	 
	 
	 //AUTO_COUNTER//AUTO_COUNTER//AUTO_COUNTER//AUTO_COUNTER//AUTO_COUNTER//AUTO_COUNTER//AUTO_COUNTER//AUTO_COUNTER//AUTO_COUNTER
  case AUTO_COUNTER:  
	 Lcd_Printf(TITLE_MSG_LEFT, TITLE_MSG_BOT, TITLE_MSG_WIDTH,
					Page_Name_List[COUNT_SETUP_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(TITLE_MOLD_LEFT, TITLE_MOLD_BOT, TITLE_MOLD_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);
	 //Msg
	 Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L0_BOT, COUNTER_MSG_WIDTH, 
					Count_Setup_Page_Msg[0], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L1_BOT, COUNTER_MSG_WIDTH,
					Count_Setup_Page_Msg[1], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L2_BOT, COUNTER_MSG_WIDTH, 
					Count_Setup_Page_Msg[2], LEFT_ALIGN, REDRAW_OFF);
	 
	 //Number
	 sprintf(str, "%5d", RobotCfg.Count.TotalCnt);
	 Lcd_Printf(COUNTER_VALUE_LEFT, TITLE_L0_BOT, COUNTER_VALUE_WIDTH,
					str, RIGHT_ALIGN, REDRAW_OFF);
	 
	 sprintf(str, "%4d", RobotCfg.Count.RejectCnt);
	 Lcd_Printf(COUNTER_VALUE_LEFT, TITLE_L1_BOT, COUNTER_VALUE_WIDTH,
					str, RIGHT_ALIGN, REDRAW_OFF);
	 
	 sprintf(str, "%4d", RobotCfg.Count.DetectFail);
	 Lcd_Printf(COUNTER_VALUE_LEFT, TITLE_L2_BOT, COUNTER_VALUE_WIDTH,
					str, RIGHT_ALIGN, REDRAW_OFF);
	 
	 break;
	 
  case AUTO_TIMER:
	 TimerSetting();
	 //Title
	 Lcd_Printf(TITLE_MSG_LEFT, TITLE_MSG_BOT, TITLE_MSG_WIDTH,
					Page_Name_List[TIMER_EDIT_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(TITLE_MOLD_LEFT, TITLE_MOLD_BOT, TITLE_MOLD_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);     
	 //T00,T01....
	 if (TimerLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*TITLE_LINE_MAX) != ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(TIMER_MSG_LEFT, TITLE_L0_BOT, TIMER_MSG_WIDTH, 
						 Timer_Page_Num_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_MSG2_LEFT, TITLE_L0_BOT, TIMER_MSG2_WIDTH, 
						 Timer_Page_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L0_BOT, TIMER_VALUE_WIDTH, StrTemp[0], RIGHT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L0_BOT, TIMER_TEMP_WIDTH, StrTemp[3], RIGHT_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L0_BOT, TIMER_TEMP_WIDTH, StrTemp[3], RIGHT_ALIGN, REDRAW_OFF);
		}
		else TimerLineCheck = DISPLAY_OFF;
	 }
	 if (TimerLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*TITLE_LINE_MAX+1) != ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(TIMER_MSG_LEFT, TITLE_L1_BOT, TIMER_MSG_WIDTH,
						 Timer_Page_Num_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_MSG2_LEFT, TITLE_L1_BOT, TIMER_MSG2_WIDTH,
						 Timer_Page_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L1_BOT, TIMER_VALUE_WIDTH, StrTemp[1], RIGHT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L1_BOT, TIMER_TEMP_WIDTH, StrTemp[4], RIGHT_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L1_BOT, TIMER_TEMP_WIDTH, StrTemp[4], RIGHT_ALIGN, REDRAW_OFF);
		}
		else TimerLineCheck = DISPLAY_OFF;
	 }
	 if (TimerLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*TITLE_LINE_MAX+2) != ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(TIMER_MSG_LEFT, TITLE_L2_BOT, TIMER_MSG_WIDTH,
						 Timer_Page_Num_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_MSG2_LEFT, TITLE_L2_BOT, TIMER_MSG2_WIDTH,
						 Timer_Page_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L2_BOT, TIMER_VALUE_WIDTH, StrTemp[2], RIGHT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 2))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L2_BOT, TIMER_TEMP_WIDTH, StrTemp[5], RIGHT_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L2_BOT, TIMER_TEMP_WIDTH, StrTemp[5], RIGHT_ALIGN, REDRAW_OFF);
		}
		else TimerLineCheck = DISPLAY_OFF;
	 }
	 break;
	 
  case AUTO_IO:     
	 Lcd_Printf(TITLE_MSG_LEFT, IO_TITLE_BOT, IO_TITLE_WIDTH,
					Page_Name_List[IO_PAGE - ControlMsg.LineNo], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(IO_TITLE_MOLD_LEFT, IO_TITLE_BOT, IO_TITLE_MOLD_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);
	 
	 //IO Signal : X11, X14, Y13.....
	 Lcd_Printf(IO_SIGNAL_LEFT, TITLE_L0_BOT, IO_SIGNAL_WIDTH,
					IO_Page_Num_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo)], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(IO_SIGNAL_LEFT, TITLE_L1_BOT, IO_SIGNAL_WIDTH,
					IO_Page_Num_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 1], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(IO_SIGNAL_LEFT, TITLE_L2_BOT, IO_SIGNAL_WIDTH,
					IO_Page_Num_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 2], LEFT_ALIGN, REDRAW_OFF);
	 
	 //IO Name(MSG)
	 Lcd_Printf(IO_MSG_LEFT, TITLE_L0_BOT, IO_MSG_WIDTH,
					IO_Page_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo)], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(IO_MSG_LEFT, TITLE_L1_BOT, IO_MSG_WIDTH,
					IO_Page_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 1], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(IO_MSG_LEFT, TITLE_L2_BOT, IO_MSG_WIDTH,
					IO_Page_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 2], LEFT_ALIGN, REDRAW_OFF);
	 break;
	 
	 
	 
	 //
  case AUTO_MODE:
	 ModeSetting();
	 
	 if (ModeLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*OUTLINE_LINE_MAX) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(MODE_NUM_LEFT, OUTLINE_LO_BOT, MODE_NUM_WIDTH, 
						 Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_MSG_LEFT, OUTLINE_LO_BOT, MODE_MSG_WIDTH, 
						 Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_LO_BOT, MODE_OPTION_WIDTH, 
						 Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX]], CENTER_ALIGN, REDRAW_OFF);
		}
		else ModeLineCheck = DISPLAY_OFF;
	 }
	 
	 
	 if (ModeLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+1) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L1_BOT, MODE_NUM_WIDTH,
						 Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L1_BOT, MODE_MSG_WIDTH,
						 Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L1_BOT, MODE_OPTION_WIDTH, 
						 Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], CENTER_ALIGN, REDRAW_OFF);
		}
		else ModeLineCheck = DISPLAY_OFF;
	 }
	 
	 if (ModeLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+2) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L2_BOT, MODE_NUM_WIDTH,
						 Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L2_BOT, MODE_MSG_WIDTH,
						 Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L2_BOT, MODE_OPTION_WIDTH, 
						 Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], CENTER_ALIGN, REDRAW_OFF);
		}
		else ModeLineCheck = DISPLAY_OFF;
	 }
	 
	 
	 if (ModeLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+3) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L3_BOT, MODE_NUM_WIDTH,
						 Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L3_BOT, MODE_MSG_WIDTH,
						 Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L3_BOT, MODE_OPTION_WIDTH, 
						 Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], CENTER_ALIGN, REDRAW_OFF);
		}
		else ModeLineCheck = DISPLAY_OFF;
	 }
	 break;
	 
  case MANUAL_MODE:
	 ModeSetting();
	 
	 if (ModeLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*OUTLINE_LINE_MAX) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(MODE_NUM_LEFT, OUTLINE_LO_BOT, MODE_NUM_WIDTH, 
						 Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_MSG_LEFT, OUTLINE_LO_BOT, MODE_MSG_WIDTH, 
						 Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_LO_BOT, MODE_OPTION_WIDTH, 
							  Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX]], CENTER_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_LO_BOT, MODE_OPTION_WIDTH, 
								Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX]], CENTER_ALIGN, REDRAW_OFF);
		}
		else ModeLineCheck = DISPLAY_OFF;
	 }
	 if (ModeLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+1) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L1_BOT, MODE_NUM_WIDTH,
						 Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L1_BOT, MODE_MSG_WIDTH,
						 Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L1_BOT, MODE_OPTION_WIDTH, 
							  Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], CENTER_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L1_BOT, MODE_OPTION_WIDTH, 
								Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], CENTER_ALIGN, REDRAW_OFF);
		}
		else ModeLineCheck = DISPLAY_OFF;
	 }
	 
	 
	 if (ModeLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+2) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L2_BOT, MODE_NUM_WIDTH,
						 Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L2_BOT, MODE_MSG_WIDTH,
						 Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 2))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L2_BOT, MODE_OPTION_WIDTH, 
							  Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], CENTER_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L2_BOT, MODE_OPTION_WIDTH, 
								Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], CENTER_ALIGN, REDRAW_OFF);
		}
		else ModeLineCheck = DISPLAY_OFF;
	 }
	 if (ModeLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+3) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L3_BOT, MODE_NUM_WIDTH,
						 Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L3_BOT, MODE_MSG_WIDTH,
						 Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], LEFT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 3))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L3_BOT, MODE_OPTION_WIDTH, 
							  Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], CENTER_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L3_BOT, MODE_OPTION_WIDTH, 
								Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], CENTER_ALIGN, REDRAW_OFF);
		}
		else ModeLineCheck = DISPLAY_OFF;
	 }
	 break;
	 
  case SETTING_MANUFACTURER:
	 //MSG
	 //      Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_WIDTH, 
	 //                 Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX)], LEFT_ALIGN, REDRAW_OFF);
	 //      Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_WIDTH,
	 //                 Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 1], LEFT_ALIGN, REDRAW_OFF);
	 //      Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_WIDTH,
	 //                 Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 2], LEFT_ALIGN, REDRAW_OFF);
	 //      		if(ControlMsg.PageNo < SETTING_MAX_PAGE)		//IMMType
	 //      Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_WIDTH,
	 //                 Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 3], LEFT_ALIGN, REDRAW_OFF);
	 //Option
	 if(ControlMsg.PageNo == 0)
	 {
		Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_WIDTH, 
					  Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX)], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_WIDTH,
					  Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 1], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_WIDTH,
					  Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 2], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_WIDTH,
					  Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 3], LEFT_ALIGN, REDRAW_OFF);
		sprintf(str, "%.1d", SettingTemp.ErrorTime);
		if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
		{
		  if (NewMoldBlink == BLINK_ON)
		  {
			 Lcd_Printf(SETTING_SEC_LEFT, OUTLINE_LO_BOT, SETTING_SEC_WIDTH,
							str, CENTER_ALIGN, REDRAW_OFF);
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);
				NewMoldBlink = BLINK_OFF;
			 } 
		  }
		  else if(NewMoldBlink == BLINK_OFF)
		  {
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);	 
				NewMoldBlink = BLINK_ON;
			 }
		  }
		}
		else Lcd_Printf(SETTING_SEC_LEFT, OUTLINE_LO_BOT, SETTING_SEC_WIDTH,
							 str, CENTER_ALIGN, REDRAW_OFF);
		Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH - 8, 
					  Factory_Setup_Option[POS_FAC_SEC], RIGHT_ALIGN, REDRAW_OFF);
		if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
		{
		  if (NewMoldBlink == BLINK_ON)
		  {
			 Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_OPT_WIDTH, 
							Factory_Setup_Option[(SettingTemp.ItlFullAuto) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);
				NewMoldBlink = BLINK_OFF;
			 } 
		  }
		  else if(NewMoldBlink == BLINK_OFF)
		  {
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);	 
				NewMoldBlink = BLINK_ON;
			 }
		  }
		}
		else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_OPT_WIDTH, 
							 Factory_Setup_Option[(SettingTemp.ItlFullAuto) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
		if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 2))
		{
		  if (NewMoldBlink == BLINK_ON)
		  {
			 Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_OPT_WIDTH, 
							Factory_Setup_Option[(SettingTemp.ItlSaftyDoor) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);
				NewMoldBlink = BLINK_OFF;
			 } 
		  }
		  else if(NewMoldBlink == BLINK_OFF)
		  {
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);	 
				NewMoldBlink = BLINK_ON;
			 }
		  }
		}
		else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_OPT_WIDTH, 
							 Factory_Setup_Option[(SettingTemp.ItlSaftyDoor) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
		if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 3))
		{
		  if (NewMoldBlink == BLINK_ON)
		  {
			 Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_OPT_WIDTH, 
							Factory_Setup_Option[(SettingTemp.ItlAutoInjection) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);
				NewMoldBlink = BLINK_OFF;
			 } 
		  }
		  else if(NewMoldBlink == BLINK_OFF)
		  {
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);	 
				NewMoldBlink = BLINK_ON;
			 }
		  }
		}
		else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_OPT_WIDTH, 
							 Factory_Setup_Option[(SettingTemp.ItlAutoInjection) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
	 }
	 else if(ControlMsg.PageNo == 1)
	 {
		Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_WIDTH, 
					  Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX)], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_WIDTH,
					  Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 1], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_WIDTH,
					  Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 2], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_WIDTH,
					  Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 3], LEFT_ALIGN, REDRAW_OFF);
		
		if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
		{
		  if (NewMoldBlink == BLINK_ON)
		  {
			 Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH, 
							Factory_Setup_Option[(SettingTemp.ItlReject) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);
				NewMoldBlink = BLINK_OFF;
			 } 
		  }
		  else if(NewMoldBlink == BLINK_OFF)
		  {
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);	 
				NewMoldBlink = BLINK_ON;
			 }
		  }
		}
		else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH, 
							 Factory_Setup_Option[(SettingTemp.ItlReject) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
		sprintf(str, "%.2d", SettingTemp.ProcessTime);
		if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
		{
		  if (NewMoldBlink == BLINK_ON)
		  {
			 Lcd_Printf(SETTING_SEC_LEFT, OUTLINE_L1_BOT, SETTING_SEC_WIDTH,
							str, CENTER_ALIGN, REDRAW_OFF);
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);
				NewMoldBlink = BLINK_OFF;
			 } 
		  }
		  else if(NewMoldBlink == BLINK_OFF)
		  {
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);	 
				NewMoldBlink = BLINK_ON;
			 }
		  }
		}
		else Lcd_Printf(SETTING_SEC_LEFT, OUTLINE_L1_BOT, SETTING_SEC_WIDTH,
							 str, CENTER_ALIGN, REDRAW_OFF);
		
		Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_OPT_WIDTH - 8, 
					  Factory_Setup_Option[POS_FAC_SEC], RIGHT_ALIGN, REDRAW_OFF);
		//DATE
		sprintf(str, "%.2d/%.2d/%.2d", SettingTemp.Time.Date.Year, SettingTemp.Time.Date.Month, SettingTemp.Time.Date.Date);
		Lcd_Printf(FACTORY_SETTING_TIME_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_TIME_WIDTH,
					  str, CENTER_ALIGN, REDRAW_OFF);
		
		sprintf(str, "__");
		if(UnderbarPos == YEAR_POS)
		  Lcd_Printf(SETTING_UNDERBAR0_LEFT, OUTLINE_L2_BOT, SETTING_UNDERBAR_WIDTH,
						 str, LEFT_ALIGN, REDRAW_OFF);
		if(UnderbarPos == MONTH_POS)
		  Lcd_Printf(SETTING_UNDERBAR1_LEFT, OUTLINE_L2_BOT, SETTING_UNDERBAR_WIDTH,
						 str, LEFT_ALIGN, REDRAW_OFF);
		if(UnderbarPos == DATE_POS)
		  Lcd_Printf(SETTING_UNDERBAR2_LEFT, OUTLINE_L2_BOT, SETTING_UNDERBAR_WIDTH,
						 str, LEFT_ALIGN, REDRAW_OFF);
		//TIME
		sprintf(str, "%.2d:%.2d:%.2d", SettingTemp.Time.Time.Hours, SettingTemp.Time.Time.Minutes, SettingTemp.Time.Time.Seconds);
		Lcd_Printf(FACTORY_SETTING_TIME_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_TIME_WIDTH,
					  str, CENTER_ALIGN, REDRAW_OFF);
		sprintf(str, "__");
		if(UnderbarPos == HOUR_POS)
		  Lcd_Printf(SETTING_UNDERBAR0_LEFT, OUTLINE_L3_BOT, SETTING_UNDERBAR_WIDTH,
						 str, LEFT_ALIGN, REDRAW_OFF);
		if(UnderbarPos == MINUTE_POS)
		  Lcd_Printf(SETTING_UNDERBAR1_LEFT, OUTLINE_L3_BOT, SETTING_UNDERBAR_WIDTH,
						 str, LEFT_ALIGN, REDRAW_OFF);
		if(UnderbarPos == SECOND_POS)
		  Lcd_Printf(SETTING_UNDERBAR2_LEFT, OUTLINE_L3_BOT, SETTING_UNDERBAR_WIDTH,
						 str, LEFT_ALIGN, REDRAW_OFF);
		if((ControlMsg.EnableEdit == ENABLE_EDIT) && (UnderbarPos == SECOND_POS))
		{
		  if (NewMoldBlink == BLINK_ON)
		  {
			 Lcd_Printf(SETTING_UNDERBAR2_LEFT, OUTLINE_L3_BOT, SETTING_UNDERBAR_WIDTH,
							str, LEFT_ALIGN, REDRAW_OFF);
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);
				NewMoldBlink = BLINK_OFF;
			 } 
		  }
		  else if(NewMoldBlink == BLINK_OFF)
		  {
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);	 
				NewMoldBlink = BLINK_ON;
			 }
		  }
		}
	 }
	 else if(ControlMsg.PageNo == 2)
	 {
		Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_WIDTH, 
					  Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX)], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_WIDTH,
					  Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 1], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_WIDTH,
					  Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 2], LEFT_ALIGN, REDRAW_OFF);
		
		if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
		{
		  if (NewMoldBlink == BLINK_ON)
		  {
			 Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH, 
							Factory_Setup_Option[(SettingTemp.DelMoldData) + POS_FAC_NO], CENTER_ALIGN, REDRAW_OFF);
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);
				NewMoldBlink = BLINK_OFF;
			 } 
		  }
		  else if(NewMoldBlink == BLINK_OFF)
		  {
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);	 
				NewMoldBlink = BLINK_ON;
			 }
		  }
		}
		else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH, 
							 Factory_Setup_Option[(SettingTemp.DelMoldData) + POS_FAC_NO], CENTER_ALIGN, REDRAW_OFF);
		if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
		{
		  if (NewMoldBlink == BLINK_ON)
		  {
			 Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_OPT_WIDTH, 
							Factory_Setup_Option[(SettingTemp.DelErrHistory) + POS_FAC_NO], CENTER_ALIGN, REDRAW_OFF);
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);
				NewMoldBlink = BLINK_OFF;
			 } 
		  }
		  else if(NewMoldBlink == BLINK_OFF)
		  {
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);	 
				NewMoldBlink = BLINK_ON;
			 }
		  }
		}
		else  Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_OPT_WIDTH, 
							  Factory_Setup_Option[(SettingTemp.DelErrHistory) + POS_FAC_NO], CENTER_ALIGN, REDRAW_OFF);
		if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 2))
		{
		  if (NewMoldBlink == BLINK_ON)
		  {
			 Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_OPT_WIDTH, 
							Factory_Setup_Option[(SettingTemp.DoorSignalChange) + POS_FAC_MSTOP], CENTER_ALIGN, REDRAW_OFF);
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);
				NewMoldBlink = BLINK_OFF;
			 } 
		  }
		  else if(NewMoldBlink == BLINK_OFF)
		  {
			 if(ChkExpireSysTick(&NewMoldTick))
			 {
				SetSysTick(&NewMoldTick,TIME_BLINK);	 
				NewMoldBlink = BLINK_ON;
			 }
		  }
		}
		else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_OPT_WIDTH, 
							 Factory_Setup_Option[(SettingTemp.DoorSignalChange) + POS_FAC_MSTOP], CENTER_ALIGN, REDRAW_OFF);
		
		//IMMType
		if((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC))
		{
		  Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_WIDTH,
						 Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 3], LEFT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 3))
		  {
			 
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_OPT_WIDTH, 
							  Factory_Setup_Option[(SettingTemp.IMMType) + POS_FAC_IMMType], CENTER_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_OPT_WIDTH, 
								Factory_Setup_Option[(SettingTemp.IMMType) + POS_FAC_IMMType], CENTER_ALIGN, REDRAW_OFF);
		}
	 }
	 //IMMType
	 else if(ControlMsg.PageNo == 3)
	 {
		if((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC))
		{
		  Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_WIDTH, 
						 Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX)], LEFT_ALIGN, REDRAW_OFF);
		  
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH, 
							  Factory_Setup_Option_Setting[(SettingTemp.RotationState) + POS_FAC_ROTATION], CENTER_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH, 
								Factory_Setup_Option_Setting[(SettingTemp.RotationState) + POS_FAC_ROTATION], CENTER_ALIGN, REDRAW_OFF);
		}
	 }
	 break;
	 
  case MANUAL_COUNTER:
	 //Title
	 Lcd_Printf(TITLE_MSG_LEFT, TITLE_MSG_BOT, TITLE_MSG_WIDTH,
					Page_Name_List[COUNT_SETUP_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(TITLE_MOLD_LEFT, TITLE_MOLD_BOT, TITLE_MOLD_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);
	 //Msg
	 Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L0_BOT, COUNTER_MSG_WIDTH, 
					Count_Setup_Page_Msg[0], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L1_BOT, COUNTER_MSG_WIDTH,
					Count_Setup_Page_Msg[1], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L2_BOT, COUNTER_MSG_WIDTH, 
					Count_Setup_Page_Msg[2], LEFT_ALIGN, REDRAW_OFF);
	 
	 //Number
	 sprintf(str, "%5d", RobotCfg.Count.TotalCnt);
	 Lcd_Printf(COUNTER_VALUE_LEFT, TITLE_L0_BOT, COUNTER_VALUE_WIDTH,
					str, RIGHT_ALIGN, REDRAW_OFF);
	 
	 sprintf(str, "%4d", RobotCfg.Count.RejectCnt);
	 Lcd_Printf(COUNTER_VALUE_LEFT, TITLE_L1_BOT, COUNTER_VALUE_WIDTH,
					str, RIGHT_ALIGN, REDRAW_OFF);
	 
	 sprintf(str, "%4d", RobotCfg.Count.DetectFail);
	 Lcd_Printf(COUNTER_VALUE_LEFT, TITLE_L2_BOT, COUNTER_VALUE_WIDTH,
					str, RIGHT_ALIGN, REDRAW_OFF);
	 break;
	 
  case MANUAL_TIMER:
	 TimerSetting();
	 //Title
	 Lcd_Printf(TITLE_MSG_LEFT, TITLE_MSG_BOT, TITLE_MSG_WIDTH,
					Page_Name_List[TIMER_EDIT_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(TITLE_MOLD_LEFT, TITLE_MOLD_BOT, TITLE_MOLD_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);
	 
	 //T00,T01....
	 if (TimerLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*TITLE_LINE_MAX) != ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(TIMER_MSG_LEFT, TITLE_L0_BOT, TIMER_MSG_WIDTH, 
						 Timer_Page_Num_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_MSG2_LEFT, TITLE_L0_BOT, TIMER_MSG2_WIDTH, 
						 Timer_Page_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L0_BOT, TIMER_VALUE_WIDTH, StrTemp[0], RIGHT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L0_BOT, TIMER_TEMP_WIDTH, StrTemp[3], RIGHT_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L0_BOT, TIMER_TEMP_WIDTH, StrTemp[3], RIGHT_ALIGN, REDRAW_OFF); 
		}
		else TimerLineCheck = DISPLAY_OFF;
	 }
	 if (TimerLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*TITLE_LINE_MAX+1) != ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(TIMER_MSG_LEFT, TITLE_L1_BOT, TIMER_MSG_WIDTH,
						 Timer_Page_Num_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_MSG2_LEFT, TITLE_L1_BOT, TIMER_MSG2_WIDTH,
						 Timer_Page_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L1_BOT, TIMER_VALUE_WIDTH, StrTemp[1], RIGHT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L1_BOT, TIMER_TEMP_WIDTH, StrTemp[4], RIGHT_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L1_BOT, TIMER_TEMP_WIDTH, StrTemp[4], RIGHT_ALIGN, REDRAW_OFF);
		}
		else TimerLineCheck = DISPLAY_OFF;
	 }
	 if (TimerLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*TITLE_LINE_MAX+2) != ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(TIMER_MSG_LEFT, TITLE_L2_BOT, TIMER_MSG_WIDTH,
						 Timer_Page_Num_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_MSG2_LEFT, TITLE_L2_BOT, TIMER_MSG2_WIDTH,
						 Timer_Page_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L2_BOT, TIMER_VALUE_WIDTH, StrTemp[2], RIGHT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 2))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L2_BOT, TIMER_TEMP_WIDTH, StrTemp[5], RIGHT_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L2_BOT, TIMER_TEMP_WIDTH, StrTemp[5], RIGHT_ALIGN, REDRAW_OFF);
		}
		else TimerLineCheck = DISPLAY_OFF;
	 }
	 break;
	 
  case MANUAL_IO:
	 //Title
	 Lcd_Printf(TITLE_MSG_LEFT, IO_TITLE_BOT, IO_TITLE_WIDTH,
					Page_Name_List[IO_PAGE - ControlMsg.LineNo], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(IO_TITLE_MOLD_LEFT, IO_TITLE_BOT, IO_TITLE_MOLD_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);
	 //IO Signal : X11, X14, Y13.....
	 Lcd_Printf(IO_SIGNAL_LEFT, TITLE_L0_BOT, IO_SIGNAL_WIDTH,
					IO_Page_Num_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo)], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(IO_SIGNAL_LEFT, TITLE_L1_BOT, IO_SIGNAL_WIDTH,
					IO_Page_Num_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 1], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(IO_SIGNAL_LEFT, TITLE_L2_BOT, IO_SIGNAL_WIDTH,
					IO_Page_Num_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 2], LEFT_ALIGN, REDRAW_OFF);
	 
	 //IO Name(MSG)
	 Lcd_Printf(IO_MSG_LEFT, TITLE_L0_BOT, IO_MSG_WIDTH,
					IO_Page_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo)], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(IO_MSG_LEFT, TITLE_L1_BOT, IO_MSG_WIDTH,
					IO_Page_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 1], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(IO_MSG_LEFT, TITLE_L2_BOT, IO_MSG_WIDTH,
					IO_Page_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 2], LEFT_ALIGN, REDRAW_OFF);
	 break;
	 
  case MANUAL_STEP_OPERATING:
	 StepTimerSetting();
	 
	 //Title
	 Lcd_Printf(STEP_PAGE_MSG0_LEFT, STEP_PAGE_MSG0_BOT, STEP_PAGE_MSG0_WIDTH,
					Page_Name_List[FILE_STEP_RUN_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(STEP_PAGE_MSG1_LEFT, STEP_PAGE_MSG1_BOT, STEP_PAGE_MSG1_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);
	 
	 //Step Name
	 if (StepLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*TITLE_LINE_MAX) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
		  Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L0_BOT, COUNTER_MSG_WIDTH, 
						 Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		else StepLineCheck = DISPLAY_OFF;
	 }
	 if (StepLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*TITLE_LINE_MAX+1) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
		  Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L1_BOT, COUNTER_MSG_WIDTH,
						 Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		else StepLineCheck = DISPLAY_OFF;
	 }
	 if (StepLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*TITLE_LINE_MAX+2) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
		  Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L2_BOT, COUNTER_MSG_WIDTH,
						 Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		else StepLineCheck = DISPLAY_OFF;
	 }
	 break;
	 
  case MANUAL_CYCLE_OPERATING:
	 StepTimerSetting();
	 //Title
	 Lcd_Printf(AUTO_PAGE_MSG0_LEFT, AUTO_PAGE_MSG0_BOT, AUTO_PAGE_MSG0_WIDTH,
					Page_Name_List[FILE_CYCLE_RUN_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(STEP_PAGE_MSG1_LEFT, STEP_PAGE_MSG1_BOT, STEP_PAGE_MSG1_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);
	 //Name
	 if (StepLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.AutoPageNo*TITLE_LINE_MAX) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L0_BOT, COUNTER_MSG_WIDTH, 
						 Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.AutoPageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L0_BOT, TIMER_VALUE_WIDTH, StrTemp[0], RIGHT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L0_BOT, TIMER_TEMP_WIDTH, StrTemp[3], RIGHT_ALIGN, REDRAW_OFF);
		}
		else StepLineCheck = DISPLAY_OFF;
	 }
	 if (StepLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.AutoPageNo*TITLE_LINE_MAX+1) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L1_BOT, COUNTER_MSG_WIDTH,
						 Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.AutoPageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L1_BOT, TIMER_VALUE_WIDTH, StrTemp[1], RIGHT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L1_BOT, TIMER_TEMP_WIDTH, StrTemp[4], RIGHT_ALIGN, REDRAW_OFF);
		}
		else StepLineCheck = DISPLAY_OFF;
	 }
	 if (StepLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.AutoPageNo*TITLE_LINE_MAX+2) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L2_BOT, COUNTER_MSG_WIDTH,
						 Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.AutoPageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L2_BOT, TIMER_VALUE_WIDTH, StrTemp[2], RIGHT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L2_BOT, TIMER_TEMP_WIDTH, StrTemp[5], RIGHT_ALIGN, REDRAW_OFF);
		}
		else StepLineCheck = DISPLAY_OFF;
	 }
	 break;
	 
  case MANUAL_MOLD_SEARCH:
	 Lcd_Printf(FILE_SEARCH_PAGE_MSG0_LEFT, FILE_SEARCH_PAGE_MSG0_BOT, FILE_SEARCH_PAGE_MSG0_WIDTH,
					Page_Name_List[FILE_SEARCH_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(FILE_SEARCH_PAGE_MSG1_LEFT, FILE_SEARCH_PAGE_MSG1_BOT, FILE_SEARCH_PAGE_MSG1_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);
	 
	 Lcd_Printf(FILE_SEARCH_PAGE_MSG2_LEFT, FILE_SEARCH_PAGE_MSG2_BOT, FILE_SEARCH_PAGE_MSG2_WIDTH,
					File_Search_Msg[(ControlMsg.PageNo*TITLE_LINE_MAX)], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(FILE_SEARCH_PAGE_MSG3_LEFT, FILE_SEARCH_PAGE_MSG3_BOT, FILE_SEARCH_PAGE_MSG3_WIDTH,
					File_Search_Msg[(ControlMsg.PageNo*TITLE_LINE_MAX)+1], LEFT_ALIGN, REDRAW_OFF);
	 
	 sprintf(strMoldNoTemp, "%.3d", MoldNoTemp);
	 Lcd_Printf(FILE_SEARCH_PAGE_MSG4_LEFT, FILE_SEARCH_PAGE_MSG4_BOT, FILE_SEARCH_PAGE_MSG4_WIDTH,
					strMoldNoTemp, CENTER_ALIGN, REDRAW_OFF);
	 break;
	 
  case MANUAL_MOLD_MANAGE:
	 Lcd_Printf(FILE_MANAGE_PAGE_MSG0_LEFT, FILE_MANAGE_PAGE_MSG0_BOT, FILE_MANAGE_PAGE_MSG0_WIDTH,
					Page_Name_List[FILE_MANAGEMENT_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(FILE_MANAGE_PAGE_MSG1_LEFT, FILE_MANAGE_PAGE_MSG1_BOT, FILE_MANAGE_PAGE_MSG1_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);
	 //MoldNO : 000 Mold Name : NEW_MOLD
	 MoldMsg.ExistMoldName[0][0] = Char_Input_Data[13];
	 MoldMsg.ExistMoldName[0][1] = Char_Input_Data[4];
	 MoldMsg.ExistMoldName[0][2] = Char_Input_Data[22];
	 MoldMsg.ExistMoldName[0][3] = Char_Input_Data[38];
	 MoldMsg.ExistMoldName[0][4] = Char_Input_Data[12];
	 MoldMsg.ExistMoldName[0][5] = Char_Input_Data[14];
	 MoldMsg.ExistMoldName[0][6] = Char_Input_Data[11];
	 MoldMsg.ExistMoldName[0][7] = Char_Input_Data[3];
	 
	 if (MoldLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*TITLE_LINE_MAX) != MoldMsg.MoldCount)
		{
		  for(uint8_t MoldNameChar = 0; MoldNameChar <= MAX_MOLDNAME_LENGTH; MoldNameChar++)
		  {
			 MoldNameStr0[MoldNameChar] = MoldMsg.ExistMoldName[ControlMsg.PageNo*TITLE_LINE_MAX][MoldNameChar];
		  }
		  sprintf(StrTemp[0], "%.3d", MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX]);
		  Lcd_Printf(FILE_MANAGE_PAGE_MSG2_LEFT, FILE_MANAGE_PAGE_MSG2_BOT, FILE_MANAGE_PAGE_MSG2_WIDTH,
						 StrTemp[0], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(FILE_MANAGE_PAGE_MSG2_OP_LEFT, FILE_MANAGE_PAGE_MSG2_OP_BOT, FILE_MANAGE_PAGE_MSG2_OP_WIDTH,
						 MoldNameStr0, LEFT_ALIGN, REDRAW_OFF);
		}
		else MoldLineCheck = DISPLAY_OFF;
	 }
	 if (MoldLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*TITLE_LINE_MAX+1) != MoldMsg.MoldCount)
		{
		  for(uint8_t MoldNameChar = 0; MoldNameChar <= MAX_MOLDNAME_LENGTH; MoldNameChar++)
		  {
			 MoldNameStr1[MoldNameChar] = MoldMsg.ExistMoldName[ControlMsg.PageNo*TITLE_LINE_MAX+1][MoldNameChar];
		  }
		  sprintf(StrTemp[1], "%.3d", MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+1]);
		  Lcd_Printf(FILE_MANAGE_PAGE_MSG3_LEFT, FILE_MANAGE_PAGE_MSG3_BOT, FILE_MANAGE_PAGE_MSG3_WIDTH,
						 StrTemp[1], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(FILE_MANAGE_PAGE_MSG3_OP_LEFT, FILE_MANAGE_PAGE_MSG3_OP_BOT, FILE_MANAGE_PAGE_MSG3_OP_WIDTH,
						 MoldNameStr1, LEFT_ALIGN, REDRAW_OFF);
		}
		else MoldLineCheck = DISPLAY_OFF;
	 }
	 if (MoldLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*TITLE_LINE_MAX+2) != MoldMsg.MoldCount)
		{
		  for(uint8_t MoldNameChar = 0; MoldNameChar <= MAX_MOLDNAME_LENGTH; MoldNameChar++)
		  {
			 MoldNameStr2[MoldNameChar] = MoldMsg.ExistMoldName[ControlMsg.PageNo*TITLE_LINE_MAX+2][MoldNameChar];
		  }
		  sprintf(StrTemp[2], "%.3d", MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+2]);
		  Lcd_Printf(FILE_MANAGE_PAGE_MSG4_LEFT, FILE_MANAGE_PAGE_MSG4_BOT, FILE_MANAGE_PAGE_MSG4_WIDTH,
						 StrTemp[2], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(FILE_MANAGE_PAGE_MSG4_OP_LEFT, FILE_MANAGE_PAGE_MSG4_OP_BOT, FILE_MANAGE_PAGE_MSG4_OP_WIDTH,
						 MoldNameStr2, LEFT_ALIGN, REDRAW_OFF);
		}
		else MoldLineCheck = DISPLAY_OFF;
	 }
	 break;
	 
  case MANUAL_MOLD_MODE:
	 ModeSetting();
	 
	 if (ModeLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*OUTLINE_LINE_MAX) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(MODE_NUM_LEFT, OUTLINE_LO_BOT, MODE_NUM_WIDTH, 
						 Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_MSG_LEFT, OUTLINE_LO_BOT, MODE_MSG_WIDTH, 
						 Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_LO_BOT, MODE_OPTION_WIDTH, 
							  Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX]], CENTER_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_LO_BOT, MODE_OPTION_WIDTH, 
								Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX]], CENTER_ALIGN, REDRAW_OFF);
		}
		else ModeLineCheck = DISPLAY_OFF;
	 }
	 if (ModeLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+1) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L1_BOT, MODE_NUM_WIDTH,
						 Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L1_BOT, MODE_MSG_WIDTH,
						 Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L1_BOT, MODE_OPTION_WIDTH, 
							  Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], CENTER_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L1_BOT, MODE_OPTION_WIDTH, 
								Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], CENTER_ALIGN, REDRAW_OFF);
		}
		else ModeLineCheck = DISPLAY_OFF;
	 }
	 if (ModeLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+2) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L2_BOT, MODE_NUM_WIDTH,
						 Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L2_BOT, MODE_MSG_WIDTH,
						 Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 2))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L2_BOT, MODE_OPTION_WIDTH, 
							  Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], CENTER_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L2_BOT, MODE_OPTION_WIDTH, 
								Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], CENTER_ALIGN, REDRAW_OFF);
		}
		else ModeLineCheck = DISPLAY_OFF;
	 }
	 if (ModeLineCheck == DISPLAY_ON)
	 {
		if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+3) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
		{
		  Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L3_BOT, MODE_NUM_WIDTH,
						 Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L3_BOT, MODE_MSG_WIDTH,
						 Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], LEFT_ALIGN, REDRAW_OFF);
		  if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 3))
		  {
			 if (NewMoldBlink == BLINK_ON)
			 {
				Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L3_BOT, MODE_OPTION_WIDTH, 
							  Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], CENTER_ALIGN, REDRAW_OFF);
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);
				  NewMoldBlink = BLINK_OFF;
				} 
			 }
			 else if(NewMoldBlink == BLINK_OFF)
			 {
				if(ChkExpireSysTick(&NewMoldTick))
				{
				  SetSysTick(&NewMoldTick,TIME_BLINK);	 
				  NewMoldBlink = BLINK_ON;
				}
			 }
		  }
		  else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L3_BOT, MODE_OPTION_WIDTH, 
								Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], CENTER_ALIGN, REDRAW_OFF);
		}
		else ModeLineCheck = DISPLAY_OFF;
	 }
	 break;
	 
  case MANUAL_MOLD_NEW:
	 Lcd_Printf(NEW_PAGE_MSG0_LEFT, NEW_PAGE_MSG0_BOT, NEW_PAGE_MSG0_WIDTH,
					Page_Name_List[FILE_NEW_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(NEW_PAGE_MSG1_LEFT, NEW_PAGE_MSG1_BOT, NEW_PAGE_MSG1_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);
	 
	 //MoldNo
	 sprintf(strMoldNoTemp, "%.3d", MoldNoTemp);
	 
	 if (UnderbarPos == MOLD_NO)
	 {
		if (NewMoldBlink == BLINK_ON)
		{
		  Lcd_Printf(NEW_PAGE_MSG2_LEFT, NEW_PAGE_MSG2_BOT, NEW_PAGE_MSG2_WIDTH,
						 strMoldNoTemp, CENTER_ALIGN, REDRAW_OFF);
		  if(ChkExpireSysTick(&NewMoldTick))
		  {
			 SetSysTick(&NewMoldTick,TIME_BLINK);
			 NewMoldBlink = BLINK_OFF;
		  } 
		}
		else if(NewMoldBlink == BLINK_OFF)
		{
		  if(ChkExpireSysTick(&NewMoldTick))
		  {
			 SetSysTick(&NewMoldTick,TIME_BLINK);	 
			 NewMoldBlink = BLINK_ON;
		  }
		}
	 }
	 else if(UnderbarPos > MOLD_NO)
		Lcd_Printf(NEW_PAGE_MSG2_LEFT, NEW_PAGE_MSG2_BOT, NEW_PAGE_MSG2_WIDTH,
					  strMoldNoTemp, CENTER_ALIGN, REDRAW_OFF);
	 
	 //MoldNames
	 NewMoldName[0] = Char_Input_Data[NewMoldNameTemp[0]];
	 NewMoldName[1] = Char_Input_Data[NewMoldNameTemp[1]];
	 NewMoldName[2] = Char_Input_Data[NewMoldNameTemp[2]];
	 NewMoldName[3] = Char_Input_Data[NewMoldNameTemp[3]];
	 NewMoldName[4] = Char_Input_Data[NewMoldNameTemp[4]];
	 NewMoldName[5] = Char_Input_Data[NewMoldNameTemp[5]];
	 NewMoldName[6] = Char_Input_Data[NewMoldNameTemp[6]];
	 NewMoldName[7] = Char_Input_Data[NewMoldNameTemp[7]];
	 NewMoldName[MoldNamePos] = Char_Input_Data[MoldNameCharPos];
	 
	 Lcd_Printf(NEW_PAGE_MSG3_LEFT, NEW_PAGE_MSG3_BOT, NEW_PAGE_MSG3_WIDTH,
					NewMoldName, LEFT_ALIGN, REDRAW_OFF);
	 
	 //MoldNameUnderBar
	 sprintf(str, "_");
	 if (UnderbarPos == MOLD_NAME)
	 {
		if (NewMoldBlink == BLINK_ON)
		{
		  Lcd_Printf(NEW_PAGE_MSG3_LEFT+(MAX_UNDERBAR_POS*MoldNamePos), OUTLINE_L2_BOT, SETTING_UNDERBAR_WIDTH,
						 str, LEFT_ALIGN, REDRAW_OFF);
		  if(ChkExpireSysTick(&NewMoldTick))
		  {
			 SetSysTick(&NewMoldTick,TIME_BLINK);	 
			 NewMoldBlink = BLINK_OFF;
		  }
		}
		else if(NewMoldBlink == BLINK_OFF)
		{
		  if(ChkExpireSysTick(&NewMoldTick))
		  {
			 SetSysTick(&NewMoldTick,TIME_BLINK);	 
			 NewMoldBlink = BLINK_ON;
		  }
		}
	 }
	 break;
	 
  case MANUAL_MOLD_DELETE:
	 Lcd_Printf(DELETE_PAGE_MSG0_LEFT, DELETE_PAGE_MSG0_BOT, DELETE_PAGE_MSG0_WIDTH,
					Page_Name_List[FILE_DEL_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.MoldNo);
	 Lcd_Printf(DELETE_PAGE_MSG1_LEFT, DELETE_PAGE_MSG1_BOT, DELETE_PAGE_MSG1_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);
	 
	 for(uint8_t MoldNameChar = 0; MoldNameChar <= MAX_MOLDNAME_LENGTH; MoldNameChar++)
	 {
		MoldNameStr0[MoldNameChar] = MoldMsg.ExistMoldName[ControlMsg.PageNo*TITLE_LINE_MAX + ControlMsg.LineNo][MoldNameChar];
	 }
	 sprintf(StrTemp[0], "%.3d", MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+ControlMsg.LineNo]);
	 
	 
	 Lcd_Printf(DELETE_PAGE_MSG2_LEFT, DELETE_PAGE_MSG2_BOT, DELETE_PAGE_MSG2_WIDTH,
					StrTemp[0], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(DELETE_PAGE_MSG3_LEFT, DELETE_PAGE_MSG3_BOT, DELETE_PAGE_MSG3_WIDTH,
					MoldNameStr0, LEFT_ALIGN, REDRAW_OFF);
	 
	 Lcd_Printf(DELETE_PAGE_MSG4_LEFT, DELETE_PAGE_MSG4_BOT, DELETE_PAGE_MSG4_WIDTH,
					Delete_Page_Msg[0], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(DELETE_PAGE_MSG5_LEFT, DELETE_PAGE_MSG5_BOT, DELETE_PAGE_MSG5_WIDTH,
					Delete_Page_Msg[1], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(DELETE_PAGE_MSG7_LEFT, DELETE_PAGE_MSG7_BOT, DELETE_PAGE_MSG7_WIDTH,
					Delete_Page_Msg[2], LEFT_ALIGN, REDRAW_OFF);
	 break;
	 
  case MANUAL_MOLD_DELETE_ING:
	 Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH,
					Delete_Page_Msg[3], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH,
					Delete_Page_Msg[4], LEFT_ALIGN, REDRAW_OFF);
	 break;
	 
	 
  case MANUAL_ERROR_LIST:
	 if(ErrMsg.ErrorCount != 0)
		ErrPage = ControlMsg.PageNo+1;
	 sprintf(Error0, "%d/%d", ErrPage, ErrMsg.ErrorCount);
	 sprintf(Error1, "%.2d/%.2d/%.2d", ErrMsgTemp[ControlMsg.PageNo].Time.Date.Year, 
				ErrMsgTemp[ControlMsg.PageNo].Time.Date.Month, ErrMsgTemp[ControlMsg.PageNo].Time.Date.Date);
	 sprintf(Error2, "%.2d:%.2d:%.2d", ErrMsgTemp[ControlMsg.PageNo].Time.Time.Hours, ErrMsgTemp[ControlMsg.PageNo].Time.Time.Minutes, ErrMsgTemp[ControlMsg.PageNo].Time.Time.Seconds);
	 sprintf(Error3, "%.3d", ErrMsgTemp[ControlMsg.PageNo].ErrorCode);
	 
	 
	 Lcd_Printf(ERROR_LIST_PAGE_MSG0_LEFT, ERROR_LIST_PAGE_MSG0_BOT, ERROR_LIST_PAGE_MSG0_WIDTH,
					Page_Name_List[FILE_ERROR_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 Lcd_Printf(ERROR_LIST_PAGE_MSG1_LEFT, ERROR_LIST_PAGE_MSG1_BOT, ERROR_LIST_PAGE_MSG1_WIDTH,
					Error0, LEFT_ALIGN, REDRAW_OFF);//에러 넘버
	 //		Lcd_Printf(ERROR_LIST_PAGE_MSG2_1_LEFT, ERROR_LIST_PAGE_MSG2_1_BOT, ERROR_LIST_PAGE_MSG2_1_WIDTH,
	 //					  Error_Pop_Page_Msg[1], LEFT_ALIGN, REDRAW_OFF);//날짜 문구
	 Lcd_Printf(ERROR_LIST_PAGE_MSG2_LEFT, ERROR_LIST_PAGE_MSG2_BOT, ERROR_LIST_PAGE_MSG2_WIDTH,
					Error1, RIGHT_ALIGN, REDRAW_OFF);//에러 발생 날짜
	 //		Lcd_Printf(ERROR_LIST_PAGE_MSG3_1_LEFT, ERROR_LIST_PAGE_MSG3_1_BOT, ERROR_LIST_PAGE_MSG3_1_WIDTH,
	 //					  Error_Pop_Page_Msg[2], LEFT_ALIGN, REDRAW_OFF);//시간 문구
	 Lcd_Printf(ERROR_LIST_PAGE_MSG3_LEFT, ERROR_LIST_PAGE_MSG3_BOT, ERROR_LIST_PAGE_MSG3_WIDTH,
					Error2, RIGHT_ALIGN, REDRAW_OFF);//에러 발생 시간
	 Lcd_Printf(ERROR_LIST_PAGE_MSG4_LEFT, ERROR_LIST_PAGE_MSG4_BOT, ERROR_LIST_PAGE_MSG4_WIDTH,
					Error3, LEFT_ALIGN, REDRAW_OFF);//에러 코드 번호
	 
	 //에러 내역
	 if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_EMG)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[8], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_IMM_EMG)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[9], LEFT_ALIGN, REDRAW_OFF);
	 
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SWING_DUPLICATE)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[10], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SUB_UP)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[11], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_MAIN_UP)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[12], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SUB_UP_AIR)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[13], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_MAIN_UP_AIR)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[14], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SWING)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[15], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SWING_RETURN)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[16], LEFT_ALIGN, REDRAW_OFF);
	 
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_VACCUM)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[17], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_CHUCK)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[18], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_RUNNER_PICK)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[19], LEFT_ALIGN, REDRAW_OFF);
	 
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_MO_SENSOR)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[20], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_MO_SENS_MISS)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[21], LEFT_ALIGN, REDRAW_OFF);
	 
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_MO_DOWN)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[22], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_PROCESS_TIME)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[23], LEFT_ALIGN, REDRAW_OFF);
	 
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SD_DISABLE)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[33], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SD_NOT_FILE)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[34], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SD_LOCKING)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[35], LEFT_ALIGN, REDRAW_OFF);
	 else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SD_EMPTY)
		Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
					  Error_Msg[36], LEFT_ALIGN, REDRAW_OFF);
	 break;
	 
  case MANUAL_VERSION:
	 Lcd_Printf(VERSION_PAGE_MSG0_LEFT, VERSION_PAGE_MSG0_BOT, VERSION_PAGE_MSG0_WIDTH,
					Page_Name_List[VERSION_PAGE], CENTER_ALIGN, REDRAW_OFF);
	 Lcd_Printf(VERSION_PAGE_MSG1_LEFT, VERSION_PAGE_MSG1_BOT, VERSION_PAGE_MSG1_WIDTH,
					Machine_Type_Name[4], LEFT_ALIGN, REDRAW_OFF);
	 Lcd_Printf(VERSION_PAGE_MSG2_LEFT, VERSION_PAGE_MSG2_BOT, VERSION_PAGE_MSG2_WIDTH,
					Machine_Type_Name[5], LEFT_ALIGN, REDRAW_OFF);
	 if(RobotCfg.RobotType == TYPE_A)
		Lcd_Printf(VERSION_PAGE_MSG3_LEFT, VERSION_PAGE_MSG3_BOT, VERSION_PAGE_MSG3_WIDTH,
					  Machine_Type_Name[0], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.RobotType == TYPE_X)
		Lcd_Printf(VERSION_PAGE_MSG3_LEFT, VERSION_PAGE_MSG3_BOT, VERSION_PAGE_MSG3_WIDTH,
					  Machine_Type_Name[1], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.RobotType == TYPE_XC)
		Lcd_Printf(VERSION_PAGE_MSG3_LEFT, VERSION_PAGE_MSG3_BOT, VERSION_PAGE_MSG3_WIDTH,
					  Machine_Type_Name[2], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.RobotType == TYPE_TWIN)
		Lcd_Printf(VERSION_PAGE_MSG3_LEFT, VERSION_PAGE_MSG3_BOT, VERSION_PAGE_MSG3_WIDTH,
					  Machine_Type_Name[3], LEFT_ALIGN, REDRAW_OFF);
	 break;
	 
  case OCCUR_ERROR: 
	 Lcd_Printf(ERROR_POPUP_PAGE_MSG0_LEFT, ERROR_POPUP_PAGE_MSG0_BOT, ERROR_POPUP_PAGE_MSG0_WIDTH,
					Error_Pop_Page_Msg[0], CENTER_ALIGN, REDRAW_OFF);
	 sprintf(str, "%.3d", RobotCfg.ErrCode);
	 Lcd_Printf(ERROR_POPUP_PAGE_MSG1_LEFT, ERROR_POPUP_PAGE_MSG1_BOT, ERROR_POPUP_PAGE_MSG1_WIDTH,
					str, CENTER_ALIGN, REDRAW_OFF);
	 
	 
	 if(RobotCfg.ErrCode == ERRCODE_EMG)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[8], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_IMM_EMG)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[9], LEFT_ALIGN, REDRAW_OFF);
	 
	 else if(RobotCfg.ErrCode == ERRCODE_SWING_DUPLICATE)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[10], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_SUB_UP)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[11], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_MAIN_UP)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[12], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_SUB_UP_AIR)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[13], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_MAIN_UP_AIR)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[14], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_SWING)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[15], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_SWING_RETURN)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[16], LEFT_ALIGN, REDRAW_OFF);
	 
	 else if(RobotCfg.ErrCode == ERRCODE_VACCUM)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[17], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_CHUCK)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[18], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_RUNNER_PICK)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[19], LEFT_ALIGN, REDRAW_OFF);
	 
	 else if(RobotCfg.ErrCode == ERRCODE_MO_SENSOR)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[20], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_MO_SENS_MISS)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[21], LEFT_ALIGN, REDRAW_OFF);
	 
	 else if(RobotCfg.ErrCode == ERRCODE_MO_DOWN)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[22], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_PROCESS_TIME)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[23], LEFT_ALIGN, REDRAW_OFF);
	 
	 else if(RobotCfg.ErrCode == ERRCODE_SD_DISABLE)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[33], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_SD_NOT_FILE)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[34], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_SD_LOCKING)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[35], LEFT_ALIGN, REDRAW_OFF);
	 else if(RobotCfg.ErrCode == ERRCODE_SD_EMPTY)
		Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
					  Error_Msg[36], LEFT_ALIGN, REDRAW_OFF);
	 
	 
	 
	 if(RobotCfg.ErrCode == ERRCODE_EMG)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[2], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[3], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == ERRCODE_IMM_EMG)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[4], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[5], LEFT_ALIGN, REDRAW_OFF);
	 }
	 
	 else if(RobotCfg.ErrCode == ERRCODE_SWING_DUPLICATE)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[6], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[7], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == ERRCODE_SUB_UP)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[8], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[9], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == ERRCODE_MAIN_UP)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[10], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[11], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if((RobotCfg.ErrCode == ERRCODE_SUB_UP_AIR) || (RobotCfg.ErrCode == ERRCODE_MAIN_UP_AIR))
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[12], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[13], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == ERRCODE_SWING)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[14], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[15], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == ERRCODE_SWING_RETURN)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[16], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[17], LEFT_ALIGN, REDRAW_OFF);
	 }
	 
	 else if((RobotCfg.ErrCode == ERRCODE_VACCUM) || (RobotCfg.ErrCode == ERRCODE_CHUCK) || (RobotCfg.ErrCode == ERRCODE_RUNNER_PICK))
	 {
		if(RobotCfg.Setting.ItlSaftyDoor == USE)
		{
		  Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
						 Error_Action_Msg[18], LEFT_ALIGN, REDRAW_OFF);
		  Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
						 Error_Action_Msg[19], LEFT_ALIGN, REDRAW_OFF);
		}
		else if(RobotCfg.Setting.ItlSaftyDoor == NO_USE)
		{
		  if(RobotCfg.ErrCode == ERRCODE_VACCUM)
		  {
			 Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
							Error_Action_Msg[20], LEFT_ALIGN, REDRAW_OFF);
			 Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
							Error_Action_Msg[21], LEFT_ALIGN, REDRAW_OFF);
		  }
		  if(RobotCfg.ErrCode == ERRCODE_CHUCK)
		  {
			 Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
							Error_Action_Msg[22], LEFT_ALIGN, REDRAW_OFF);
			 Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
							Error_Action_Msg[23], LEFT_ALIGN, REDRAW_OFF);
		  }
		  if(RobotCfg.ErrCode == ERRCODE_RUNNER_PICK)
		  {
			 Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
							Error_Action_Msg[24], LEFT_ALIGN, REDRAW_OFF);
			 Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
							Error_Action_Msg[25], LEFT_ALIGN, REDRAW_OFF);
		  }
		}
	 }
	 else if(RobotCfg.ErrCode == ERRCODE_MO_SENSOR)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[26], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[27], LEFT_ALIGN, REDRAW_OFF);
	 }
	 if(RobotCfg.ErrCode == ERRCODE_MO_SENS_MISS)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[0], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[1], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == ERRCODE_MO_DOWN)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[28], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == ERRCODE_PROCESS_TIME)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[29], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[30], LEFT_ALIGN, REDRAW_OFF);
	 }
	 
	 else if(RobotCfg.ErrCode == ERRCODE_SD_DISABLE)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[0], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[1], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == ERRCODE_SD_NOT_FILE)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[32], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == ERRCODE_SD_LOCKING)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[33], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
					  Error_Action_Msg[34], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == ERRCODE_SD_EMPTY)
	 {
		Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
					  Error_Action_Msg[35], LEFT_ALIGN, REDRAW_OFF);
	 }
	 
	 break;
	 
  case MANUAL_ERROR:
	 //MANUAL_OPERATING
	 if(RobotCfg.ErrCode == MANUAL_ERR_CHYCK_RO)
	 {
		Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
					  Massage_Pop_Msg[8], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
					  Massage_Pop_Msg[9], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
					  Massage_Pop_Msg[10], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == MANUAL_ERR_CHYCK_ROR)
	 {
		Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
					  Massage_Pop_Msg[11], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
					  Massage_Pop_Msg[12], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
					  Massage_Pop_Msg[13], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == MANUAL_ERR_MOLD_DELETE)
	 {
		Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
					  Massage_Pop_Msg[32], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
					  Massage_Pop_Msg[33], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == MANUAL_ERR_ROBOT_TYPE)
	 {
		Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
					  Massage_Pop_Msg[5], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
					  Massage_Pop_Msg[6], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
					  Massage_Pop_Msg[7], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == MANUAL_ERR_NO_MOLDNO)
	 {
		Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
					  Massage_Pop_Msg[1], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
					  Massage_Pop_Msg[2], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
					  Massage_Pop_Msg[3], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG3_LEFT, MASSAGE_MSG3_BOT, MASSAGE_MSG3_WIDTH, 
					  Massage_Pop_Msg[4], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == MANUAL_ERR_MOLD)
	 {
		Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
					  Massage_Pop_Msg[14], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
					  Massage_Pop_Msg[15], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
					  Massage_Pop_Msg[16], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == MANUAL_ERR_SAFTYDOOR_STEP)
	 {
		Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
					  Massage_Pop_Msg[25], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
					  Massage_Pop_Msg[26], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
					  Massage_Pop_Msg[27], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == MANUAL_ERR_SAFTYDOOR_CYCLE)
	 {
		Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
					  Massage_Pop_Msg[25], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
					  Massage_Pop_Msg[26], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
					  Massage_Pop_Msg[28], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == MANUAL_ERR_SAFTYDOOR_AUTO)
	 {
		Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
					  Massage_Pop_Msg[25], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
					  Massage_Pop_Msg[26], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
					  Massage_Pop_Msg[29], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == MANUAL_ERR_SAFTYDOOR_CHANGE)
	 {
		Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
					  Massage_Pop_Msg[30], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
					  Massage_Pop_Msg[31], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == MANUAL_ERR_REBOOTING)
	 {
		Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
					  Massage_Pop_Msg[34], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
					  Massage_Pop_Msg[35], LEFT_ALIGN, REDRAW_OFF);
	 }
	 else if(RobotCfg.ErrCode == MANUAL_ERR_SAFTYDOOR_CLOSE)
	 {
		Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
					  Massage_Pop_Msg[17], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
					  Massage_Pop_Msg[18], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
					  Massage_Pop_Msg[19], LEFT_ALIGN, REDRAW_OFF);
		Lcd_Printf(MASSAGE_MSG3_LEFT, MASSAGE_MSG3_BOT, MASSAGE_MSG3_WIDTH, 
					  Massage_Pop_Msg[20], LEFT_ALIGN, REDRAW_OFF);
	 }
	 break;
	 
  default:
	 break;
  }