/* 
 * File:   AppError.h
 * Author: Jehun
 *
* Created on 2021??02??26??(Fri), AM 10:06
*/


#ifndef __APPERR_H
#define	__APPERR_H

#ifdef	__cplusplus
extern "C" {
#endif
#include "main.h"    
#include "interface.h"
#include "AppMain.h"
#include "AppKey.h"
#include "SDCard.h"

/* Private typedef -----------------------------------------------------------*/

  
extern CONTROL_MSG ControlMsg;
extern ROBOT_CONFIG RobotCfg;
extern IO_MSG IOMsg;
extern SEQ_MSG SeqMsg;

//SEQ_MSG SeqMsg;
extern MOLD_MSG MoldMsg;
extern COUNT_MSG CountMsg;
extern ERROR_MSG ErrMsg;

extern uint64_t SDTempTick;
extern uint8_t fPreviousPage;

//ErrLoopState = ERR_CHECK;

extern void ClearPage(void);
extern void ClearAuto(void);
extern void TimerDispCheck(void);
extern SDRES WriteRobotCfg(ROBOT_CONFIG* pRobotCfg);
void LoopAppError(void);
void WriteErrorList(void);




#ifdef	__cplusplus
}
#endif

#endif	/* __APPTERR_H */