/* Includes -------------------------------------------------------------------*/
#include "DrawIcon.h"
#include "Icon.h"
#include <string.h>


extern uint8_t StepCursor;
extern uint8_t Display_Scr[1024];
extern ROBOT_CONFIG RobotCfg;
extern CONTROL_MSG ControlMsg;
extern IO_MSG IOMsg;
extern SEQ_MSG SeqMsg;
extern ERROR_MSG ErrMsg;


void Draw_Icon(uint32_t x, uint32_t y, uint32_t width, uint8_t icon_num, uint8_t clear_flag)
{
  uint32_t cur_page;
  int32_t i;
  uint8_t *image;
  
  image = (Icon_Graphic + (icon_num * 32));
  
  
  if(clear_flag == REDRAW_OFF)
  {
	 y = (y / 16) * 2;
	 cur_page = y;
	 cur_page *= 128;
	 cur_page += x;
	 width--;
	 for(i = width ; i >= 0 ; i--)
	 {
		*(Display_Scr + cur_page + i) |= *(image + i);
	 }
	 
	 cur_page = y + 1;
	 cur_page *= 128;
	 cur_page += x;
	 image += 16;
	 for(i = width ; i >= 0 ; i--)
	 {
		*(Display_Scr + cur_page + i) |= *(image + i);
	 }
  }
  else
  {
	 y = (y / 16) * 2;
	 cur_page = y;
	 cur_page *= 128;
	 cur_page += x;
	 memcpy(Display_Scr + cur_page, image, width);
	 
	 cur_page = y + 1;
	 cur_page *= 128;
	 cur_page += x;
	 image += 16;
	 memcpy(Display_Scr + cur_page, image, width);
  }
}

void Draw_Icon1()
{
  uint8_t KickTemp = 0;  //DrawIcon.c만 쓰임
  static uint8_t SwingTemp = 0; //DrawIcon.c만 쓰임
  static uint8_t IO_Display[3];
  
  switch(ControlMsg.TpMode)
  {
	 
	 //MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING//MANUAL_OPERATING
  case MANUAL_OPERATING:
	 
	 //MotionArm이 제품이거나 제품런너 일떄
	 if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS))     
	 {
		
		if (IOMsg.Y20_Down == SIGNAL_OFF)
		  Draw_Icon(MANUAL_PAGE_MSG2_OP0_LEFT, MANUAL_PAGE_MSG2_OP0_BOT, MANUAL_PAGE_MSG2_OP0_WIDTH, 
						IOMsg.Y20_Down + GRP_ARROW_UP_Complete, REDRAW_ON);
		else if (IOMsg.Y20_Down == SIGNAL_ON)
		  Draw_Icon(MANUAL_PAGE_MSG2_OP0_LEFT, MANUAL_PAGE_MSG2_OP0_BOT, MANUAL_PAGE_MSG2_OP0_WIDTH, 
						IOMsg.Y20_Down + GRP_ARROW_UP, REDRAW_ON);
		
		
		if(RobotCfg.MotionMode.MainChuck == USE)
		  Draw_Icon(MANUAL_PAGE_MSG2_OP4_LEFT, MANUAL_PAGE_MSG2_OP4_BOT, MANUAL_PAGE_MSG2_OP4_WIDTH,
						IOMsg.Y22_Chuck + GRP_P_CHUCK_OFF, REDRAW_ON);
		
		//XC 타입, TWIN타입
		if((RobotCfg.RobotType == ROBOT_TYPE_XC) || (RobotCfg.RobotType == ROBOT_TYPE_TWIN))
		{
		  //흡착 사용이면
		  if(RobotCfg.MotionMode.MainVaccum == USE)
			 Draw_Icon(MANUAL_PAGE_MSG2_OP5_LEFT, MANUAL_PAGE_MSG2_OP5_BOT, MANUAL_PAGE_MSG2_OP5_WIDTH,
						  IOMsg.Y25_Vaccum + GRP_P_SUCTION_OFF, REDRAW_ON);
		}
		
		//A타입제외 -> X,XC,XN?,TWIN
		if(RobotCfg.RobotType != ROBOT_TYPE_A)
		{
		  if(RobotCfg.MotionMode.MainRotateChuck == USE)
			 Draw_Icon(MANUAL_PAGE_MSG2_OP6_LEFT, MANUAL_PAGE_MSG2_OP6_BOT, MANUAL_PAGE_MSG2_OP6_WIDTH, 
						  IOMsg.Y24_ChuckRotation + GRP_CHUCK_ROTATE, REDRAW_ON);
		}
		//			 if(RobotCfg.MotionMode.MainNipper == USE)
		//				Draw_Icon(MANUAL_PAGE_MSG2_OP8_LEFT, MANUAL_PAGE_MSG2_OP8_BOT, MANUAL_PAGE_MSG2_OP8_WIDTH, 
		//							 IOMsg.Y26_Nipper + GRP_NIPPER_OFF, REDRAW_ON);
	 }
	 
	 
	 
	 if((RobotCfg.RobotType == ROBOT_TYPE_TWIN) && 
		 ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)))//When Use SUB Arm
	 {
		if (IOMsg.Y2D_SubUp == SIGNAL_OFF)
		  Draw_Icon(MANUAL_PAGE_MSG2_OP3_LEFT, MANUAL_PAGE_MSG2_OP3_BOT, MANUAL_PAGE_MSG2_OP3_WIDTH,
						IOMsg.Y2D_SubUp + GRP_R_ARROW_UP_Complete, REDRAW_ON);
		else if (IOMsg.Y2D_SubUp == SIGNAL_ON)
		  Draw_Icon(MANUAL_PAGE_MSG2_OP3_LEFT, MANUAL_PAGE_MSG2_OP3_BOT, MANUAL_PAGE_MSG2_OP3_WIDTH,
						IOMsg.Y2D_SubUp + GRP_R_ARROW_UP, REDRAW_ON);
		Draw_Icon(MANUAL_PAGE_MSG2_OP7_LEFT, MANUAL_PAGE_MSG2_OP7_BOT, MANUAL_PAGE_MSG2_OP7_WIDTH,
					 IOMsg.Y27_SubGrip + GRP_R_Grip_OFF, REDRAW_ON);
	 }
	 if(((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (IOMsg.Y21_Kick == SIGNAL_ON)) 
		 || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (IOMsg.Y2E_SubKick == SIGNAL_ON))) KickTemp = 1;
	 else if((IOMsg.Y21_Kick == SIGNAL_OFF) && (IOMsg.Y2E_SubKick == SIGNAL_OFF)) KickTemp = 0;
	 
	 //		if((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y21_Kick == SIGNAL_OFF))
	 //		  if((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y21_Kick == SIGNAL_ON)) 
	 if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)
	 {
		if(((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y21_Kick == SIGNAL_OFF))
			|| ((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y2E_SubKick == SIGNAL_ON)) 
			  || ((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE) && (IOMsg.Y21_Kick == SIGNAL_ON))
				 || ((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE) && (IOMsg.Y2E_SubKick == SIGNAL_OFF)))
		  KickTemp = 0;
		else KickTemp = 1;
	 }
	 else if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN)
	 {
		if(((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y21_Kick == SIGNAL_OFF)) 
			|| ((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE) && (IOMsg.Y21_Kick == SIGNAL_ON)))
		  KickTemp = 0;
		else KickTemp = 1;
	 }
	 else if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB)
	 {
		if(((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y2E_SubKick == SIGNAL_ON)) ||
			((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE) && (IOMsg.Y2E_SubKick == SIGNAL_OFF)))
		  KickTemp = 0;
		else KickTemp = 1;
	 }
	 Draw_Icon(MANUAL_PAGE_MSG2_OP1_LEFT, MANUAL_PAGE_MSG2_OP1_BOT, MANUAL_PAGE_MSG2_OP1_WIDTH,
				  KickTemp + GRP_ARROW_BACKWARD, REDRAW_ON);
	 
	 if((IOMsg.X14_SwingOk == SIGNAL_ON) || (IOMsg.Y23_Swing == SIGNAL_ON)) SwingTemp = 1;
	 else if((IOMsg.X15_SwingReturnOk == SIGNAL_ON) || (IOMsg.Y2F_SwingReturn == SIGNAL_ON)) SwingTemp = 0;
	 
	 //		if ((IOMsg.X14_SwingOk == SIGNAL_ON) || (IOMsg.X15_SwingReturnOk == SIGNAL_ON))
	 Draw_Icon(MANUAL_PAGE_MSG2_OP2_LEFT, MANUAL_PAGE_MSG2_OP2_BOT, MANUAL_PAGE_MSG2_OP2_WIDTH, 
				  SwingTemp + GRP_ARROW_ROTATE_RETURN_Complete, REDRAW_ON);		
	 //		else if ((IOMsg.X14_SwingOk == SIGNAL_OFF) || (IOMsg.X15_SwingReturnOk == SIGNAL_OFF))
	 //		  Draw_Icon(MANUAL_PAGE_MSG2_OP2_LEFT, MANUAL_PAGE_MSG2_OP2_BOT, MANUAL_PAGE_MSG2_OP2_WIDTH, 
	 //						SwingTemp + GRP_ARROW_ROTATE_RETURN, REDRAW_ON);	
	 
	 //Interlock
	 if(RobotCfg.Setting.ItlFullAuto == USE && IOMsg.X1H_FullAuto == SIGNAL_ON)
		Draw_Icon(MANUAL_PAGE_MSG3_OP0_LEFT, MANUAL_PAGE_MSG3_OP0_BOT, MANUAL_PAGE_MSG3_OP0_WIDTH, GRP_FULLAUTO, REDRAW_ON);
	 
	 if(RobotCfg.Setting.ItlAutoInjection  == USE && IOMsg.X19_AutoInjection == SIGNAL_ON)
		Draw_Icon(MANUAL_PAGE_MSG3_OP1_LEFT, MANUAL_PAGE_MSG3_OP1_BOT, MANUAL_PAGE_MSG3_OP1_WIDTH, GRP_AUTO_INJECTION, REDRAW_ON);
	 
	 if(IOMsg.X18_MoldOpenComplete == SIGNAL_ON)
		Draw_Icon(MANUAL_PAGE_MSG3_OP2_LEFT, MANUAL_PAGE_MSG3_OP2_BOT, MANUAL_PAGE_MSG3_OP2_WIDTH, GRP_MOLD_OPEN_Complete, REDRAW_ON);
	 
	 if(RobotCfg.Setting.ItlSaftyDoor  == USE && IOMsg.X1A_SaftyDoor == SIGNAL_ON)
		Draw_Icon(MANUAL_PAGE_MSG3_OP3_LEFT, MANUAL_PAGE_MSG3_OP3_BOT, MANUAL_PAGE_MSG3_OP3_WIDTH, GRP_SAFETY_DOOR, REDRAW_ON);
	 if(IOMsg.Y2A_MoldOpenClose == SIGNAL_ON)
		Draw_Icon(MANUAL_PAGE_MSG3_OP4_LEFT, MANUAL_PAGE_MSG3_OP4_BOT, MANUAL_PAGE_MSG3_OP4_WIDTH, GRP_MOLD_OPEN_N_CLOSE, REDRAW_ON);
	 if(IOMsg.Y2B_Ejector == SIGNAL_ON)
		Draw_Icon(MANUAL_PAGE_MSG3_OP5_LEFT, MANUAL_PAGE_MSG3_OP5_BOT, MANUAL_PAGE_MSG3_OP5_WIDTH, GRP_EJECTOR, REDRAW_ON);
	 
	 //Function
	 Draw_Icon(ICON_LEFT, ICON_1_BOT, ICON_WIDTH, RobotCfg.Function.Buzzer + buzzer_ER, REDRAW_OFF);
	 Draw_Icon(ICON_LEFT, ICON_2_BOT, ICON_WIDTH, RobotCfg.Function.Detection + detect_switch_ER, REDRAW_OFF);
	 Draw_Icon(ICON_LEFT, ICON_3_BOT, ICON_WIDTH, RobotCfg.Function.Ejector + ejector_ER, REDRAW_OFF);
	 Draw_Icon(ICON_LEFT, ICON_4_BOT, ICON_WIDTH, RobotCfg.Function.Reject + reject_ER, REDRAW_OFF);
	 break;
	 
	 
	 
	 //MANUAL_CYCLE_OPERATING//MANUAL_CYCLE_OPERATING//MANUAL_CYCLE_OPERATING//MANUAL_CYCLE_OPERATING//MANUAL_CYCLE_OPERATING//MANUAL_CYCLE_OPERATING
  case MANUAL_CYCLE_OPERATING:
	 //ICON ">"
	 Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.AutoLineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
					Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
	 break;
	 //AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING//AUTO_OPERATING
  case AUTO_OPERATING:
	 Draw_Icon(ICON_LEFT, ICON_1_BOT, ICON_WIDTH, RobotCfg.Function.Buzzer + buzzer_ER, REDRAW_OFF);
	 Draw_Icon(ICON_LEFT, ICON_2_BOT, ICON_WIDTH, RobotCfg.Function.Detection + detect_switch_ER, REDRAW_OFF);
	 Draw_Icon(ICON_LEFT, ICON_3_BOT, ICON_WIDTH, RobotCfg.Function.Ejector + ejector_ER, REDRAW_OFF);
	 Draw_Icon(ICON_LEFT, ICON_4_BOT, ICON_WIDTH, RobotCfg.Function.Reject + reject_ER, REDRAW_OFF);
	 
	 Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.AutoLineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
					Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
	 break;
	 //MANUAL_MODE//MANUAL_MODE//MANUAL_MODE//MANUAL_MODE//MANUAL_MODE//MANUAL_MODE//MANUAL_MODE//MANUAL_MODE//MANUAL_MODE//MANUAL_MODE
  case MANUAL_MODE:
	 Draw_Icon(MODE_CURSOR_LEFT, OUTLINE_LO_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
				  GRP_ARROW_RIGHT, REDRAW_OFF);
	 break;
	 //SETTING_MANUFACTURER//SETTING_MANUFACTURER//SETTING_MANUFACTURER//SETTING_MANUFACTURER//SETTING_MANUFACTURER//SETTING_MANUFACTURER
  case SETTING_MANUFACTURER:
	 //ICON ">"	 
	 Draw_Icon(SETTING_CURSOR_LEFT, OUTLINE_LO_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), SETTING_CURSOR_WIDTH,
				  GRP_ARROW_RIGHT, REDRAW_OFF);
	 break;
	 //MANUAL_COUNTER//MANUAL_COUNTER//MANUAL_COUNTER//MANUAL_COUNTER//MANUAL_COUNTER//MANUAL_COUNTER//MANUAL_COUNTER//MANUAL_COUNTER
  case MANUAL_COUNTER:
	 //ICON ">"
	 Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
					Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
	 break;
	 //MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO//MANUAL_IO
  case MANUAL_IO:
	 if(ControlMsg.LineNo == IO_INPUT)
	 {
		if(ControlMsg.PageNo == 0)
		{
		  IO_Display[0] = IOMsg.X11_MainArmUpComplete;
		  IO_Display[1] = IO_NOT_DISPLAY;
		  IO_Display[2] = IOMsg.X16_MainChuckOk;
		}
		else if(ControlMsg.PageNo == 1)
		{
		  IO_Display[0] = IOMsg.X14_SwingOk;
		  IO_Display[1] = IOMsg.X15_SwingReturnOk;
		  IO_Display[2] = IO_NOT_DISPLAY;
		}
		else if(ControlMsg.PageNo == 2)
		{
		  IO_Display[0] = IOMsg.X17_MainVaccumOk;
		  IO_Display[1] = IO_NOT_DISPLAY;
		  IO_Display[2] = IOMsg.X1G_SubUpComplete;
		}
		else if(ControlMsg.PageNo == 3)
		{
		  IO_Display[0] = IO_NOT_DISPLAY;
		  IO_Display[1] = IOMsg.X1F_SubGripComplete;
		  IO_Display[2] = IO_NOT_DISPLAY;
		}
		else if(ControlMsg.PageNo == 4)
		{
		  IO_Display[0] = IOMsg.X1H_FullAuto;
		  IO_Display[1] = IOMsg.X19_AutoInjection;
		  IO_Display[2] = IOMsg.X18_MoldOpenComplete;
		}
		else if(ControlMsg.PageNo == 5)
		{
		  IO_Display[0] = IOMsg.X1A_SaftyDoor;
		  IO_Display[1] = IOMsg.X1B_Reject;
		  IO_Display[2] = IOMsg.X1I_EMOFromIMM;
		}
	 }
	 else if(ControlMsg.LineNo == IO_OUTPUT)
	 {
		if(ControlMsg.PageNo == 0)
		{
		  IO_Display[0] = IOMsg.Y20_Down;
		  IO_Display[1] = IOMsg.Y21_Kick;
		  IO_Display[2] = IOMsg.Y22_Chuck;
		}
		else if(ControlMsg.PageNo == 1)
		{
		  IO_Display[0] = IOMsg.Y23_Swing;
		  IO_Display[1] = IOMsg.Y2F_SwingReturn;
		  IO_Display[2] = IOMsg.Y24_ChuckRotation;
		}
		else if(ControlMsg.PageNo == 2)
		{
		  IO_Display[0] = IOMsg.Y25_Vaccum;
		  IO_Display[1] = IOMsg.Y26_Nipper;
		  IO_Display[2] = IOMsg.Y2D_SubUp;
		}
		else if(ControlMsg.PageNo == 3)
		{
		  IO_Display[0] = IOMsg.Y2E_SubKick;
		  IO_Display[1] = IOMsg.Y27_SubGrip;
		  IO_Display[2] = IOMsg.Y28_Alarm;
		}
		else if(ControlMsg.PageNo == 4)
		{
		  IO_Display[0] = IOMsg.Y29_CycleStart;
		  IO_Display[1] = IOMsg.Y2A_MoldOpenClose;
		  IO_Display[2] = IOMsg.Y2B_Ejector;
		}
		else if(ControlMsg.PageNo == 5)
		{
		  IO_Display[0] = IOMsg.Y2C_Conveyor;
		  IO_Display[1] = IOMsg.Y28_Alarm;
		  IO_Display[2] = IO_NOT_DISPLAY;
		}
	 }
	 
	 if(IO_Display[0] != IO_NOT_DISPLAY) Draw_Icon(IO_ICON_LEFT, TITLE_L0_BOT, IO_ICON_WIDTH, GRP_RAMP_OFF + IO_Display[0], REDRAW_ON);
	 if(IO_Display[1] != IO_NOT_DISPLAY) Draw_Icon(IO_ICON_LEFT, TITLE_L1_BOT, IO_ICON_WIDTH, GRP_RAMP_OFF + IO_Display[1], REDRAW_ON);
	 if(IO_Display[2] != IO_NOT_DISPLAY) Draw_Icon(IO_ICON_LEFT, TITLE_L2_BOT, IO_ICON_WIDTH, GRP_UNDER_RAMP_OFF + IO_Display[2], REDRAW_ON);
	 
	 break;
	 
	 
  case MANUAL_TIMER:
	 //ICON "<"
	 Lcd_Printf(TIMER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
					Normal_Option[0], CENTER_ALIGN, REDRAW_OFF);
	 break;
	 
  case MANUAL_STEP_OPERATING:
	 //ICON ">"
	 if (((ControlMsg.KeyValue == KEY_ARROW_DOWM) || (StepCursor != DISPLAY_OFF)) && (IOMsg.X18_MoldOpenComplete == SIGNAL_ON))
	 {
		Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
					  Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
		StepCursor = DISPLAY_ON;
	 }
	 break;
	 
  case MANUAL_MOLD_SEARCH:
	 break;
	 
  case MANUAL_MOLD_MANAGE:
	 Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
					Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
	 break;
	 
  case MANUAL_MOLD_MODE:
	 //ICON ">"
	 //      Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
	 //                 Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
	 Draw_Icon(MODE_CURSOR_LEFT, OUTLINE_LO_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
				  GRP_ARROW_LEFT, REDRAW_OFF);
	 break;
	 
  case MANUAL_MOLD_NEW:
	 break;
	 
  case MANUAL_ERROR_LIST:
	 break;
	 
  case MANUAL_VERSION:
	 break;
	 
  case AUTO_MODE:
	 Draw_Icon(MODE_CURSOR_LEFT, OUTLINE_LO_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
				  GRP_ARROW_RIGHT, REDRAW_OFF);
	 break;
	 
  case AUTO_COUNTER:
	 Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
					Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
	 break;
	 
  case AUTO_TIMER:
	 //ICON "<"
	 Lcd_Printf(TIMER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
					Normal_Option[0], CENTER_ALIGN, REDRAW_OFF);
	 break;
	 
  case AUTO_IO:
	 if(ControlMsg.LineNo == IO_INPUT)
	 {
		if(ControlMsg.PageNo == 0)
		{
		  IO_Display[0] = IOMsg.X11_MainArmUpComplete;
		  IO_Display[1] = IO_NOT_DISPLAY;
		  IO_Display[2] = IOMsg.X16_MainChuckOk;
		}
		else if(ControlMsg.PageNo == 1)
		{
		  IO_Display[0] = IOMsg.X14_SwingOk;
		  IO_Display[1] = IOMsg.X15_SwingReturnOk;
		  IO_Display[2] = IO_NOT_DISPLAY;
		}
		else if(ControlMsg.PageNo == 2)
		{
		  IO_Display[0] = IOMsg.X17_MainVaccumOk;
		  IO_Display[1] = IO_NOT_DISPLAY;
		  IO_Display[2] = IOMsg.X1G_SubUpComplete;
		}
		else if(ControlMsg.PageNo == 3)
		{
		  IO_Display[0] = IO_NOT_DISPLAY;
		  IO_Display[1] = IOMsg.X1F_SubGripComplete;
		  IO_Display[2] = IO_NOT_DISPLAY;
		}
		else if(ControlMsg.PageNo == 4)
		{
		  IO_Display[0] = IOMsg.X1H_FullAuto;
		  IO_Display[1] = IOMsg.X19_AutoInjection;
		  IO_Display[2] = IOMsg.X18_MoldOpenComplete;
		}
		else if(ControlMsg.PageNo == 5)
		{
		  IO_Display[0] = IOMsg.X1A_SaftyDoor;
		  IO_Display[1] = IOMsg.X1B_Reject;
		  IO_Display[2] = IOMsg.X1I_EMOFromIMM;
		}
	 }
	 else if(ControlMsg.LineNo == IO_OUTPUT)
	 {
		if(ControlMsg.PageNo == 0)
		{
		  IO_Display[0] = IOMsg.Y20_Down;
		  IO_Display[1] = IOMsg.Y21_Kick;
		  IO_Display[2] = IOMsg.Y22_Chuck;
		}
		else if(ControlMsg.PageNo == 1)
		{
		  IO_Display[0] = IOMsg.Y23_Swing;
		  IO_Display[1] = IOMsg.Y2F_SwingReturn;
		  IO_Display[2] = IOMsg.Y24_ChuckRotation;
		}
		else if(ControlMsg.PageNo == 2)
		{
		  IO_Display[0] = IOMsg.Y25_Vaccum;
		  IO_Display[1] = IOMsg.Y26_Nipper;
		  IO_Display[2] = IOMsg.Y2D_SubUp;
		}
		else if(ControlMsg.PageNo == 3)
		{
		  IO_Display[0] = IOMsg.Y2E_SubKick;
		  IO_Display[1] = IOMsg.Y27_SubGrip;
		  IO_Display[2] = IOMsg.Y28_Alarm;
		}
		else if(ControlMsg.PageNo == 4)
		{
		  IO_Display[0] = IOMsg.Y29_CycleStart;
		  IO_Display[1] = IOMsg.Y2A_MoldOpenClose;
		  IO_Display[2] = IOMsg.Y2B_Ejector;
		}
		else if(ControlMsg.PageNo == 5)
		{
		  IO_Display[0] = IOMsg.Y2C_Conveyor;
		  IO_Display[1] = IOMsg.Y28_Alarm;
		  IO_Display[2] = IO_NOT_DISPLAY;
		}
	 }
	 
	 if(IO_Display[0] != IO_NOT_DISPLAY) Draw_Icon(IO_ICON_LEFT, TITLE_L0_BOT, IO_ICON_WIDTH, GRP_RAMP_OFF + IO_Display[0], REDRAW_ON);
	 if(IO_Display[1] != IO_NOT_DISPLAY) Draw_Icon(IO_ICON_LEFT, TITLE_L1_BOT, IO_ICON_WIDTH, GRP_RAMP_OFF + IO_Display[1], REDRAW_ON);
	 if(IO_Display[2] != IO_NOT_DISPLAY) Draw_Icon(IO_ICON_LEFT, TITLE_L2_BOT, IO_ICON_WIDTH, GRP_UNDER_RAMP_OFF + IO_Display[2], REDRAW_ON);
	 
	 break;
	 
  case OCCUR_ERROR:
	 break;
	 
  default:
	 break;
  }
}