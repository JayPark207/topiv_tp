/* Includes -------------------------------------------------------------------*/
#include "AppError.h"
#include <string.h>
#include "stdlib.h"

/* Private function prototypes -----------------------------------------------*/
void WriteErrorList(void);
void LoopAppError(void);
/* ---------------------------------------------------------------------------*/

void LoopAppError(void)
{
	static enum
	{
	  ERR_CHECK = 0,
	  ERR_WRITE,
	  ERR_TAKEOUT,
	  ERR_EMO,
	  ERR_SENSOR,
	  ERR_CLEAR,
	  ERR_CLEAR_ACCEPT,
	}ErrLoopState = ERR_CHECK;
	
  switch(ErrLoopState)
  {
	 //ERR_CHECK//ERR_CHECK//ERR_CHECK//ERR_CHECK//ERR_CHECK//ERR_CHECK//ERR_CHECK//ERR_CHECK//ERR_CHECK
  case ERR_CHECK:
    if((RobotCfg.ErrorState == ERROR_STATE_EMO) || (RobotCfg.ErrCode == ERRCODE_IMM_EMG))
    {
      ControlMsg.CommMode = MODE_ERROR;
      ControlMsg.TpMode = OCCUR_ERROR;
      ErrLoopState = ERR_WRITE;
      
      IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
      IOMsg.Y2B_Ejector = SIGNAL_OFF;
      IOMsg.Y29_CycleStart = SIGNAL_OFF;
      IOMsg.Y2C_Conveyor = SIGNAL_OFF;
      RobotCfg.ErrCode = ERRCODE_EMG;
    }
    else if(RobotCfg.ErrorState == ERROR_STATE_OCCUR)
    {
      ControlMsg.CommMode = MODE_ERROR;
      ControlMsg.TpMode = OCCUR_ERROR;
      ErrLoopState = ERR_WRITE;
    }
    break;
	 
    //ERR_WRITE//ERR_WRITE//ERR_WRITE//ERR_WRITE//ERR_WRITE//ERR_WRITE//ERR_WRITE//ERR_WRITE//ERR_WRITE//ERR_WRITE//ERR_WRITE//ERR_WRITE//ERR_WRITE
  case ERR_WRITE:
    if(RobotCfg.ErrCode != 0)
    {
      WriteErrorList();
      if((RobotCfg.ErrorState == ERROR_STATE_EMO) || (RobotCfg.ErrCode == ERRCODE_IMM_EMG))       //? or 사출비상
        ErrLoopState = ERR_EMO;
		
      else if ((RobotCfg.ErrCode == ERRCODE_VACCUM) || (RobotCfg.ErrCode == ERRCODE_CHUCK) || (RobotCfg.ErrCode == ERRCODE_RUNNER_PICK))  //흡착/척/런너 타임아웃
      {
        //Count
        CountMsg.DetectFail = RobotCfg.Count.DetectFail;
        memcpy(&(MoldMsg.Count), &CountMsg, sizeof(CountMsg));
        WriteRobotCfg(&RobotCfg);
        fPreviousPage = MANUAL_ERROR_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
        
        //		  WriteMold(&MoldMsg);
        ErrLoopState = ERR_TAKEOUT;
      }
      else ErrLoopState = ERR_SENSOR;
    }
    break;
	 
	 
    //ERR_EMO//ERR_EMO//ERR_EMO//ERR_EMO//ERR_EMO//ERR_EMO//ERR_EMO//ERR_EMO//ERR_EMO//ERR_EMO//ERR_EMO//ERR_EMO//ERR_EMO
  case ERR_EMO:
    if (HAL_GPIO_ReadPin(EMG_GPIO_Port,EMG_Pin) == GPIO_PIN_SET)
    {
      RobotCfg.ErrorState = ERROR_STATE_CLEAR;
      ErrLoopState = ERR_CLEAR;
    }
    break;
	 
	 
    //ERR_TAKEOUT//ERR_TAKEOUT//ERR_TAKEOUT//ERR_TAKEOUT//ERR_TAKEOUT//ERR_TAKEOUT//ERR_TAKEOUT//ERR_TAKEOUT//ERR_TAKEOUT
  case ERR_TAKEOUT:
    if((RobotCfg.Setting.ItlSaftyDoor == USE) && (RobotCfg.ErrorState == ERROR_STATE_CLEAR))
    {
      RobotCfg.ErrorState = ERROR_STATE_ACCEPT;
      ErrLoopState = ERR_CLEAR_ACCEPT;
      ControlMsg.KeyValue = KEY_NONE;
    }
    else if(!((RobotCfg.Setting.ItlSaftyDoor == USE) && (RobotCfg.Setting.ItlAutoInjection == USE)))
    {
      if (ControlMsg.KeyValue == KEY_CLEAR)
      {
        RobotCfg.ErrorState = ERROR_STATE_CLEAR;
        ErrLoopState = ERR_CLEAR;
        ControlMsg.KeyValue = KEY_NONE;
      }
    }
    break;
	 
	 
    //ERR_SENSOR//ERR_SENSOR//ERR_SENSOR//ERR_SENSOR//ERR_SENSOR//ERR_SENSOR//ERR_SENSOR//ERR_SENSOR//ERR_SENSOR//ERR_SENSOR
  case ERR_SENSOR:
    if(ControlMsg.KeyValue == KEY_CLEAR)
    {
      RobotCfg.ErrorState = ERROR_STATE_CLEAR;
      ErrLoopState = ERR_CLEAR;
      ControlMsg.KeyValue = KEY_NONE;
    }
    break;
	 
	 
    //ERR_CLEAR//ERR_CLEAR//ERR_CLEAR//ERR_CLEAR//ERR_CLEAR//ERR_CLEAR//ERR_CLEAR//ERR_CLEAR//ERR_CLEAR//ERR_CLEAR//ERR_CLEAR
  case ERR_CLEAR:
    if(ControlMsg.KeyValue == KEY_CLEAR)
    {
      RobotCfg.ErrorState = ERROR_STATE_ACCEPT;
      ErrLoopState = ERR_CLEAR_ACCEPT;
      ControlMsg.KeyValue = KEY_NONE;
    }
    break;
	 
	 
    //ERR_CLEAR_ACCEPT//ERR_CLEAR_ACCEPT//ERR_CLEAR_ACCEPT//ERR_CLEAR_ACCEPT//ERR_CLEAR_ACCEPT//ERR_CLEAR_ACCEPT//ERR_CLEAR_ACCEPT
  case ERR_CLEAR_ACCEPT:
    if(RobotCfg.ErrorState == ERROR_STATE_NONE)
    {
      ControlMsg.TpMode = MANUAL_OPERATING;
      RobotCfg.ErrCode = 0;
      ClearPage();
      ClearAuto();
      SeqMsg.SeqDispPos = 0;
      ControlMsg.StepSeq = 0;
      ErrLoopState = ERR_CHECK;
    }
    break;
  }
}

void WriteErrorList(void)
{
  ErrMsg.ErrorCode = RobotCfg.ErrCode;
  ErrMsg.ErrorNo++;
  ErrMsg.Time = GetTime();
  WriteError(&ErrMsg);              //전역변수라 주소값 안넣어도되지않나?
}