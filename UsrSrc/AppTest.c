//#include "AppTest.h"
//#include "stm32f4xx_hal.h"
//#include "AppKey.h"
//#include "SDCard.h"
//#include <stdio.h>
//#include <string.h>
//
////extern RTC_HandleTypeDef hrtc;
//STRUCT_TIME sRtc;
//
//extern IO_MSG IOMsg;
//extern ERROR_MSG ErrMsg;
//
//extern CONTROL_MSG ControlMsg;
//extern ROBOT_CONFIG RobotCfg;
//
//void LoopAppTest(void)
//{
//  static uint64_t TempTick = 0;
//  //  static uint16_t TempCnt = 0;
//  //	static MOLD_MSG* pMoldMsg;
//  //  static ERROR_MSG ErrHistory[MAX_LOG_COUNT];
//  static uint32_t TestData = 0;
//  static enum
//  {
//	 INIT = 0,
//	 
//	 STATE_KEY_TIME_INTERVAL,
//	 STATE_KEY_PRESS,
//	 
//	 
//	 STATE_ON,
//	 STATE_OFF,
//	 
//	 STATE_RTC,
//	 
//	 STATE_SD_INIT,
//	 STATE_SD_WRITE,
//	 STATE_SD_CHECK,
//	 
//	 STATE_SD_LinkDriver,
//	 STATE_SD_MOUNT,
//	 STATE_SD_FORMAT,
//	 STATE_SD_FILE_OPEN,
//	 STATE_SD_FILE_WRITE,
//	 STATE_SD_FILE_CLOSE,
//	 STATE_SD_FILE_OPEN_FOR_READ,
//	 STATE_SD_FILE_READ,
//	 STATE_SD_FILE_CLOSE_FOR_READ,
//	 
//	 
//	 STATE_READ_RTC,
//	 STATE_SET_RTC,
//	 
//	 
//  }TestLoopState = INIT; 
//  
//  switch(TestLoopState)
//  {
//	 
//  case INIT:
//	 
//	 SetSysTick(&TempTick, TIME_TEST);
//	 
//	 
//#if (TEST_MODE == TEST_LOG)
//	  ErrMsg.Time.Date.Year = 18;
//	  ErrMsg.Time.Date.Month = 5;
//	  ErrMsg.Time.Date.Date = 10;
//	  ErrMsg.Time.Time.Hours = 14;
//	  ErrMsg.Time.Time.Minutes = 20;
//	  ErrMsg.Time.Time.Seconds = 30;
//	  SetTime(&(ErrMsg.Time));
//	 TestLoopState = STATE_SD_INIT;
//#elif (TEST_MODE == TEST_SETTING)
//	 //	  SettingMsg.ControlMode = CONTROLMODE_CYCLE;
//	 //	  SettingMsg.Count.TotalCnt = 9900;
//	 //	  SettingMsg.Count.NormalCnt = 9000;
//	 //	  SettingMsg.Count.RejectCnt = 900;
//	 //	  SettingMsg.FuncSaveCnt = FUNC_ON;
//	 //	  SettingMsg.Language = LANGUAGE_ENG;
//	 //	  SettingMsg.MoldCnt = 100;
//	 //	  SettingMsg.MoldNo = 99;
//	 //	  SettingMsg.Version = 10000;
//	 //	  SettingMsg.Time.Date.Year = 18;
//	 //	  SettingMsg.Time.Date.Month = 5;
//	 //	  SettingMsg.Time.Date.Date = 20;
//	 //	  SettingMsg.Time.Time.Hours = 17;
//	 //	  SettingMsg.Time.Time.Minutes = 20;
//	 //	  SettingMsg.Time.Time.Seconds = 30;
//	 //	  SetTime(&(SettingMsg.Time));
//	 TestLoopState = STATE_SD_INIT;
//#elif (TEST_MODE == TEST_MOLD)
//	 
//	 pMoldMsg = &MoldMsg;
//	 memset(&(pMoldMsg->MoldDelay), 20, sizeof(MOLD_DELAY));
//	 memset(&(pMoldMsg->MoldConfig), 1, sizeof(MOLD_CONFIG));
//	 pMoldMsg->MoldNo = 100;
//	 sprintf((pMoldMsg->MoldName), "Hundred");
//	 TestLoopState = STATE_SD_INIT;
//#elif (TEST_MODE == TEST_RTC)
//	 sRtc = GetTime();
//	 printf("%2d-%2d-%2d %2d:%2d:%2d\n",
//			  sRtc.Date.Year,sRtc.Date.Month,sRtc.Date.Date,
//			  sRtc.Time.Hours,sRtc.Time.Minutes,sRtc.Time.Seconds);
//	 
//	 HAL_Delay(100);
//	 if(sRtc.Date.Year < 18)
//	 {
//		sRtc.Date.Year = 18;
//		sRtc.Date.Month = 5;
//		sRtc.Date.Date = 23;
//		sRtc.Time.Hours = 14;
//		sRtc.Time.Minutes = 20;
//		sRtc.Time.Seconds = 30;
//		SetTime(&(sRtc));
//		printf("Set Time\n");
//	 }
//	 TestLoopState = STATE_RTC;
//#elif (TEST_MODE == TEST_SEQUENCE)	  
//	 TestLoopState = STATE_KEY_TIME_INTERVAL;
//#endif
//	 
//	 break;
//	 
//	 
//	 /******************TEST_SEQUENCE*******************/
//  case STATE_KEY_TIME_INTERVAL:
//	 if(!ChkExpireSysTick(&TempTick)) break;
//	 
//	 switch(ControlMsg.TpMode)
//	 {
//	 case INIT_READ_TYPE:		  
//	 case INIT_DISP_LOGO:		  
//	 case INIT_LOAD_SD:
//		ControlMsg.KeyReady = KEY_PRESSED;
//		//		  ControlMsg.TpMode = MANUAL_OPERATING;
//		break;
//		
//	 case MANUAL_OPERATING:
//		
//		ControlMsg.DoComm = DO_COMM;
//		ControlMsg.CommMode = WRITE_MOTION;
//		
//		//		  ControlMsg.KeyReady = KEY_PRESSED;
//		//		  ControlMsg.KeyValue = KEY_MANUFACTURER;//KEY_MENU_MODE;//KEY_MENU_MODE;//KEY_MENU_IO;//KEY_MENU_TIMER;//KEY_MENU_COUNTER;
//		break;
//		
//	 case MANUAL_MODE:
//		  ControlMsg.KeyReady = KEY_PRESSED;
//		  ControlMsg.KeyValue = KEY_ARROW_DOWM;
//		if(ControlMsg.PageNo >= MODE_MAX_PAGE && ControlMsg.LineNo > 0)
//		  {
//			  ControlMsg.PageNo = 0;
//			  ControlMsg.LineNo ^= 1;
//		  }
//		break;
//		
//	 case MANUAL_STEP_OPERATING:
//		
//	 case MANUAL_CYCLE_OPERATING:
//		
//	 case MANUAL_MOLD_SEARCH:
//		
//	 case MANUAL_MOLD_MANAGE:
//		
//	 case MANUAL_MOLD_MODE:
//		
//	 case MANUAL_MOLD_NEW:
//		
//	 case MANUAL_MOLD_DELETE:
//		
//	 case MANUAL_VERSION:
//		
//	 case AUTO_MESSAGE:
//		
//	 case AUTO_OPERATING:
//		
//	 case AUTO_MODE:	
//		
//	 case AUTO_COUNTER:
//		
//	 case AUTO_IO:
//		
//	 case AUTO_TIMER:
//		
//	 case AUTO_ERROR_LIST:
//		
//	 case OCCUR_ERROR:
//		
//	 case SETTING_MANUFACTURER:
//	  ControlMsg.KeyReady = KEY_PRESSED;
//		ControlMsg.KeyValue = KEY_ARROW_DOWM;
//		if(ControlMsg.PageNo >= MODE_MAX_PAGE && ControlMsg.LineNo > 0)
//		{
//		  ControlMsg.PageNo = 0;
//		  ControlMsg.LineNo = 0;
//		}
//		break;
//		
//	 }
//	 //	  if(ControlMsg.TpMode)
//	 //	  ControlMsg.KeyReady = KEY_PRESSED;
//	 //	  ControlMsg.KeyValue = KEY_MENU_MOLD;
//	 
//	 //	  RobotCfg.Buzzer ^= FUNC_ON;
//	 //	  RobotCfg.Detection ^= FUNC_ON;
//	 //	  RobotCfg.Ejector ^= FUNC_ON;
//	 //	  RobotCfg.Reject ^= FUNC_ON;
//	 //	  
//	 //	  StateMsg.FullAuto ^= SIGNAL_ON;
//	 //	  StateMsg.AutoInjection ^= SIGNAL_ON;
//	 //	  StateMsg.MoldOpenComplete ^= SIGNAL_ON;
//	 //	  StateMsg.SaftyDoor ^= SIGNAL_ON;
//	 //	  StateMsg.MoldAction ^= SIGNAL_ON;
//	 //	  StateMsg.Ejector ^= SIGNAL_ON;
//	 //	  
//	 //	  StateMsg.UpDown = TestData % 4;
//	 //	  StateMsg.Kick = TestData % 2;
//	 //	  StateMsg.Swing = TestData % 4;
//	 //	  StateMsg.SubUpDown = TestData % 4;
//	 //	  StateMsg.Chuck = TestData % 2;
//	 //	  StateMsg.Vacuum = TestData % 2;
//	 //	  StateMsg.ChuckRotate = TestData % 2;
//	 //	  StateMsg.SubGrip = TestData % 2;	
//	 
//	 TestData++;
//	 
//	 TestLoopState = STATE_KEY_PRESS;
//	 break;
//	 
//  case STATE_KEY_PRESS:
//	 SetSysTick(&TempTick, TIME_KEYCMD_FOR_LCD);
//	 //	  printf("TpMode = %4X, KeyReady = %4X, IsChangedLCD = %4X\n",ControlMsg.TpMode, ControlMsg.KeyReady, ControlMsg.IsChangedLCD);
//	 TestLoopState = STATE_KEY_TIME_INTERVAL;
//	 break;
//	 
//	 
//	 
//	 /******************TEST_SEQUENCE*******************/
//	 
//	 
//  case STATE_SD_INIT:
//	 if(ChkExpireSysTick(&TempTick))
//	 {
//		if(InitSD() != SD_OK)
//		{
//		  printf("ERROR : FAIL!!****\n");
//		  TestLoopState = STATE_SD_CHECK;
//		}
//		else
//		{
//		  printf("Initialize SD!!\n");
//		  TestLoopState = STATE_SD_WRITE;
//		}
//		SetSysTick(&TempTick, 1);
//	 }
//	 break;
//	 
//  case STATE_SD_WRITE:
//	 if(ChkExpireSysTick(&TempTick))
//	 {
//		//		  static uint8_t i;
//		//		  printf("SDPath = %s\n",SDPath);
//#if (TEST_MODE == TEST_LOG)
//		  //ADD LogTEST*********************************
////		  i++;
//		  ErrMsg.ErrorCode++;
////		  ErrMsg.MoldNo += 2;
//		  ErrMsg.Time = GetTime();
//		  AddSDLog(&ErrMsg);
//		  
//		  //		  if(i > (MAX_LOG_COUNT-5)) 
////		  if(i > 1) 
////		  {
////			  //			  printf("Done.......\n\r");
////			  TestLoopState = STATE_SD_CHECK;
////		  }
//#elif (TEST_MODE == TEST_SETTING)
//		SaveSDSetting(&SettingMsg);
//		//		  memset(&SettingMsg, 0, sizeof(SETTING_MSG));
//		TestLoopState = STATE_SD_CHECK;
//#elif (TEST_MODE == TEST_MOLD)
//		SaveSDMold(pMoldMsg);
//		memset(pMoldMsg, 0, sizeof(MOLD_MSG));
//		TestLoopState = STATE_SD_CHECK;
//#endif
//		
//	 }
//	 break;
//	 
//  case STATE_SD_CHECK:
//#if (TEST_MODE == TEST_LOG)
//	 //LOAD LogTEST*********************************
//	 //	  LoadSDLog(ErrHistory, MAX_LOG_COUNT);
//#elif (TEST_MODE == TEST_SETTING)
//	 LoadSDSetting(&SettingMsg);
//	 printf( 
//			  "ControlMode : %3d\nVersion : %8d\nLanguage : %3d\nCount[Total] : %8d\nCount[Normal] : %8d\nCount[Reject] : %8d\nFuncSaveCnt : %3d\nMoldCnt : %6d\nMoldNo : %6d\n",
//			  SettingMsg.ControlMode, SettingMsg.Version, SettingMsg.Language,
//			  SettingMsg.Count.TotalCnt, SettingMsg.Count.NormalCnt, SettingMsg.Count.RejectCnt,
//			  SettingMsg.FuncSaveCnt, SettingMsg.MoldCnt, SettingMsg.MoldNo);
//#elif (TEST_MODE == TEST_MOLD)
//	 LoadSDMold(pMoldMsg, 100);
//	 printf("MoldNo : %3d\nMoldName : %8s\n\n",
//			  pMoldMsg->MoldNo, pMoldMsg->MoldName);
//	 
//	 printf("DownDelay : %3d\nKickDelay : %3d\nEjectorDelay : %3d\nChuckDelay : %3d\nKickReturnDelay : %3d\n\n",
//			  pMoldMsg->MoldDelay.DownDelay, pMoldMsg->MoldDelay.KickDelay,pMoldMsg->MoldDelay.EjectorDelay,
//			  pMoldMsg->MoldDelay.ChuckDelay,pMoldMsg->MoldDelay.KickReturnDelay);	
//	 
//	 printf("UpDelay : %3d\nSwingDelay : %3d\nDown2ndDelay : %3d\nOpenDelay : %3d\nUp2ndDelay : %3d\n\n",
//			  pMoldMsg->MoldDelay.UpDelay, pMoldMsg->MoldDelay.SwingDelay,pMoldMsg->MoldDelay.Down2ndDelay,
//			  pMoldMsg->MoldDelay.OpenDelay, pMoldMsg->MoldDelay.Up2ndDelay);	
//	 
//	 printf("ChuckRotateReturnDelay : %3d\nSwingReturnDelay : %3d\nNipperOnDelay : %3d\nConveyorDelay : %3d\n\n\n",
//			  pMoldMsg->MoldDelay.ChuckRotateReturnDelay,pMoldMsg->MoldDelay.SwingReturnDelay,
//			  pMoldMsg->MoldDelay.NipperOnDelay,pMoldMsg->MoldDelay.ConveyorDelay);	
//	 
//	 
//	 printf("MotionArm : %3d\nMainChuck : %3d\nMainVaccum : %3d\nMainRotateChuck : %3d\nNipper : %3d\n\n",
//			  pMoldMsg->MoldConfig.MotionArm, pMoldMsg->MoldConfig.MainChuck,pMoldMsg->MoldConfig.MainVaccum,
//			  pMoldMsg->MoldConfig.MainRotateChuck,pMoldMsg->MoldConfig.Nipper);	
//	 
//	 printf("OutsideWait : %3d\nMainArmType : %3d\nSubArmType : %3d\nMainArmDownPos : %3d\nSubArmDownPos : %3d\n\n",
//			  pMoldMsg->MoldConfig.OutsideWait, pMoldMsg->MoldConfig.MainArmType,pMoldMsg->MoldConfig.SubArmType,
//			  pMoldMsg->MoldConfig.MainArmDownPos, pMoldMsg->MoldConfig.SubArmDownPos);	
//	 
//	 printf("ChuckOff : %3d\nVaccumOff : %3d\nSubArmOff : %3d\nChuckRejectPos : %3d\nVaccumRejectPos : %3d\n\n\n",
//			  pMoldMsg->MoldConfig.ChuckOff,pMoldMsg->MoldConfig.VaccumOff,	pMoldMsg->MoldConfig.SubArmOff,
//			  pMoldMsg->MoldConfig.ChuckRejectPos, pMoldMsg->MoldConfig.VaccumRejectPos);	
//#endif
//	 printf("Done.......\n\r");
//	 TestLoopState = STATE_SD_FILE_CLOSE;
//	 
//	 break;
//	 
//	 
//  case STATE_ON:
//	 if(ChkExpireSysTick(&TempTick))
//	 {
//		//		 HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);
//		//		 HAL_GPIO_WritePin(LCD_BACKLIGHT_GPIO_Port, LCD_BACKLIGHT_Pin, GPIO_PIN_RESET);
//		SetSysTick(&TempTick, TIME_TEST);
//		TestLoopState = STATE_OFF;
//	 }
//	 break;
//	 
//  case STATE_OFF:
//	 
//	 if(ChkExpireSysTick(&TempTick))
//	 {
//		//		 		 HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
//		//		 HAL_GPIO_WritePin(LCD_BACKLIGHT_GPIO_Port, LCD_BACKLIGHT_Pin, GPIO_PIN_SET);
//		SetSysTick(&TempTick, TIME_TEST);
//		TestLoopState = STATE_ON;
//	 }
//	 break;
//  case STATE_RTC:
//	 if(ChkExpireSysTick(&TempTick))
//	 {
//		SetSysTick(&TempTick, TIME_RTC_TEST);
//		sRtc = GetTime();
//		printf("%2d-%2d-%2d %2d:%2d:%2d\n",
//				 sRtc.Date.Year,sRtc.Date.Month,sRtc.Date.Date,
//				 sRtc.Time.Hours,sRtc.Time.Minutes,sRtc.Time.Seconds);
//	 }
//	 break;
//  }
//  
//}
