/* 
 * File:   ActiveRun.h
 * Author: Jehun
 *
* Created on 2021??02??26??(Fri), PM 17:11
*/


#ifndef __ACTIVERUN_H
#define	__ACTIVERUN_H

#ifdef	__cplusplus
extern "C" {
#endif
  
#include "main.h"    
#include "interface.h"
#include "AppMain.h"
#include "AppKey.h"
#include "SDCard.h"



extern CONTROL_MSG ControlMsg;
extern ROBOT_CONFIG RobotCfg;
extern IO_MSG IOMsg;
extern SEQ_MSG SeqMsg;

//SEQ_MSG SeqMsg;
extern MOLD_MSG MoldMsg;
extern COUNT_MSG CountMsg;
extern ERROR_MSG ErrMsg;
extern ERROR_MSG ErrMsgTemp[];

extern uint64_t SDTempTick;
extern uint8_t fPreviousPage;
extern uint8_t AutoInjectionTemp;
//ErrLoopState = ERR_CHECK;

extern void ClearPage(void);
extern void ClearAuto(void);
extern SDRES WriteRobotCfg(ROBOT_CONFIG* pRobotCfg);


//extern ERROR_MSG ErrMsgTemp[100];
extern MOTION_DELAY MotionDelayTemp;
extern MOTION_MODE MotionModeTemp;
extern MANUFACTURER_SETTING SettingTemp;
extern MOTION_DELAY CycleDelay;
extern MOLD_MSG MoldMsg;

extern uint8_t StepCursor;

extern char NewMoldName[MAX_MOLDNAME_LENGTH];
extern char NewMoldNameTemp[MAX_MOLDNAME_LENGTH];
extern uint8_t MoldNameCharPos;
extern uint8_t MoldNamePos;
extern uint16_t MoldNoTemp;
extern uint8_t UnderbarPos;

extern uint8_t AutoStepDelay[20];
  


//MANUFACTURER_SETTING SettingTemp;   //�ߺ�����
//uint8_t UnderbarPos = 0;
//SDRES SdRes;
//SDRES SdResTemp;

/* Private typedef -----------------------------------------------------------*/

uint8_t ReadType(void);
uint8_t KeyIsNum(uint32_t* key);
uint8_t KeyNum(uint32_t* key);

void ChangeLanguage(void);
//void ActiveRun(TP_MODE* TpMode);
void ActiveRun(void);
void ChangeSCMode(void);

extern void ClearPage(void);
extern void ClearAuto(void);

extern void TimerDispCheck(void);
extern void ModeDispCheck(uint16_t MoldNoMode);
extern void StepCheck(void);
extern void ManualIO(void);
extern void ManualInterlock(void);

extern void WriteFunction(void);
extern void AutoState(uint8_t AutoState);
extern void ImportMode(void);

extern void InMenuUpButton(uint8_t Max_Line);
extern void InMenuDownButton(uint8_t Max_Line , uint8_t Max_Page, uint8_t *DispTempArr);

extern void MoldInit(void);
extern void NewMoldInit(uint16_t NewMoldNo);
extern void OpenMold(uint16_t MoldNo);
extern void IMMTypeINIT(uint16_t MoldNo);
extern SDRES WriteRobotCfg(ROBOT_CONFIG* pRobotCfg);




#ifdef	__cplusplus
}
#endif

#endif	/* __ACTIVERUN_H */